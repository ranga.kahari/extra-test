<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Wards
    Route::get('search-wards', 'WardsController@search')->name('wards.search');
    Route::resource('wards', 'WardsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Villages
    Route::get('search-villages', 'VillagesController@search')->name('villages.search');
    Route::delete('villages/destroy', 'VillagesController@massDestroy')->name('villages.massDestroy');
    Route::resource('villages', 'VillagesController');

    // Thematic Areas
    Route::delete('thematic-areas/destroy', 'ThematicAreasController@massDestroy')->name('thematic-areas.massDestroy');
    Route::resource('thematic-areas', 'ThematicAreasController');

    // Age Bands
    Route::delete('age-bands/destroy', 'AgeBandsController@massDestroy')->name('age-bands.massDestroy');
    Route::resource('age-bands', 'AgeBandsController');

    // Value Chains
    Route::delete('value-chains/destroy', 'ValueChainsController@massDestroy')->name('value-chains.massDestroy');
    Route::resource('value-chains', 'ValueChainsController');

    // Training Categories
    Route::delete('training-categories/destroy', 'TrainingCategoriesController@massDestroy')->name('training-categories.massDestroy');
    Route::resource('training-categories', 'TrainingCategoriesController');

    // Topics
    Route::delete('topics/destroy', 'TopicsController@massDestroy')->name('topics.massDestroy');
    Route::resource('topics', 'TopicsController');

    // Facilitators
    Route::get('search-facilitators', 'FacilitatorsController@search')->name('facilitators.search');
    Route::delete('facilitators/destroy', 'FacilitatorsController@massDestroy')->name('facilitators.massDestroy');
    Route::post('facilitators/import', 'FacilitatorsController@import')->name('facilitators.import');
    Route::resource('facilitators', 'FacilitatorsController');

    // Groups
    Route::get('search-groups', 'GroupsController@search')->name('groups.search');
    Route::delete('groups/destroy', 'GroupsController@massDestroy')->name('groups.massDestroy');
    // Route::get('groups/{group_id}/members/create', 'GroupsController@createForGroup')->name('group.members.create');
    //Route::post('mass-store-members', 'GroupsController@massStore')->name('members.massStore');
    Route::resource('groups', 'GroupsController');


    // Beneficiaries
    Route::get('search-beneficiaries', 'BeneficiariesController@search')->name('beneficiaries.search');
    Route::delete('beneficiaries/destroy', 'BeneficiariesController@massDestroy')->name('beneficiaries.massDestroy');
    Route::get('beneficiaries/export','BeneficiariesController@export')->name('beneficiaries.export');
    Route::resource('beneficiaries', 'BeneficiariesController');

    // Trainings
    Route::post('trainings/import', 'TrainingsController@import')->name('trainings.import');
    Route::delete('trainings/destroy', 'TrainingsController@massDestroy')->name('trainings.massDestroy');
    // Route::post('trainings/process-excel-import', 'TrainingsController@processExcelImport')->name('trainigs.processCsvImport');
    Route::resource('trainings', 'TrainingsController');

    // Attendances
    Route::delete('attendances/destroy', 'AttendancesController@massDestroy')->name('attendances.massDestroy');
    Route::get('trainings/{training_id}/attendances/create', 'AttendancesController@createForTraining')->name('training.attendances.create');
    Route::post('mass-store-attendances', 'AttendancesController@massStore')->name('attendances.massStore');
    Route::resource('attendances', 'AttendancesController');

      //Members
    Route::delete('members/destroy', 'MembersController@massDestroy')->name('members.massDestroy');
    Route::get('groups/{group_id}/members/create', 'MembersController@createForGroup')->name('group.members.create');
    Route::get('value-chain-training-sites/{group_id}/members/create', 'MembersController@createForValueChainTrainingSite')->name('vcts.members.create');
    Route::get('care-groups/{group_id}/members/create', 'MembersController@createForCareGroup')->name('care-groups.members.create');
    Route::get('isals/{group_id}/members/create', 'MembersController@createForIsals')->name('isals.members.create');
    Route::post('mass-store-members', 'MembersController@massStore')->name('members.massStore');
    Route::post('mass-store-vcts-members', 'MembersController@massStoreValueChainTrainingSite')->name('members.massStoreValueChainTrainingSite');
    Route::post('mass-store-caregroups-members', 'MembersController@massStoreCareGroup')->name('members.massStoreCareGroup');
    Route::post('mass-store-isals-members', 'MembersController@massStoreIsal')->name('members.massStoreIsal');
    Route::resource('members', 'MembersController');

    //Value Chain Training Site - Group-Category
    Route::post('value-chain-training-sites/import', 'ValueChainTrainingSiteController@import')->name('value-chain-training-sites.import');
    Route::delete('value-chain-training-sites/destroy', 'ValueChainTrainingSiteController@massDestroy')->name('value-chain-training-sites.massDestroy');
    Route::get('value-chain-training-sites/create', 'ValueChainTrainingSiteController@create')->name('value-chain-training-sites.create');
    Route::get('value-chain-training-sites/{group}', 'ValueChainTrainingSiteController@showGroup')->name('value-chain-training-sites.showGroup');
    Route::get('value-chain-training-sites/{group}/edit', 'ValueChainTrainingSiteController@edit')->name('value-chain-training-sites.edit');
    Route::patch('value-chain-training-sites/{id}', 'ValueChainTrainingSiteController@update')->name('value-chain-training-sites.update');
    Route::delete('value-chain-training-sites/{group}', 'ValueChainTrainingSiteController@destroy')->name('value-chain-training-sites.destroy');
    Route::resource('value-chain-training-sites', 'ValueChainTrainingSiteController');

    //Care Group - Group-Category
    Route::post('care-groups/import', 'CareGroupController@import')->name('care-groups.import');
    Route::delete('care-groups/destroy', 'CareGroupController@massDestroy')->name('care-groups.massDestroy');
    Route::get('care-groups/create', 'CareGroupController@create')->name('care-groups.create');
    Route::get('care-groups/{group}', 'CareGroupController@show')->name('care-groups.show');
    Route::get('care-groups/{group}/edit', 'CareGroupController@edit')->name('care-groups.edit');
    Route::patch('care-groups/{id}', 'CareGroupController@update')->name('care-groups.update');
    Route::delete('care-groups/{group}', 'CareGroupController@destroy')->name('care-groups.destroy');
    Route::resource('care-groups', 'CareGroupController');
    
    //Isals Group - Group Category
    Route::post('isals/import', 'IsalsController@import')->name('isals.import');
    Route::delete('isals/destroy', 'IsalsController@massDestroy')->name('isals.massDestroy');
    Route::get('isals/create', 'IsalsController@create')->name('isals.create');
    Route::get('isals/{group}', 'IsalsController@show')->name('isals.show');
    Route::get('isals/{group}/edit', 'IsalsController@edit')->name('isals.edit');
    Route::patch('isals/{id}', 'IsalsController@update')->name('isals.update');
    Route::delete('isals/{group}', 'IsalsController@destroy')->name('isals.destroy');
    Route::resource('isals', 'IsalsController');
    
    //Interventions
    Route::post('interventions/import', 'InterventionsController@import')->name('interventions.import');
    Route::delete('interventions/destroy', 'InterventionsController@massDestroy')->name('interventions.massDestroy');
    Route::get('interventions/map', 'InterventionsController@map')->name('interventions.map');
    Route::resource('interventions', 'InterventionsController');
    
    //Distribution
    Route::post('distributions/import', 'DistributionsController@import')->name('distributions.import');
    Route::get('distributions/export','DistributionsController@export')->name('distributions.export');
    Route::delete('distributions/destroy', 'DistributionsController@massDestroy')->name('distributions.massDestroy');
    Route::get('distributions/{distribution}/edit', 'DistributionsController@edit')->name('distributions.edit');
    Route::patch('distributions/{id}', 'DistributionsController@update')->name('distributions.update');
    Route::resource('distributions', 'DistributionsController');
    
    //Crops Data
    Route::post('crops/import', 'CropsController@import')->name('crops.import');
    Route::get('crops/export','CropsController@export')->name('crops.export');
    Route::delete('crops/destroy', 'CropsController@massDestroy')->name('crops.massDestroy');
    Route::resource('crops', 'CropsController');
    
    //Pfumvudza
    Route::post('pfumvudza/import', 'PfumvudzaController@import')->name('pfumvudza.import');
    Route::get('pfumvudza/export','PfumvudzaController@export')->name('pfumvudza.export');
    Route::delete('pfumvudza/destroy', 'PfumvudzaController@massDestroy')->name('pfumvudza.massDestroy');
    Route::resource('pfumvudza', 'PfumvudzaController');
    
    //Clean Facilitator - Teachers
    Route::delete('teachers/destroy', 'TeachersController@massDestroy')->name('teachers.massDestroy');
    Route::post('teachers/import', 'TeachersController@import')->name('teachers.import');
    Route::get('teachers/export','TeachersController@export')->name('teachers.export');
    Route::resource('teachers', 'TeachersController');
    
    //Clean Beneficiaries - People
    Route::delete('people/destroy', 'PeopleController@massDestroy')->name('people.massDestroy');
    Route::post('people/import', 'PeopleController@import')->name('people.import');
    Route::get('people/export','PeopleController@export')->name('people.export');
    Route::resource('people', 'PeopleController');
    
    //Smart Subsidies
    Route::delete('smart-subsidies/destroy', 'SmartSubsidies@massDestroy')->name('smart-subsidies.massDestroy');
    Route::post('smart-subsidies/import', 'SmartSubsidiesController@import')->name('smart-subsidies.import');
    Route::get('smart-subsidies/create', 'SmartSubsidiesController@create')->name('smart-subsidies.create');
    Route::get('smart-subsidies/{smartSubsidies}', 'SmartSubsidiesController@show')->name('smart-subsidies.show');
    Route::get('smart-subsidies/{smartSubsidies}/edit', 'SmartSubsidiesController@edit')->name('smart-subsidies.edit');
    Route::patch('smart-subsidies/{id}', 'SmartSubsidiesController@update')->name('smart-subsidies.update');
    Route::delete('smart-subsidies/{smartSubsidies}', 'SmartSubsidiesController@destroy')->name('smart-subsidies.destroy');
    Route::resource('smart-subsidies', 'SmartSubsidiesController');

    //Reports
    Route::get('reports/value-chain', 'ReportsController@valueChainReport')->name('reports.value-chain');
    Route::get('reports/value-chain/data', 'ReportsController@valueChainReportData')->name('reports.value-chain-data');
    Route::get('reports/value-chain/data/gender-counts', 'ReportsController@valueChainReportDataGenderCounts')->name('reports.value-chain-data-gender-counts');
    Route::get('reports/topic', 'ReportsController@topicReport')->name('reports.topic');
    Route::get('reports/topic/data', 'ReportsController@topicReportData')->name('reports.topic-data');
    Route::get('reports/topic/data/gender-counts', 'ReportsController@topicReportDataGenderCounts')->name('reports.topic-data-gender-counts');
    Route::get('reports/care-group', 'ReportsController@careGroupReport')->name('reports.care-group');
    Route::get('reports/care-group/data', 'ReportsController@careGroupReportData')->name('reports.care-group-data');
    Route::post('reports/care-group/search', 'ReportsController@careGroupReportSearch')->name('reports.care-group-search');
    Route::get('reports/care-group/facilitator', 'ReportsController@careGroupFacilitatorReport')->name('reports.care-group-facilitator');
    Route::post('reports/care-group/search/facilitator', 'ReportsController@careGroupReportSearchByFacilitator')->name('reports.care-group-facilitator-search');
    Route::get('reports/distribution', 'ReportsController@distribution')->name('reports.distribution');
    Route::get('reports/interventions', 'ReportsController@intervention')->name('reports.intervention');
    Route::post('reports/interventions', 'ReportsController@interventionSearch')->name('reports.intervention-search');
    Route::get('reports/vcts', 'ReportsController@vcts')->name('reports.vcts');
    Route::post('reports/vcts', 'ReportsController@vctsSearch')->name('reports.vcts-search');
    Route::get('reports/isal', 'ReportsController@isal')->name('reports.isal');
    Route::post('reports/isal', 'ReportsController@isalSearch')->name('reports.isal-search');
    Route::get('reports/yieldData', 'ReportsController@yielddata')->name('reports.yieldData');
    Route::post('reports/yieldData', 'ReportsController@yielddatasearch')->name('reports.yieldData-search');
    Route::get('reports/pfumvudza', 'ReportsController@pfumvudza')->name('reports.pfumvudza');
    Route::post('reports/pfumvudza', 'ReportsController@pfumvudzaSearch')->name('reports.pfumvudza-search');
    Route::get('reports/pfumvudza-value-chain', 'ReportsController@pfumvudzaValueChain')->name('reports.pfumvudza-value-chain');
    Route::post('reports/pfumvudza-value-chain', 'ReportsController@pfumvudzaValueChainSearch')->name('reports.pfumvudza-value-chain-search');
    Route::get('reports/smart-subsidies', 'ReportsController@smartSubsidy')->name('reports.smart-subsidies');
    Route::post('reports/smart-subsidies', 'ReportsController@smartSubsidySearch')->name('reports.smart-subsidies-search');
    Route::resource('reports', 'ReportsController');

    //Route::get('care-group-analysis','CareGroupAnalysisController@index')->name('care-group-analysis');

});





Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
