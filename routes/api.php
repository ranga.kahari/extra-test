<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // // Permissions
    // Route::apiResource('permissions', 'PermissionsApiController');

    // // Roles
    // Route::apiResource('roles', 'RolesApiController');

    // // Users
    // Route::apiResource('users', 'UsersApiController');

    // // Wards
    // Route::apiResource('wards', 'WardsApiController', ['except' => ['store', 'update', 'destroy']]);

    // // Villages
    // Route::apiResource('villages', 'VillagesApiController');

    // // Thematic Areas
    // Route::apiResource('thematic-areas', 'ThematicAreasApiController');

    // // Age Bands
    // Route::apiResource('age-bands', 'AgeBandsApiController');

    // // Value Chains
    // Route::apiResource('value-chains', 'ValueChainsApiController');

    // // Training Categories
    // Route::apiResource('training-categories', 'TrainingCategoriesApiController');

    // // Topics
    // Route::apiResource('topics', 'TopicsApiController');

    // // Facilitators
    // Route::apiResource('facilitators', 'FacilitatorsApiController');

    // // Groups
    // Route::apiResource('groups', 'GroupsApiController');

    // // Beneficiaries
    // Route::apiResource('beneficiaries', 'BeneficiariesApiController');

    // // Trainings
    // Route::apiResource('trainings', 'TrainingsApiController');
});
