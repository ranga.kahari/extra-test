@extends('layouts.admin')
@section('filters')
<button class="c-header-toggler c-class-toggler mfe-md-3" type="button" data-target="#aside"
    data-class="c-sidebar-show">
    <i class="fas fa-filter"></i> Filters
</button>
@endsection

@section('filter-sidebar')
@include('partials.filters')
@endsection

@section('content')
<div class="content">

    <div class="row">
        @include('partials.widgets.one-third-chart', [
        'chartId' => 'farmers_by_gender',
        'chartTitle' => 'Farmers by Gender',
        'chartType' => 'pie'
        ])
        @include('partials.widgets.one-third-chart', [
        'chartId' => 'farmers_by_category',
        'chartTitle' => 'Farmers by Category',
        'chartType' => 'pie'
        ])
        @include('partials.widgets.one-third-chart', [
        'chartId' => 'farmers_by_age_band',
        'chartTitle' => 'Farmers by Age Band',
        'chartType' => 'bar'
        ])
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}" />
@endsection

@section('scripts')
    @parent

    <script src="{{ asset('js/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>

    <script>
        var thematicAreas = {!! json_encode($thematicAreas) !!};
        var wards = {!! json_encode($wards) !!};
        
        var request = null;

        var start_date = '';
        var end_date = '';

        $('#date_range').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD MMM YYYY',
                separator: ' - '
            },
            showDropdowns: true,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 6 Months': [moment().subtract(6, 'months'), moment()],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            }
        });

        refreshDashboard();

        //redraw plot on date change
        $('#date_range').on('apply.daterangepicker', function(e, picker) {
            $(this).val(picker.startDate.format('DD MMM YYYY') + ' - ' + picker.endDate.format('DD MMM YYYY'));
            start_date = picker.startDate;
            end_date = picker.endDate;
            refreshDashboard();
        });

        $('#date_range').on('cancel.daterangepicker change', function() {
            $(this).val('');
            start_date = '';
            end_date = '';
            refreshDashboard();
        });

        //populate dropdowns and redraw plot on select
        $('.select2').on('select2:select select2:unselect', function (e) {
            var data = $(this).val();
            
            if ($(this).attr('id') == 'thematic_areas') {
                var trainingCodeOptions = '';
                $.each(thematicAreas, function( key, value ) {
                    if ($.inArray((value.id).toString(), data) != -1) {
                        trainingCodeOptions += ('<optgroup label="'+value.label+'">');
                        $.each(value.training_codes, function( key, value ) {
                            trainingCodeOptions += ('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                        trainingCodeOptions += '</optgroup>';
                    }
                });
                
                $('#training_codes').html(trainingCodeOptions);
            }
            
            if ($(this).attr('id') == 'districts') {
                var wardOptions = '';
                $.each(wards, function( key, value ) {
                    if ($.inArray((value.district).toString(), data) != -1) {
                        wardOptions += ('<option value="'+value.id+'">'+value.name+'</option>');
                    }
                });
                
                $('#wards').html(wardOptions);
            }
            refreshDashboard();
        });

        $(document).on('click', '.clear-all', function (e) {
            e.preventDefault();
            $('.clearable').val(null).trigger('change');
        });


        function refreshDashboard(){
            if (request) {
                request.abort();
            }

            request = $.ajax(
            {
                url: "{!! route('admin.home') !!}",
                data: refreshCurrentFilters(),
                dataType: 'json',
                type: 'GET'
            }).done(function (rows){
                var charts = [];
                var classes = [];

                $('.charts').each(function(){
                    var chartid = $(this).attr('id');
                    var charttype = $(this).data('charttype');
                    var charttitle = $(this).data('charttitle');
                    var chartdata = rows[chartid];

                    if(!Array.isArray(chartdata) || !chartdata.length){
                        $(this).html('No data for chart: ' + charttitle);
                    }
                    else{
                        if(charttype.length > 0){
                            // TODO check dom element exists before disposing
                            echarts.dispose(document.getElementById(chartid));
                            charts[chartid] = echarts.init(document.getElementById(chartid), chartTheme());
                            charts[chartid].setOption(chartOptions(chartdata, charttype, charttitle));
                        }
                        else{
                            $(this).html(chartdata);
                        }
                    }
                });
            }).fail(function (data){

            });
        }

        function refreshCurrentFilters() {
            var selected = {};
            $('.filter-select-multiple').each(function(index, element){
                var filter_key = $(element).attr('id');
                selected[filter_key] = $('select[name="'+filter_key+'[]"]').val();
            });
            $('.filter-select-single').each(function(index, element){
                var filter_key = $(element).attr('id');
                selected[filter_key] = $('select[name="'+filter_key+'"]').val();
            });

            if (moment.isMoment(start_date) || moment.isDate(start_date)) {
                selected['start_date'] = start_date.format('YYYY-MM-DD');
            } else {
                selected['start_date'] = start_date;
            }

            if (moment.isMoment(end_date) || moment.isDate(end_date)) {
                selected['end_date'] = end_date.format('YYYY-MM-DD');
            } else {
                selected['end_date'] = end_date;
            }

            return selected;
        }

        function chartOptions(chartdata, charttype, charttitle) {
            var options = {};
            if(charttype != null && chartdata != null){
                options['title'] = [
                    {
                        text: charttitle,
                        left: 'left',
                        top: 'top',
                        textStyle: {fontFamily: 'Source Sans Pro', fontWeight: 500, color: '#444', fontSize: '16'}
                    }
                ];
                options['tooltip'] = {
                    trigger: charttype == 'pie' ? 'item' : 'axis'
                };
                if(charttype == 'pie'){
                    options['tooltip']['formatter'] = function (params) {
                        var formatted_value = Math.round(params.value);
                        return params.seriesName+"<br/>"+params.name+": "+formatted_value+" ("+params.percent+"%)";
                    }
                }
                options['toolbox'] = {
                };
                if(charttype == 'column' || charttype == 'line'){
                    options['yAxis'] = {
                        type: 'value',
                        boundaryGap: [0, 0.01]
                    };
                    options['xAxis'] = {
                        type: 'category',
                        axisPointer: {
                            type: 'shadow'
                        },
                        data: unpack(chartdata, 'name')
                    };
                }
                else if(charttype == 'bar') {
                    options['xAxis'] = {
                        type: 'value',
                        boundaryGap: [0, 0.01]
                    };
                    options['yAxis'] = {
                        type: 'category',
                        axisPointer: {
                            type: 'shadow'
                        },
                        data: unpack(chartdata, 'name')
                    };
                }
                options['series'] = [
                    {
                        name: charttitle,
                        type: charttype,
                        smooth:true,
                        symbol: 'none',
                        sampling: 'average',
                    }
                ];
                if(charttype == 'pie'){
                    options['series'][0]['radius'] = ['25%', '40%'];
                    options['series'][0]['data'] = chartdata;
                    options['series'][0]['label'] = {
                        normal: {show: true, textStyle: {fontSize: '9'}, formatter: "{b}\n{d}%"},
                        emphasis: { show: true, formatter: "{b}\n{d}%"}
                    };
                }
                else{
                    options['series'][0]['data'] = unpack(chartdata, 'value')
                }
            }

            return options;
        }

        function chartTheme() {
            return {
                color: ['#26B99A', '#1D7775', '#4FCFD5', '#FCE651', '#FF7050', '#FFC050', '#3498DB', '#9B59B6', '#BA1F33', '#BDC3C7', '#759c6a', '#34495E', '#ec4863'],
                valueAxis: {minInterval: 1, splitLine: { show: true }},
                categoryAxis: {splitLine: {show: false }},
                legend: {show: false},
                grid: {left: '3%',right: '3%',containLabel: true},
                toolbox: {
                    show: true,
                    feature: {
                        mark : {show: true},
                        saveAsImage: {show: true, title: "Download plot as a png", pixelRatio: 6}
                    }
                }
            };
        }

        function unpack(rows, key) {
            return rows.map(function (row) {
                return row[key];
            });
        }
</script>

@endsection