<div class="c-sidebar c-sidebar-lg c-sidebar-light c-sidebar-right c-sidebar-overlaid  " id="aside">
    <form role="form" action="#" method="GET">
        <div class="card-body">
            <div class="form-group">
                <label for="date_range">Date Range</label>
                <input type="text" class="form-control pull-right clearable" id="date_range" style="font-size: 13px;margin-bottom:1rem;">
            </div>
            
            <div class="form-group">
                <label for="thematic_areas">Thematic Areas</label>
                <select name="thematic_areas[]" id="thematic_areas" class="form-control select2 filter-select-multiple clearable"
                    multiple>
                    @foreach($thematicAreas as $key => $thematicArea)
                        <option value="{{ $thematicArea->id }}">{{ $thematicArea->label }}</option>
                    @endforeach
                </select>
            </div>
            
            {{-- <div class="form-group">
                <label for="training_codes">Training Codes </label>
                <select id="training_codes" name="training_codes[]"
                    class="form-control select2 filter-select-multiple clearable" multiple>
                </select>
            </div> --}}
            
            {{-- <div class="form-group">
                <label for="value_chains">Value Chains</label>
                <select name="value_chains[]" id="value_chains" class="form-control select2 filter-select-multiple clearable"
                    multiple>
                    @foreach($valueChains as $id => $valueChain)
                        <option value="{{ $id }}">{{ $valueChain }}</option>
                    @endforeach
                </select>
            </div> --}}
            
            <div class="form-group">
                <label for="districts">Districts</label>
                <select name="districts[]" id="districts" class="form-control select2 filter-select-multiple clearable" multiple>
                    @foreach(App\Models\Ward::DISTRICT_SELECT as $key => $label)
                        <option value="{{ $key }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="wards">Wards</label>
                <select name="wards[]" id="wards" class="form-control select2 filter-select-multiple clearable" multiple>
                </select>
            </div>
            
            <div class="form-group">
                <label for="farmer_category">Farmer Category</label>
                <select id="farmer_category" name="farmer_category"
                    class="form-control select2 filter-select-single clearable">
                    <option value=""></option>
                    @foreach(App\Models\Beneficiary::FARMER_CATEGORY_SELECT as $key => $label)
                        <option value="{{ $key }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="gender">Gender</label>
                <select id="gender" name="gender" class="form-control select2 filter-select-single clearable">
                    <option value="" class=""></option>
                    @foreach(App\Models\Beneficiary::GENDER_SELECT as $key => $label)
                    <option value="{{ $key }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
    
            <div>
                <a class="btn btn-secondary clear-all" href="#">Clear</a>
            </div>
        </div>
    </form>
</div>
