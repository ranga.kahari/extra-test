<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('database_access')
        <li
            class="c-sidebar-nav-dropdown {{ in_array(request()->segment(2), ['beneficiaries', 'groups', 'facilitators']) ? 'c-show' : '' }}">
            <a class="c-sidebar-nav-dropdown-toggle" href="#">
                <i class="fa-fw fas fa-database c-sidebar-nav-icon">

                </i>
                {{ trans('cruds.database.title') }}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                @can('beneficiary_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.beneficiaries.index") }}"
                        class="c-sidebar-nav-link {{ request()->segment(2) == 'beneficiaries' ? 'active c-active' : '' }}">
                        <i class="fa fa-minus pr-2"></i>
                        {{ trans('cruds.beneficiary.title') }}
                    </a>
                </li>
                @endcan
                @can('facilitator_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.facilitators.index") }}"
                        class="c-sidebar-nav-link {{ request()->segment(2) == 'facilitators' ? 'active c-active' : '' }}">
                        <i class="fa fa-minus pr-2"></i>
                        {{ trans('cruds.facilitator.title') }}
                    </a>
                </li>
                @endcan
                @can('intervention_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.interventions.index") }}"
                        class="c-sidebar-nav-link {{ request()->segment(2) == 'facilitators' ? 'active c-active' : '' }}">
                        <i class="fa fa-minus pr-2"></i>
                        {{ trans('cruds.interventions.title') }}
                    </a>
                </li>
                @endcan
                @can('distribution_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.distributions.index") }}"
                        class="c-sidebar-nav-link {{ request()->segment(2) == 'distribution' ? 'active c-active' : '' }}">
                        <i class="fa fa-minus pr-2"></i>
                        {{ trans('cruds.distribution.title') }}
                    </a>
                </li>
                @endcan
                @can('crop_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.crops.index") }}"
                        class="c-sidebar-nav-link {{ request()->segment(2) == 'crops' ? 'active c-active' : '' }}">
                        <i class="fa fa-minus pr-2"></i>
                        {{ trans('cruds.yieldData.title') }}
                    </a>
                </li>
                @endcan
                @can('pfumvudza_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.pfumvudza.index") }}"
                        class="c-sidebar-nav-link {{ request()->segment(2) == 'pfumvudza' ? 'active c-active' : '' }}">
                        <i class="fa fa-minus pr-2"></i>
                        {{ trans('cruds.pfumvudza.title') }}
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan

        @can('smart_access')
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.smart-subsidies.index") }}"
                class="c-sidebar-nav-link {{ request()->segment(2) == 'smart-subsidies' ? 'active c-active' : '' }}">
                <i class="fa fa-university fas fa-tasks c-sidebar-nav-icon">

                </i>
                {{ trans('cruds.smartSubsidies.title') }}
            </a>
        </li>
        @endcan

        @can('group_access')
        <li
            class="c-sidebar-nav-dropdown {{ in_array(request()->segment(3), ['valuechaintrainingsites', 'caregroups', 'isals']) ? 'c-show' : '' }}">
            <a class="c-sidebar-nav-dropdown-toggle" href="#">
                <i class="fa-fw fas fa-cubes c-sidebar-nav-icon">

                </i>
                {{ trans('cruds.group.title') }}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                {{-- @can('group_access')
                                    <li class="c-sidebar-nav-item">
                                        <a href="{{ route("admin.groups.index") }}"
                class="c-sidebar-nav-link {{ request()->segment(2) == 'groups' ? 'active c-active' : '' }}">
                <i class="fa fa-minus pr-2"></i>
                {{ trans('cruds.group.title') }}
                </a>
        </li>
        @endcan --}}
        @can('vcts_access')
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.value-chain-training-sites.index") }}"
                class="c-sidebar-nav-link {{ request()->segment(2) == 'valuechaintrainingsites' ? 'active c-active' : '' }}">
                <i class="fa fa-minus pr-2"></i>
                {{ trans('cruds.valueChainTrainingSite.title') }}
            </a>
        </li>
        @endcan

        @can('caregroup_access')
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.care-groups.index") }}"
                class="c-sidebar-nav-link {{ request()->segment(2) == 'caregroups' ? 'active c-active' : '' }}">
                <i class="fa fa-minus pr-2"></i>
                {{ trans('cruds.careGroup.title') }}
            </a>
        </li>
        @endcan

        @can('isals_access')
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.isals.index") }}"
                class="c-sidebar-nav-link {{ request()->segment(2) == 'isals' ? 'active c-active' : '' }}">
                <i class="fa fa-minus pr-2"></i>
                {{ trans('cruds.isals.title') }}
            </a>
        </li>
        @endcan
    </ul>
    </li>
    @endcan

    @can('training_access')
    <li class="c-sidebar-nav-item">
        <a href="{{ route("admin.trainings.index") }}"
            class="c-sidebar-nav-link {{ request()->segment(2) == 'trainings' ? 'active c-active' : '' }}">
            <i class="fa-fw fas fa-tasks c-sidebar-nav-icon">

            </i>
            {{ trans('cruds.training.title') }}
        </a>
    </li>
    @endcan
    @can('dimension_access')
    <li
        class="c-sidebar-nav-dropdown {{ in_array(request()->segment(2), ['thematic-areas', 'age-bands', 'value-chains', 'training-categories', 'topics']) ? 'c-show' : '' }}">
        <a class="c-sidebar-nav-dropdown-toggle" href="#">
            <i class="fa-fw fas fa-table c-sidebar-nav-icon">

            </i>
            {{ trans('cruds.dimension.title') }}
        </a>
        <ul class="c-sidebar-nav-dropdown-items">
            @can('thematic_area_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.thematic-areas.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'thematic-areas' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.thematicArea.title') }}
                </a>
            </li>
            @endcan
            @can('age_band_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.age-bands.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'age-bands' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.ageBand.title') }}
                </a>
            </li>
            @endcan
            @can('value_chain_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.value-chains.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'value-chains' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.valueChain.title') }}
                </a>
            </li>
            @endcan
            @can('training_category_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.training-categories.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'training-categories' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.trainingCategory.title') }}
                </a>
            </li>
            @endcan
            @can('topic_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.topics.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'topics' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.topic.title') }}
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('user_access')
    <li class="c-sidebar-nav-item">
        <a href="{{ route("admin.users.index") }}"
            class="c-sidebar-nav-link {{ request()->segment(2) == 'users' ? 'active c-active' : '' }}">
            <i class="fa-fw fas fa-user c-sidebar-nav-icon">
            </i>
            {{ trans('cruds.user.title') }}
        </a>
    </li>
    @endcan
    @can('system_config_access')
    <li class="c-sidebar-nav-dropdown {{ in_array(request()->segment(2), ['wards', 'villages']) ? 'c-show' : '' }}">
        <a class="c-sidebar-nav-dropdown-toggle" href="#">
            <i class="fa-fw fas fa-cog c-sidebar-nav-icon">

            </i>
            {{ trans('cruds.systemConfig.title') }}
        </a>
        <ul class="c-sidebar-nav-dropdown-items">
            @can('ward_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.wards.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'wards' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.ward.title') }}
                </a>
            </li>
            @endcan
            @can('village_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.villages.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(2) == 'villages' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.village.title') }}
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('report_access')
    <li class="c-sidebar-nav-dropdown {{ in_array(request()->segment(2), ['value-chains', 'topic']) ? 'c-show' : '' }}">
        <a class="c-sidebar-nav-dropdown-toggle" href="#">
            <i class="fa-fw fas fa-chart-bar c-sidebar-nav-icon">

            </i>
            {{ trans('cruds.report.title') }}
        </a>
        <ul class="c-sidebar-nav-dropdown-items">
            @can('valuechainreport_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.value-chain") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'value-chain' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.report.value_chain') }}
                </a>
            </li>
            @endcan
            @can('topicreport_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.topic") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'topic' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.report.topic') }}
                </a>
            </li>
            @endcan
            @can('topicreport_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.care-group") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'care-group-report' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.careGroup.fields.neighbor_women_group') }}
                </a>
            </li>
            @endcan
            @can('topicreport_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.care-group-facilitator") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'topic' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.careGroup.title_singular') }}
                </a>
            </li>
            @endcan
            @can('teacher_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.teachers.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'teachers' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    New {{ trans('cruds.facilitator.title') }}
                </a>
            </li>
            @endcan
            @can('people_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.people.index") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'people' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    New {{ trans('cruds.beneficiary.title') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.distribution") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'distribution' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.distribution.title_singular') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.intervention") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'intervention' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.interventions.title_singular') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.vcts") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'vcts' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.valueChainTrainingSite.title_singular') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.isal") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'isal' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.isals.title_singular') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.yieldData") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'people' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.yieldData.title_singular') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.pfumvudza") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'people' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.pfumvudza.title_singular') }} -
                    {{ trans('cruds.pfumvudza.fields.agronomic_practice') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.pfumvudza-value-chain") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'people' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    {{ trans('cruds.pfumvudza.title_singular') }} - {{ trans('cruds.pfumvudza.fields.value_chain') }}
                </a>
            </li>
            @endcan
            @can('report_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.reports.smart-subsidies") }}"
                    class="c-sidebar-nav-link {{ request()->segment(4) == 'people' ? 'active c-active' : '' }}">
                    <i class="fa fa-minus pr-2"></i>
                    Smart Subsidies
                    {{-- {{ trans('cruds.isals.title_singular') }} --}}
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    </ul>

</div>