<div class="col-sm-2 border-right">
    <div class="description-block">
        <h5 class="description-header kpi-text"  id="{{ $chartId }}">0</h5>
        <span class="description-text">{{ strtoupper( str_replace('_', ' ', $chartId) ) }}</span>
    </div>
</div>
