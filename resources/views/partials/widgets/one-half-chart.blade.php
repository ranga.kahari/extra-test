<div class="col-md-6 col-sm-12 col-xs-12">
    <div class="card">
        <div class="card-body">
            <div class="">
                <div id="{{ $chartId }}" style="height:250px;" class="charts" data-charttitle="{{ $chartTitle }}" data-charttype="{{ $chartType }}"></div>
            </div>
        </div>
    </div>
</div>
