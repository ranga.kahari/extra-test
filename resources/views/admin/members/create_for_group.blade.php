@extends('layouts.admin')
@section('content')

<form action="{{ route("admin.members.massStore") }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="card">
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.member.title_singular') }}
        </div>

        <div class="card-body">

            <div class="form-group row {{ $errors->has('group_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label"
                    for="group">{{ trans('cruds.group.fields.group_number') }}</label>
                <div class="col-md-9">
                    <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <input type="text" class="form-control" value="{{ $group->group_number }}" disabled>
                    @if($errors->has('group_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('group_id') }}
                    </em>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('beneficiary_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="beneficiary">Add Members</label>
                <div class="col-md-9">
                    <select id="attendances" class="form-control select2-ajax-search"
                        ajax-search-url="{{ route('admin.beneficiaries.search') }}" multiple="multiple"></select>
                </div>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="{{ trans('global.save') }}">
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="attendees-heading">
            Members 
            <div class="card-header-actions">

            </div>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped datatable" id="attendees_table">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th width="10">#</th>
                        <th>{{ trans('cruds.beneficiary.fields.first_name') }}</th>
                        <th>{{ trans('cruds.beneficiary.fields.surname') }}</th>
                        <th>{{ trans('cruds.beneficiary.fields.national_id_number') }}</th>

                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="attendees_tbody">
                    @foreach ($group->members as $item)
                    <tr data-entry-id="{{ $item->beneficiary->id }}" data-index="{{ $loop->iteration }}">
                        <td></td>
                        <td>{{ $loop->iteration }}</td>
                        <td data-search="{{ $item->beneficiary->first_name }}">
                            <input type="hidden" name="attendances[{{ $item->beneficiary->id }}][beneficiary_id]"
                                value="{{ $item->beneficiary->id }}">
                            <input type="text" class="form-control" value="{{ $item->beneficiary->first_name }}"
                                disabled="">
                        </td>
                        <td data-search="{{ $item->beneficiary->surname }}">
                            <input type="text" class="form-control" value="{{ $item->beneficiary->surname }}"
                                disabled="">
                        </td>
                        <td data-search="{{ $item->beneficiary->national_id_number }}">
                            <input type="text" class="form-control" value="{{ $item->beneficiary->national_id_number }}"
                                disabled="">
                        </td>

                        <td>
                            <a href="#" class="remove btn btn-xs btn-danger">{{ trans('global.delete') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</form>

@endsection

@section('scripts')
@parent

<script>
    $(function () {
            var selected_attendee_ids = {!! json_encode($group->beneficiaries->pluck('beneficiary_id')) !!};
           
            var tableBody = $('#attendees_tbody');

            let deleteButtonTrans = '{{ trans('global.delete') }}';
            let deleteButton = {
				text: deleteButtonTrans,
				url: "{{ route('admin.members.massDestroy') }}",
				className: 'btn-danger',
				action: function (e, dt, node, config) {
					var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
						return $(entry).data('index');
					});

					if (ids.length === 0) {
						alert('{{ trans('global.datatables.zero_selected') }}');

						return;
					}

					if (confirm('{{ trans('global.areYouSure') }}')) {
						//TODO update selected_attendee_ids array, and remove row with ids given
					}
				}
			};
            let dtButtons = [];
            @can('person_delete')
                dtButtons.push(deleteButton);
            @endcan

            let dtOverrideGlobals = {
				buttons: dtButtons
			};

            var table = $('.datatable').DataTable(dtOverrideGlobals);

            $('.select2-ajax-search').on('select2:select', function (e) {
                var data = e.params.data;

                if ($.inArray(data.id, selected_attendee_ids) != -1) {
                    alert(data.text + ' has been added already');
                }
                else {
                    var lastIndex = parseInt(tableBody.find('tr').last().data('index'));
                    if (isNaN(lastIndex)) {
                        lastIndex = 0;
                    }
                    addAttendeeToTable(table, lastIndex + 1, data);
                    selected_attendee_ids.push(data.id);
                }
                return false;

            });

            function addAttendeeToTable(table, index, data) {
                var tempRow = $(
                    '<tr data-entry-id="' + data.id + '" data-index=' + index + '>' +
                        '<td></td>' +
                        '<td>' + index + '</td>' +
                        '<td data-search="' + data.first_name + '">' +
                            '<input type="hidden" name="attendances[' + index + '][beneficiary_id]" value="' + data.id + '">' +
                            '<input type="text" class="form-control" value="' + data.first_name + '" disabled>' +
                        '</td>' +
                        '<td data-search="' + data.surname + '">' +
                            '<input type="text" class="form-control" value="' + data.surname + '" disabled>' +
                        '</td>' +
                        '<td data-search="' + data.national_id_number + '">' +
                            '<input type="text" class="form-control" value="' + data.national_id_number + '" disabled>' +
                        '</td>' +
                        '<td>' +
                            '<a href="#" class="remove btn btn-xs btn-danger">{!! trans('global.delete') !!}</a>' +
                        '</td>' +
                    '</tr>'
                );
                table.row.add(tempRow).draw();
            }

            $('.select2-ajax-search').on('select2:close', function (e) {
                $(this).val(null).trigger('change');
            });

            $(document).on('click', '.remove', function () {
                table
                    .row( $(this).parentsUntil('tr').parent() )
                    .remove()
                    .draw();

                for( var i = 0; i < selected_attendee_ids.length; i++){
                    if ( selected_attendee_ids[i] == $(this).parentsUntil('tr').parent().data('entry-id')) {
                        selected_attendee_ids.splice(i, 1);
                        i--;
                    }
                }
                return false;
            });
        });
</script>

@endsection