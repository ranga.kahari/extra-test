<div class="card" id="addBeneficiaryForm" style="display: none;">
    <form id="beneficiaryForm" method="POST" action="{{ route("admin.beneficiaries.store") }}" enctype="multipart/form-data">
        @csrf
    
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.beneficiary.title_singular') }}
        </div>
    
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            Demographic Data
                        </div>
    
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label required"
                                    for="first_name">{{ trans('cruds.beneficiary.fields.first_name') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}"
                                        type="text" name="first_name" id="first_name" value="{{ old('first_name', '') }}"
                                        required>
                                    @if($errors->has('first_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('first_name') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="surname">{{ trans('cruds.beneficiary.fields.surname') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}"
                                        type="text" name="surname" id="surname" value="{{ old('surname', '') }}">
                                    @if($errors->has('surname'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('surname') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label
                                    class="col-md-3 col-form-label required">{{ trans('cruds.beneficiary.fields.gender') }}</label>
                                <div class="col-md-9">
                                    <select class="form-control {{ $errors->has('gender') ? 'is-invalid' : '' }}"
                                        name="gender" id="gender" required>
                                        <option value disabled {{ old('gender', null) === null ? 'selected' : '' }}>
                                            {{ trans('global.pleaseSelect') }}</option>
                                        @foreach(App\Models\Beneficiary::GENDER_SELECT as $key => $label)
                                        <option value="{{ $key }}"
                                            {{ old('gender', 'Unspecified') === (string) $key ? 'selected' : '' }}>
                                            {{ $label }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('gender'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="national_id_number">{{ trans('cruds.beneficiary.fields.national_id_number') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('national_id_number') ? 'is-invalid' : '' }}"
                                        type="text" name="national_id_number" id="national_id_number"
                                        value="{{ old('national_id_number', '') }}">
                                    @if($errors->has('national_id_number'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('national_id_number') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="year_of_birth">{{ trans('cruds.beneficiary.fields.year_of_birth') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('year_of_birth') ? 'is-invalid' : '' }}"
                                        type="number" name="year_of_birth" id="year_of_birth"
                                        value="{{ old('year_of_birth', '') }}" step="1" min="0">
                                    @if($errors->has('year_of_birth'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('year_of_birth') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="phone_number">{{ trans('cruds.beneficiary.fields.phone_number') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}"
                                        type="text" name="phone_number" id="phone_number"
                                        value="{{ old('phone_number', '') }}">
                                    @if($errors->has('phone_number'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('phone_number') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label required"
                                    for="village_id">{{ trans('cruds.beneficiary.fields.village') }}</label>
                                <div class="col-md-9">
                                    <select name="village_id" id="village" class="form-control select2-ajax-search"
                                        ajax-search-url="{{ route('admin.villages.search') }}" required>
                                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                                    </select>
                                    @if($errors->has('village'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('village') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div style="display: none;" id="ward-details"
                                class="form-group row {{ $errors->has('ward_id') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label"
                                    for="ward">{{ trans('cruds.village.fields.ward') }}*</label>
                                <div class="col-md-9">
                                    <select name="ward_id" id="ward" class="form-control select2-ajax-search"
                                        ajax-search-url="{{ route('admin.wards.search') }}">
                                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                                    </select>
                                    @if($errors->has('ward_id'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('ward_id') }}
                                    </em>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            Additional Data
                        </div>
    
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="year_joined">{{ trans('cruds.beneficiary.fields.year_joined') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('year_joined') ? 'is-invalid' : '' }}"
                                        type="number" name="year_joined" id="year_joined"
                                        value="{{ old('year_joined', '') }}" step="1" min="0">
                                    @if($errors->has('year_joined'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('year_joined') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="size_of_household">{{ trans('cruds.beneficiary.fields.size_of_household') }}</label>
                                <div class="col-md-9">
                                    <input class="form-control {{ $errors->has('size_of_household') ? 'is-invalid' : '' }}"
                                        type="number" name="size_of_household" id="size_of_household"
                                        value="{{ old('size_of_household', '') }}" step="1" min="0">
                                    @if($errors->has('size_of_household'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('size_of_household') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label"
                                    for="children_under_five_in_household">{{ trans('cruds.beneficiary.fields.children_under_five_in_household') }}</label>
                                <div class="col-md-9">
                                    <input
                                        class="form-control {{ $errors->has('children_under_five_in_household') ? 'is-invalid' : '' }}"
                                        type="number" name="children_under_five_in_household"
                                        id="children_under_five_in_household"
                                        value="{{ old('children_under_five_in_household', '') }}" step="1" min="0">
                                    @if($errors->has('children_under_five_in_household'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('children_under_five_in_household') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label
                                    class="col-md-3 col-form-label">{{ trans('cruds.beneficiary.fields.farmer_category') }}*</label>
                                <div class="col-md-9">
                                    <select required
                                        class="form-control select2 {{ $errors->has('farmer_category') ? 'is-invalid' : '' }}"
                                        name="farmer_category" id="farmer_category">
                                        <option value="">@lang('global.pleaseSelect')</option>
                                        @foreach(App\Models\Beneficiary::FARMER_CATEGORY_SELECT as $key => $label)
                                        <option value="{{ $key }}"
                                            {{ old('farmer_category', '') === (string) $key ? 'selected' : '' }}>
                                            {{ $label }}
                                        </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('farmer_category'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('farmer_category') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label
                                    class="col-md-3 col-form-label">{{ trans('cruds.beneficiary.fields.registered_under_ecofarmer') }}</label>
                                <div class="col-md-9">
                                    <select
                                        class="select2 form-control {{ $errors->has('registered_under_ecofarmer') ? 'is-invalid' : '' }}"
                                        name="registered_under_ecofarmer" id="registered_under_ecofarmer" required>
                                        <option value="">@lang('global.pleaseSelect')</option>
                                        <option value="1"
                                            {{ old('registered_under_ecofarmer', '') === 1 ? 'selected' : '' }}>YES</option>
                                        <option value="0"
                                            {{ old('registered_under_ecofarmer', '') === 0 ? 'selected' : '' }}>NO</option>
                                    </select>
                                    @if($errors->has('registered_under_ecofarmer'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('registered_under_ecofarmer') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
    
    
                            <div class="form-group row">
                                <label
                                    class="col-md-3 col-form-label">{{ trans('cruds.beneficiary.fields.groups') }}</label>
                                <div class="col-md-9">
                                    <select multiple
                                        class="select2 form-control {{ $errors->has('groups') ? 'is-invalid' : '' }}"
                                        name="groups[]" id="groups">
                                        <option value="">@lang('global.pleaseSelect')</option>
                                        @foreach($farmerGroups as $key => $label)
                                        <option value="{{ $key }}"
                                            {{ in_array($key, old('groups', [])) ? 'selected' : '' }}>
                                            {{ $label }}
                                        </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('groups'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('groups') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="card-footer">
            <button class="btn btn-success" type="submit"> {{ trans('global.save') }}</button>
            <button class="btn btn-danger" type="reset"> Reset</button>
            <a class="btn btn-default" href="{{ route('admin.beneficiaries.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </form>
</div>