<div class="modal fade" id="facilitatorsModal" tabindex="-1" role="dialog" aria-labelledby="facilitatorsModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="facilitatorsModalLabel">Select Facilitators</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row {{ $errors->has('facilitators') ? 'has-error' : '' }}">
                    <label class="col-md-3 col-form-label"
                        for="facilitators">{{ trans('cruds.group.fields.facilitators') }}</label>
                    <div class="col-md-9">
                        <select name="facilitators[]" id="facilitators" class="form-control modal-select select2-ajax-search"
                            ajax-search-url="{{ route('admin.facilitators.search') }}" multiple="multiple"
                            placeholder="{{ trans('global.pleaseSelect') }}"></select>
                        @if($errors->has('facilitators'))
                        <em class="invalid-feedback">
                            {{ $errors->first('facilitators') }}
                        </em>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>