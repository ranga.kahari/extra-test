@extends('layouts.admin')

@section('content')
@can('intervention_create')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
    </div>
</div>
@endcan

<div class="card">
    <div class="card-header">
        {{ trans('cruds.interventions.title_singular') }} Map
    </div>

    <div class="card-body">
        <div id="mapid" style="height:500px;"></div>
    </div>
</div>
<div class="form-group">
    <a class="btn btn-primary" href="{{ route('admin.interventions.index') }}">
        {{ trans('global.back_to_list') }}
    </a>
</div>


@endsection

@section('scripts')
@parent
<script>
    $(function () {
        var data = {!! json_encode($data) !!};
        console.log(data);
        var mymap = L.map('mapid').setView([-19.0169211, 29.1528018], 7);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoicmFuZ2Eta2FoYXJpIiwiYSI6ImNraW82NXNtYTBmaXIydG1seWpha2ZqdnQifQ.qBmWTIPpRnypiDI-pIUxzg'
        }).addTo(mymap);
        
        var markers = [];
        $.each(data, function(index, value) {
            marker = L.marker([value.latitude, value.longitude]).addTo(mymap);
            popupContent = '<table class="table table-bordered table-striped"><tbody>'+
                '<tr><th>Custodian</th><td>'+value.custodian+'</td></tr>'+
                '<tr><th>Intervention</th><td>'+value.intervention+'</td></tr>'+
                '<tr><th>Village</th><td>'+value.village.name+'</td></tr>'+
                '<tr><th>Longitude</th><td>'+value.longitude+'</td></tr>'+
                '<tr><th>Latitude</th><td>'+value.latitude+'</td></tr>'+
                '</tbody></table>';
            marker.bindPopup(popupContent)
        })
    });
</script>
@endsection