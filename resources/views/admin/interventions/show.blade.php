@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.interventions.title') }}
    </div>
    <div class="form-group">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('cruds.interventions.fields.id') }}
                    </th>
                    <td>
                        {{ $intervention->id }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.interventions.fields.custodian') }}
                    </th>
                    <td>
                        {{ $intervention->custodian }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.interventions.fields.name') }}
                    </th>
                    <td>
                        {{ $intervention->intervention }}
                    </td>
                </tr>
                <tr>
                   <th>
                    {{ trans('cruds.interventions.fields.latitude') }}
                </th>
                <td>
                    {{ $intervention->latitude }}
                </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.interventions.fields.longitude') }}
                    </th>
                    <td>
                        {{ $intervention->longitude }}
                    </td>
                </tr>
                <tr> 
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.village.title_singular') }}
                    </th>
                    <td>
                        @if ($intervention->village)
                            @can('village_show')
                            <a href="{{ route('admin.villages.show', $intervention->village->id) }}">
                                {{ ($intervention->village->name ?? '') . ' (' . ($intervention->village->ward->name ?? '') . ')' }}
                            </a>
                            @else
                            {{ ($intervention->village->name ?? '') . ' (' . ($intervention->village->ward->name ?? '') . ')' }}
                            @endcan
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.interventions.fields.district') }}
                    </th>
                    <td>
                       {{ $intervention->district }}
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
            <a class="btn btn-default" href="{{ route('admin.interventions.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </div>
</div>

@endsection