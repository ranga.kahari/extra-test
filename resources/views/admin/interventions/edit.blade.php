@extends('layouts.admin')
@section('content')

<div class="card">
    <form method="POST" action="{{ route("admin.interventions.update", [$intervention->id]) }}" enctype="multipart/form-data">
       @method('PUT')
       @csrf
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.interventions.title_singular') }}
    </div>

        <div class="card-body">
            <div class="form-group row">
                <label class="col-md-3 col-form-label required"
                    for="village_id">{{ trans('cruds.village.title_singular') }}</label>
                <div class="col-md-9">
                    <select name="village_id" id="village" class="form-control select2-ajax-search"
                        ajax-search-url="{{ route('admin.villages.search') }}" required>
                        <option value="{{ $intervention->village_id  }}">
                            {{ $intervention->village->name ?? '' }}
                        </option>
                    </select>
                    @if($errors->has('village'))
                    <div class="invalid-feedback">
                        {{ $errors->first('village') }}
                    </div>
                    @endif
                </div>
            </div>
            <div style="display: none;" id="ward-details"
                class="form-group row {{ $errors->has('ward_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="ward">{{ trans('cruds.village.fields.ward') }}*</label>
                <div class="col-md-9">
                    <select name="ward_id" id="ward" class="form-control select2-ajax-search"
                        ajax-search-url="{{ route('admin.wards.search') }}">
                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                    </select>
                    @if($errors->has('ward_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('ward_id') }}
                    </em>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="required col-md-3 col-form-label"
                    for="custodian">{{ trans('cruds.interventions.fields.custodian') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('custodian') ? 'is-invalid' : '' }}" type="text"
                        name="custodian" id="custodian" value="{{ old('custodian', $intervention->custodian) }}" required>
                    @if($errors->has('custodian'))
                    <div class="invalid-feedback">
                        {{ $errors->first('custodian') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label
                    class="required col-md-3 col-form-label">{{ trans('cruds.interventions.title_singular') }}</label>
                <div class="col-md-9">
                    <select class="select2 form-control {{ $errors->has('intervention') ? 'is-invalid' : '' }}"
                        name="intervention" id="intervention" required>
                        <option value="">Select Mapped Intervention</option>
                        @foreach(App\Models\Intervention::INTERVENTION_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('intervention', $intervention->intervention) === (string) $key ? 'selected' : '' }}>
                            {{ $label }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('intervention'))
                    <div class="invalid-feedback">
                        {{ $errors->first('intervention') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="latitude">{{ trans('cruds.interventions.fields.latitude') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('latitude') ? 'is-invalid' : '' }}" type="number"
                        name="latitude" id="latitude" value="{{ old('latitude', $intervention->latitude) }}" step="0.0000000000000001">
                    @if($errors->has('latitude'))
                    <div class="invalid-feedback">
                        {{ $errors->first('latitude') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="longitude">{{ trans('cruds.interventions.fields.longitude') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('longitude') ? 'is-invalid' : '' }}" type="number"
                        name="longitude" id="longitude" value="{{ old('longitude', $intervention->longitude) }}" step="0.0000000000000001">
                    @if($errors->has('longitude'))
                    <div class="invalid-feedback">
                        {{ $errors->first('longitude') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
            <a class="btn btn-default" href="{{ route('admin.interventions.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </form>

</div>
@endsection

@section('scripts')
@parent
<script>
    $(document).ready(function(){
        $('.select2-ajax-search').on('select2:select', function (e) {
            if ('village' == $(e.target).attr('id')) {
                if (e.params.data) {
                    $('#ward-details').find('#ward').attr('required', 'required');
                    $('#ward-details').show();
                } else {
                    $('#ward-details').hide();
                    $('#ward-details').find('#ward').removeAttr('required');
                }
            }
        });
    });
</script>
@endsection