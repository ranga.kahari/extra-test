@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.isals.title_singular') }}
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        Details
                    </div>

                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-md-4">{{ trans('cruds.group.fields.id') }}</dt>
                            <dd class="col-md-8">{{ $group->id }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.group.fields.district') }}</dt>
                            <dd class="col-md-8">{{ $group->group_district }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.group.fields.group_number') }}</dt>
                            <dd class="col-md-8">{{ $group->group_number }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.group.fields.group_categories') }}</dt>
                            <dd class="col-md-8">
                                {{ App\Models\Group::GROUP_CATEGORIES_SELECT[$group->group_categories] ?? '' }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.isals.fields.isal_group_category') }}</dt>
                            <dd class="col-md-8">{{ $group->isals_categories }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.isals.fields.group_name') }}</dt>
                            <dd class="col-md-8">{{ $group->name }}</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>

        @can('attendance_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.isals.members.create", ['group_id' => $group->id]) }}">
                    {{ trans('global.add') }} {{ trans('cruds.member.title') }}
                </a>

            </div>
        </div>
        @endcan

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header" id="people-heading">
                        Members
                        <div class="card-header-actions">
                            <a class="card-header-action" href="#" data-toggle="collapse" data-target="#people-data"
                                aria-expanded="true">
                                <small class="text-muted">show/hide</small>
                            </a>
                        </div>
                    </div>

                    <div class="collapse show" id="people-data" role="tabpanel" aria-labelledby="people-heading">
                        <div class="card-body">
                            <div class="row">
                                {{-- @foreach ($genderCounts as $genderKPI)
                                                    <div class="col-sm-6 col-md-{{ ceil(12 / ($loop->count)) }}">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-value">{{ $genderKPI->count }}</div>
                                        <small class="text-muted text-uppercase font-weight-bold">
                                            {{ $genderKPI->name . ' - ' . $genderKPI->percent . '%' }}
                                        </small>
                                        <div class="progress progress-xs mt-3 mb-0">
                                            <div class="progress-bar bg-info" role="progressbar"
                                                style="width: {{ $genderKPI->percent }}%"
                                                aria-valuenow="{{ $genderKPI->percent }}" aria-valuemin="0"
                                                aria-valuemax="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach --}}
                        </div>
                        @includeIf('admin.isals.relationships.beneficiaries', ['members' => $group->members])
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <a class="btn btn-default" href="{{ route('admin.isals.index') }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
</div>
</div>

@endsection