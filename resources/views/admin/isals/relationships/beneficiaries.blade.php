<table class=" table table-bordered table-striped table-hover datatable datatable-groupsBeneficiaries">
    <thead>
        <tr>
            <th width="10">

            </th>
            <th>
                #
            </th>
            <th>
                {{ trans('cruds.beneficiary.fields.first_name') }}
            </th>
            <th>
                {{ trans('cruds.beneficiary.fields.surname') }}
            </th>
            <th>
                {{ trans('cruds.beneficiary.fields.gender') }}
            </th>
            <th>
                {{ trans('cruds.beneficiary.fields.year_of_birth') }}
            </th>
            <th>
                {{ trans('cruds.beneficiary.fields.phone_number') }}
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($members as $key => $member)
        <tr data-entry-id="{{ $member->id }}">
            <td>

            </td>
            <td>
                {{ $loop->iteration }}
            </td>
            <td>
                {{ $member->beneficiary->first_name ?? '' }}
            </td>
            <td>
                {{ $member->beneficiary->surname ?? '' }}
            </td>
            <td>
                {{ App\Models\Beneficiary::GENDER_SELECT[$member->beneficiary->gender] ?? '' }}
            </td>
            <td>
                {{ $member->beneficiary->year_of_birth ?? '' }}
            </td>
            <td>
                {{ $member->beneficiary->phone_number ?? '' }}
            </td>
            <td>
                @can('beneficiary_show')
                <a class="btn btn-xs btn-primary"
                    href="{{ route('admin.beneficiaries.show', $member->beneficiary->id) }}">
                    {{ trans('global.view') }}
                </a>
                @endcan

                @can('beneficiary_edit')
                <a class="btn btn-xs btn-info" href="{{ route('admin.beneficiaries.edit', $member->beneficiary->id) }}">
                    {{ trans('global.edit') }}
                </a>
                @endcan

                @can('beneficiary_delete')
                <form action="{{ route('admin.members.destroy', $member->id) }}" method="POST"
                    onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                </form>
                @endcan

            </td>

        </tr>
        @endforeach
    </tbody>
</table>