@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.isals.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.isals.store") }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.group.fields.group_categories') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('group_categories') ? 'is-invalid' : '' }}" type="text"
                        name="group_categories" id="group_categories"
                        value="{{ App\Models\Group::GROUP_CATEGORIES_SELECT['ISALS Group'] }}">
                    @if($errors->has('group_categories'))
                    <div class="invalid-feedback">
                        {{ $errors->first('group_categories') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.isals.fields.isal_group_category') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('isals_categories') ? 'is-invalid' : '' }}"
                        name="isals_categories" id="isals_categories">
                        <option value disabled {{ old('isals_categories', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach (App\Models\Group::ISAL_GROUP_CATEGORY as $key => $isals_categories)
                        {{-- <option value="{{ $key }}">{{ $isals_categories }}</option> --}}
                        <option value="{{ $key }}" {{ old('isals_categories', $group->isals_categories) === (string) $key ? 'selected' : '' }}>
                            {{ $isals_categories }}</option>
                        
                        @endforeach
                    </select>
                    @if($errors->has('isals_categories'))
                    <div class="invalid-feedback">
                        {{ $errors->first('isals_categories') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">{{ trans('cruds.isals.fields.group_name') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name"
                        id="name" value="{{ old('name', $group->name) }}">
                    @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="card-footer">
                <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
                <button class="btn btn-danger" type="reset"> Reset</button>
                <a class="btn btn-default" href="{{ route('admin.isals.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </form>
    </div>
</div>
@endsection