@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.trainingCategory.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.training-categories.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="thematic_area_id">{{ trans('cruds.trainingCategory.fields.thematic_area') }}</label>
                <select class="form-control select2 {{ $errors->has('thematic_area') ? 'is-invalid' : '' }}" name="thematic_area_id" id="thematic_area_id">
                    @foreach($thematic_areas as $id => $thematic_area)
                        <option value="{{ $id }}" {{ old('thematic_area_id') == $id ? 'selected' : '' }}>{{ $thematic_area }}</option>
                    @endforeach
                </select>
                @if($errors->has('thematic_area'))
                    <div class="invalid-feedback">
                        {{ $errors->first('thematic_area') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.trainingCategory.fields.thematic_area_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.trainingCategory.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.trainingCategory.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection