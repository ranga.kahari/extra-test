@extends('layouts.admin')
@section('content')

<div class="card">
    <form method="POST" action="{{ route("admin.smart-subsidies.store") }}" enctype="multipart/form-data">
        @csrf
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.smartSubsidies.title_singular') }}
        </div>

        <div class="card-body">

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.smartSubsidies.fields.district') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('ward_id') ? 'is-invalid' : '' }}" name="ward_id"
                        id="ward_id">
                        <option value disabled {{ old('ward_id', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach($wards as $id => $ward)
                        <option value="{{ old('wards', $id ?? null) }}">{{ $ward }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('ward_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('ward_id') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="required col-md-3 col-form-label"
                    for="custodian">{{ trans('cruds.smartSubsidies.fields.custodian') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('custodian') ? 'is-invalid' : '' }}" type="text"
                        name="custodian" id="custodian" value="{{ old('custodian', '') }}" required>
                    @if($errors->has('custodian'))
                    <div class="invalid-feedback">
                        {{ $errors->first('custodian') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="males">{{ trans('cruds.smartSubsidies.fields.male') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('males') ? 'is-invalid' : '' }}" type="number"
                        name="males" id="males" value="{{ old('males', '') }}" step="1">
                    @if($errors->has('males'))
                    <div class="invalid-feedback">
                        {{ $errors->first('males') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="females">{{ trans('cruds.smartSubsidies.fields.female') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('females') ? 'is-invalid' : '' }}" type="number"
                        name="females" id="females" value="{{ old('females', '') }}" step="1">
                    @if($errors->has('females'))
                    <div class="invalid-feedback">
                        {{ $errors->first('females') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.total') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('total') ? 'is-invalid' : '' }}" type="number"
                        name="total" id="total" value="{{ old('total', '') }}" step="1">
                    @if($errors->has('total'))
                    <div class="invalid-feedback">
                        {{ $errors->first('total') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.smartSubsidies.fields.valuechain') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('value_chain') ? 'is-invalid' : '' }}"
                        name="value_chain" id="value_chain">
                        <option value disabled {{ old('value_chain', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach (App\Models\SmartSubsidies::ValueChain as $key => $value_chain)
                        <option value="{{ $key }}">{{ $value_chain }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('value_chain'))
                    <div class="invalid-feedback">
                        {{ $errors->first('value_chain') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.farmer_contribution_usd') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('farmer_contribution_value_usd') ? 'is-invalid' : '' }}"
                        type="number" name="farmer_contribution_value_usd" id="farmer_contribution_value_usd"
                        value="{{ old('farmer_contribution_value_usd', '')  }}" step="any">
                    @if($errors->has('farmer_contribution_value_usd'))
                    <div class="invalid-feedback">
                        {{ $errors->first('farmer_contribution_value_usd') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.farmer_contribution_rtgs') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('farmer_contribution_value_rtgs') ? 'is-invalid' : '' }}"
                        type="number" name="farmer_contribution_value_rtgs" id="farmer_contribution_value_rtgs"
                        value="{{ old('farmer_contribution_value_rtgs', '')  }}" step="any">
                    @if($errors->has('farmer_contribution_value_rtgs'))
                    <div class="invalid-feedback">
                        {{ $errors->first('farmer_contribution_value_rtgs') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.extra_contribution') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('extra_contribution') ? 'is-invalid' : '' }}"
                        type="number" name="extra_contribution" id="extra_contribution"
                        value="{{ old('extra_contribution', '')  }}" step="any">
                    @if($errors->has('extra_contribution'))
                    <div class="invalid-feedback">
                        {{ $errors->first('extra_contribution') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.subsidy_allocated') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('subsidy_allocated_per_hh') ? 'is-invalid' : '' }}"
                        type="number" name="subsidy_allocated_per_hh" id="subsidy_allocated_per_hh"
                        value="{{ old('subsidy_allocated_per_hh', '')  }}" step="any">
                    @if($errors->has('subsidy_allocated_per_hh'))
                    <div class="invalid-feedback">
                        {{ $errors->first('subsidy_allocated_per_hh') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.collected') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('collected') ? 'is-invalid' : '' }}" type="number"
                        name="collected" id="collected" value="{{ old('collected', '')  }}" step="any">
                    @if($errors->has('collected'))
                    <div class="invalid-feedback">
                        {{ $errors->first('collected') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="total">{{ trans('cruds.smartSubsidies.fields.outstanding') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('outstanding') ? 'is-invalid' : '' }}" type="number"
                        name="outstanding" id="outstanding" value="{{ old('outstanding', '')  }}" step="any">
                    @if($errors->has('outstanding'))
                    <div class="invalid-feedback">
                        {{ $errors->first('outstanding') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.smartSubsidies.fields.status') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status"
                        id="status">
                        <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach (App\Models\smartSubsidies::Status as $key => $status)
                        <option value="{{ $key }}">{{ $status }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('status') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-success" type="submit"> {{ trans('global.save') }}</button>
            <button class="btn btn-danger" type="reset"> Reset</button>
            <a class="btn btn-default" href="{{ route('admin.smart-subsidies.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
</div>
@endsection