@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.smartSubsidies.title') }}
    </div>
    <div class="form-group">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.id') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->id }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.ward') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->ward->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.custodian') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->custodian }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.male') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->males }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.female') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->females }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.total') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->total }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.valuechain') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->value_chain }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.farmer_contribution_usd') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->farmer_contribution_value_usd ?? null }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.farmer_contribution_rtgs') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->farmer_contribution_value_rtgs ?? null}}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.extra_contribution') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->extra_contribution }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.subsidy_allocated') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->subsidy_allocated_per_hh }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.supplier') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->supplier ?? null }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.officer') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->officer }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.collected') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->collected }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.outstanding') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->outstanding }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('cruds.smartSubsidies.fields.status') }}
                    </th>
                    <td>
                        {{ $smartSubsidies->status }}
                    </td>
                </tr>

            </tbody>
        </table>
        <div class="form-group">
            <a class="btn btn-default" href="{{ route('admin.smart-subsidies.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </div>
</div>
@endsection