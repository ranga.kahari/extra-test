@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.thematicArea.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.thematic-areas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.thematicArea.fields.id') }}
                        </th>
                        <td>
                            {{ $thematicArea->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.thematicArea.fields.name') }}
                        </th>
                        <td>
                            {{ $thematicArea->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.thematicArea.fields.label') }}
                        </th>
                        <td>
                            {{ $thematicArea->label }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.thematicArea.fields.slug') }}
                        </th>
                        <td>
                            {{ $thematicArea->slug }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.thematicArea.fields.show_in_side_menu') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $thematicArea->show_in_side_menu ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.thematicArea.fields.order') }}
                        </th>
                        <td>
                            {{ $thematicArea->order }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.thematic-areas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endsection