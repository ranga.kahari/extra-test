@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.thematicArea.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.thematic-areas.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.thematicArea.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.thematicArea.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="label">{{ trans('cruds.thematicArea.fields.label') }}</label>
                <input class="form-control {{ $errors->has('label') ? 'is-invalid' : '' }}" type="text" name="label" id="label" value="{{ old('label', '') }}" required>
                @if($errors->has('label'))
                    <div class="invalid-feedback">
                        {{ $errors->first('label') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.thematicArea.fields.label_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="slug">{{ trans('cruds.thematicArea.fields.slug') }}</label>
                <input class="form-control {{ $errors->has('slug') ? 'is-invalid' : '' }}" type="text" name="slug" id="slug" value="{{ old('slug', '') }}" required>
                @if($errors->has('slug'))
                    <div class="invalid-feedback">
                        {{ $errors->first('slug') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.thematicArea.fields.slug_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('show_in_side_menu') ? 'is-invalid' : '' }}">
                    <input class="form-check-input" type="checkbox" name="show_in_side_menu" id="show_in_side_menu" value="1" required {{ old('show_in_side_menu', 0) == 1 || old('show_in_side_menu') === null ? 'checked' : '' }}>
                    <label class="required form-check-label" for="show_in_side_menu">{{ trans('cruds.thematicArea.fields.show_in_side_menu') }}</label>
                </div>
                @if($errors->has('show_in_side_menu'))
                    <div class="invalid-feedback">
                        {{ $errors->first('show_in_side_menu') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.thematicArea.fields.show_in_side_menu_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="order">{{ trans('cruds.thematicArea.fields.order') }}</label>
                <input class="form-control {{ $errors->has('order') ? 'is-invalid' : '' }}" type="number" name="order" id="order" value="{{ old('order', '') }}" step="1" required>
                @if($errors->has('order'))
                    <div class="invalid-feedback">
                        {{ $errors->first('order') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.thematicArea.fields.order_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection