@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        {{ trans('cruds.ward.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Ward">
            <thead>
                <tr>
                    <th width="10"></th>
                    <th>{{ trans('cruds.ward.fields.id') }}</th>
                    <th>{{ trans('cruds.ward.fields.district') }}</th>
                    <th>{{ trans('cruds.ward.fields.ward_number') }}</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
  
        let dtOverrideGlobals = {
            buttons: dtButtons,
            processing: true,
            serverSide: true,
            retrieve: true,
            aaSorting: [],
            ajax: "{{ route('admin.wards.index') }}",
            columns: [
                { data: 'placeholder', name: 'placeholder' },
                { data: 'id', name: 'id' },
                { data: 'district', name: 'district' },
                { data: 'ward_number', name: 'ward_number' },
                { data: 'actions', name: '{{ trans('global.actions') }}' }
            ],
            orderCellsTop: true,
            order: [[ 1, 'asc' ]],
            pageLength: 100,
        };
        let table = $('.datatable-Ward').DataTable(dtOverrideGlobals);
    });
</script>
@endsection