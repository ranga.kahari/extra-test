@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.yieldData.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.id') }}
                        </th>
                        <td>
                            {{ $crop->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.district') }}
                        </th>
                        <td>
                            {{ $crop->district }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.season') }}
                        </th>
                        <td>
                            {{ $crop->season }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.value_chain') }}
                        </th>
                        <td>
                            {{ $crop->value_chain }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.area_planted') }}
                        </th>
                        <td>
                            {{ $crop->area_planted }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.production') }}
                        </th>
                        <td>
                            {{ $crop->production }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.yield') }}
                        </th>
                        <td>
                            {{ $crop->yield }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.yieldData.fields.beneficiary_name') }}
                        </th>
                        <td>
                            <a href="{{ route('admin.beneficiaries.show', $crop->beneficiaries[0]->id) }}">
                            {{ $crop->beneficiaries[0]->name }}
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.crops.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>



</div>

@endsection