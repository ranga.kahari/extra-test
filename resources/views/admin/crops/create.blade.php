@extends('layouts.admin')
@section('content')

<div class="card">

    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.yieldData.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.crops.store") }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="season">{{ trans('cruds.yieldData.fields.season') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('season') ? 'is-invalid' : '' }}" type="number"
                        name="season" id="season" value="{{ old('season', '') }}" step="1">
                    @if($errors->has('year_of_birth'))
                    <div class="invalid-feedback">
                        {{ $errors->first('season') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.yieldData.fields.value_chain') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('value_chain') ? 'is-invalid' : '' }}"
                        name="value_chain" id="value_chain">
                        <option value disabled {{ old('value_chain', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach(App\Models\Crop::ValueChain as $key => $label)
                        <option value="{{ $key }}" {{ old('value_chain', '') === (string) $key ? 'selected' : '' }}>
                            {{ $label }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('value_chain'))
                    <div class="invalid-feedback">
                        {{ $errors->first('value_chain') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="area_planted">{{ trans('cruds.yieldData.fields.area_planted') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('area_planted') ? 'is-invalid' : '' }}" type="number"
                        name="area_planted" id="area_planted" value="{{ old('area_planted', '') }}"
                        step="0.0000000000000001">
                    @if($errors->has('area_planted'))
                    <div class="invalid-feedback">
                        {{ $errors->first('area_planted') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="yield">{{ trans('cruds.yieldData.fields.yield') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('yield') ? 'is-invalid' : '' }}" type="number"
                        name="yield" id="yield" value="{{ old('yield', '') }}" step="0.0000000000000001">
                    @if($errors->has('yield'))
                    <div class="invalid-feedback">
                        {{ $errors->first('yield') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="yield">{{ trans('cruds.yieldData.fields.production') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('production') ? 'is-invalid' : '' }}" type="number"
                        name="production" id="production" value="{{ old('production', '') }}" step="0.0000000000000001">
                    @if($errors->has('yield'))
                    <div class="invalid-feedback">
                        {{ $errors->first('production') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row {{ $errors->has('beneficiary_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="beneficiary">Beneficiary </label>
                <div class="col-md-9">
                    <select name="beneficiary_id" id="attendances" class="form-control select2-ajax-search"
                        ajax-search-url="{{ route('admin.beneficiaries.search') }}"></select>
                </div>
            </div>
            {{-- @include('admin.yieldData.add_beneficiaries_partial') --}}

            <div class="card-footer">
                <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
                <button class="btn btn-danger" type="reset"> Reset</button>
                <a class="btn btn-default" href="{{ route('admin.crops.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </form>

    </div>
</div>
@endsection