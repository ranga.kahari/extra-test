@extends('layouts.admin')

@section('content')

@can('crop_create')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route('admin.crops.create') }}">
            {{ trans('global.add') }} {{ trans('cruds.yieldData.title_singular') }}
        </a>
        <a class="btn btn-warning" href="#" data-toggle="modal" data-target="#csvImportModal">
            Import {{ trans('cruds.yieldData.title') }} {{ trans('cruds.importActivity.title') }}
        </a>
        
        @include('admin.crops.import')
        
        <a class="btn btn-primary" href="{{ route('admin.crops.export') }}">
            Export all to Excel
        </a>
       
    </div>
</div>

<div class="card">
    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Crop">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.yieldData.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.yieldData.fields.season') }}
                    </th>
                    <th>
                        {{ trans('cruds.yieldData.fields.value_chain') }}
                    </th>
                    <th>
                        {{ trans('cruds.yieldData.fields.area_planted') }}
                    </th>
                    <th>
                        {{ trans('cruds.yieldData.fields.yield') }}
                    </th>
                    <th>

                    </th>

                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>
                    </td>
                </tr>
            </thead>
        </table>
    </div>
</div>

</div>
@endcan

@endsection



@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('yielddata_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.crops.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.crops.index') }}",
    columns: [
    { data: 'placeholder', name: 'placeholder' },
    { data: 'id', name: 'id' },
    { data: 'season', name: 'season' },
    { data: 'value_chain', name: 'value_chain' },
    { data: 'area_planted', name: 'area_planted' },
    { data: 'yield', name: 'yield' },
    { data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-Crop').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
});

</script>
@endsection