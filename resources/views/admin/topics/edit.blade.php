@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.topic.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.topics.update", [$topic->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="training_category_id">{{ trans('cruds.topic.fields.training_category') }}</label>
                <select class="form-control select2 {{ $errors->has('training_category_id') ? 'is-invalid' : '' }}" name="training_category_id" id="training_category_id" required>
                    @foreach($training_categories as $id => $training_category)
                        <option value="{{ $id }}" {{ ($topic->trainingCategory ? $topic->trainingCategory->id : old('training_category_id')) == $id ? 'selected' : '' }}>{{ $training_category }}</option>
                    @endforeach
                </select>
                @if($errors->has('training_category_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('training_category_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.topic.fields.training_category_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.topic.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $topic->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.topic.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.topic.fields.description') }}</label>
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description', $topic->description) }}</textarea>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.topic.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection