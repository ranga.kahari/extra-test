@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.ageBand.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.age-bands.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.ageBand.fields.id') }}
                        </th>
                        <td>
                            {{ $ageBand->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ageBand.fields.name') }}
                        </th>
                        <td>
                            {{ $ageBand->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ageBand.fields.lower_limit') }}
                        </th>
                        <td>
                            {{ $ageBand->lower_limit }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ageBand.fields.upper_limit') }}
                        </th>
                        <td>
                            {{ $ageBand->upper_limit }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.age-bands.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection