@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.ageBand.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.age-bands.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.ageBand.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ageBand.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="lower_limit">{{ trans('cruds.ageBand.fields.lower_limit') }}</label>
                <input class="form-control {{ $errors->has('lower_limit') ? 'is-invalid' : '' }}" type="number" name="lower_limit" id="lower_limit" value="{{ old('lower_limit', '') }}" step="1" required>
                @if($errors->has('lower_limit'))
                    <div class="invalid-feedback">
                        {{ $errors->first('lower_limit') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ageBand.fields.lower_limit_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="upper_limit">{{ trans('cruds.ageBand.fields.upper_limit') }}</label>
                <input class="form-control {{ $errors->has('upper_limit') ? 'is-invalid' : '' }}" type="number" name="upper_limit" id="upper_limit" value="{{ old('upper_limit', '') }}" step="1">
                @if($errors->has('upper_limit'))
                    <div class="invalid-feedback">
                        {{ $errors->first('upper_limit') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ageBand.fields.upper_limit_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection