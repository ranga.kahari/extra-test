@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.valueChain.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.value-chains.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.valueChain.fields.id') }}
                        </th>
                        <td>
                            {{ $valueChain->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.valueChain.fields.name') }}
                        </th>
                        <td>
                            {{ $valueChain->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.valueChain.fields.categories') }}
                        </th>
                        <td>
                            {{ App\Models\ValueChain::CATEGORIES_SELECT[$valueChain->categories] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.value-chains.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endsection