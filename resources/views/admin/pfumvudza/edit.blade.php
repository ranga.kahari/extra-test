@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.pfumvudza.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.pfumvudza.update", [$pfumvudza->id]) }}" enctype="multipart/form-data">
           @method('PUT')
            @csrf

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.pfumvudza.fields.agronomic_practice') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('agronomic_practice') ? 'is-invalid' : '' }}"
                        name="agronomic_practice" id="agronomic_practice">
                        <option value disabled {{ old('agronomic_practice', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach(App\Models\Pfumvudza::AGRONOMIC_PRACTICE as $key => $label)
                        <option value="{{ $key }}"
                            {{ old('agronomic_practice', $pfumvudza->agronomic_practice) === (string) $key ? 'selected' : '' }}>
                            {{ $label }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('agronomic_practice'))
                    <div class="invalid-feedback">
                        {{ $errors->first('agronomic_practice') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.pfumvudza.fields.value_chain') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('value_chain') ? 'is-invalid' : '' }}"
                        name="value_chain" id="value_chain">
                        <option value disabled {{ old('value_chain', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach(App\Models\Pfumvudza::ValueChain as $key => $label)
                        <option value="{{ $key }}" {{ old('value_chain', $pfumvudza->value_chain) === (string) $key ? 'selected' : '' }}>
                            {{ $label }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('value_chain'))
                    <div class="invalid-feedback">
                        {{ $errors->first('value_chain') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="variety">{{ trans('cruds.pfumvudza.fields.variety') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('variety') ? 'is-invalid' : '' }}" type="text"
                        name="variety" id="variety" value="{{ old('variety', $pfumvudza->variety) }}">
                    @if($errors->has('variety'))
                    <div class="invalid-feedback">
                        {{ $errors->first('variety') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="area_planted">{{ trans('cruds.yieldData.fields.area_planted') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('area_planted') ? 'is-invalid' : '' }}" type="number"
                        name="area_planted" id="area_planted" value="{{ old('area_planted', $pfumvudza->area_planted) }}"
                        step="0.0000000000000001">
                    @if($errors->has('area_planted'))
                    <div class="invalid-feedback">
                        {{ $errors->first('area_planted') }}
                    </div>
                    @endif
                </div>
            </div>
            
            <div class="form-group row {{ $errors->has('beneficiary_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="beneficiary">Beneficiary </label>
                <div class="col-md-9">
                    <input class="form-control" type="text" disabled
                        value="{{ old('beneficiary_id',$pfumvudza->beneficiaries[0]['name']) }}">
                </div>
            </div>

            <div class="form-group row {{ $errors->has('beneficiary_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="beneficiary">Beneficiary </label>
                <div class="col-md-9">
                    <select name="beneficiary_id" id="attendances" class="form-control select2-ajax-search"
                        ajax-search-url="{{ route('admin.beneficiaries.search') }}"></select>
                </div>
            </div>
            {{-- @include('admin.yieldData.add_beneficiaries_partial') --}}
            
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="facilitators">{{ trans('cruds.group.fields.facilitators') }}</label>
                <div class="col-md-9">
                    <table class="table table-bordered table-striped" id="facilitators_table">
                        <thead>
                            <tr>
                                <th>@lang('cruds.facilitator.fields.name')</th>
                                <th>@lang('cruds.group.fields.position')</th>
            
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody id="facilitators_tbody">
                            @foreach ($pfumvudza->facilitators as $key => $item)
                            <tr>
                                <td>
                                    <input type="text" class="form-control" value="{{ $item->name }}" disabled="">
                                </td>
                                <td>
                                    <input type="text" name="facilitators[{{ $item->id }}][position]" class="form-control"
                                        value="{{ $item->pivot->position }}">
                                </td>
                                <td>
                                    <a href="#" class="remove btn btn-xs btn-danger">{{ trans('global.delete') }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#facilitatorsModal">
                        {{ trans('global.add')}}
                    </button>
                </div>
            </div>
            

            <div class="card-footer">
                <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
                <button class="btn btn-danger" type="reset"> Reset</button>
                <a class="btn btn-default" href="{{ route('admin.pfumvudza.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </form>

    </div>
</div>

@include('admin.pfumvudza.modals.facilitators')

@endsection

@section('scripts')
@parent
<script>
    $(document).ready(function(){
        $('.modal-select').on('select2:select', function (e) {
            var selected_id = $(e.target).attr('id');
            var tableBody = $('#' + selected_id + '_tbody');
            var lastIndex = parseInt(tableBody.find('tr').last().data('index'));

            if (isNaN(lastIndex)) {
                lastIndex = 0;
            }

            if ('facilitators' == selected_id) {
                addFacilitatorDataToTable(tableBody, lastIndex + 1, e.params.data);
            }

            return false;
        });

        $(document).on('click', '.remove', function () {
            var row = $(this).parentsUntil('tr').parent();
            row.remove();
            return false;
        });

        function addFacilitatorDataToTable(table_body, index, data) {
            console.log(data);
            table_body.append(
                '<tr data-index=' + index + '>' +
                    '<td>' +
                        '<input type="text" class="form-control" value="' + (data.newTag ? data.id : data.name) + '" disabled>' +
                    '</td>' +
                    ' <td>' +
                        '<input type="text" name="facilitators[' + data.id + '][position]" class="form-control" value="Facilitator">' +
                    '</td>' +
                    '<td>' +
                        '<a href="#" class="remove btn btn-xs btn-danger">{!! trans('global.delete') !!}</a>' +
                    '</td>' +
                '</tr>'
            );
        }
    });
</script>
@endsection