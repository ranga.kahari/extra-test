@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.pfumvudza.title') }}
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        Details
                    </div>
        
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-md-4">{{ trans('cruds.yieldData.fields.id') }}</dt>
                            <dd class="col-md-8">{{ $pfumvudza->id }}</dd>
        
                            <dt class="col-md-4">{{ trans('cruds.pfumvudza.fields.district') }}</dt>
                            <dd class="col-md-8">{{ $pfumvudza->district }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.pfumvudza.fields.agronomic_practice') }}</dt>
                            <dd class="col-md-8">{{ $pfumvudza->agronomic_practice }}</dd>
        
                            <dt class="col-md-4">{{ trans('cruds.pfumvudza.fields.value_chain') }}</dt>
                            <dd class="col-md-8">{{ $pfumvudza->value_chain }}</dd>
        
                            <dt class="col-md-4">{{ trans('cruds.pfumvudza.fields.variety') }}</dt>
                            <dd class="col-md-8"> {{ $pfumvudza->variety }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.pfumvudza.fields.area_planted') }}</dt>
                            <dd class="col-md-8">{{ $pfumvudza->area_planted }}</dd>
        
                            <dt class="col-md-4">{{ trans('cruds.pfumvudza.fields.beneficiary_name') }}</dt>
                            <dd class="col-md-8">{{ $pfumvudza->beneficiaries[0]->name }}</dd>
        
                            <dt class="col-md-4">{{ trans('cruds.group.fields.facilitators') }}</dt>
                            <dd class="col-md-8">
                                <table class="table table-bordered table-striped table-sm" id="groups_table">
                                    <thead>
                                        <tr>
                                            <th>@lang('cruds.group.fields.name')</th>
                                            <th>@lang('cruds.beneficiary.fields.position')</th>
                                        </tr>
                                    </thead>
                                    <tbody id="groups_tbody">
                                        @foreach ($pfumvudza->facilitators as $item)
                                        <tr>
                                            <td>
                                                @can('facilitator_show')
                                                <a href="{{ route('admin.facilitators.show', $item->id) }}">
                                                    {{ $item->name ?? '' }}
                                                </a>
                                                @else
                                                {{ $item->name ?? '' }}
                                                @endcan
                                            </td>
                                            <td>{{ $item->pivot->position }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </dd>
                        </dl>
                    </div>
                </div>
                
                <div class="form-group">
                    <a class="btn btn-default" href="{{ route('admin.pfumvudza.index') }}">
                        {{ trans('global.back_to_list') }}
                    </a>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection