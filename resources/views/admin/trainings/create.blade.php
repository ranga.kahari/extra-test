@extends('layouts.admin')
@section('content')

<div class="card">
    <form method="POST" action="{{ route("admin.trainings.store") }}" enctype="multipart/form-data">
        @csrf
        
        <div class="card-header">
            {{ trans('global.create') }} {{ trans('cruds.training.title_singular') }}
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            Main Details - 1
                        </div>

                        <div class="card-body">
                            <div class="form-group row {{ $errors->has('training_code') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="venue">{{ trans('cruds.training.fields.training_code') }}*</label>
                                <div class="col-md-9">
                                    <input type="text" id="training_code" name="training_code" class="form-control"
                                        value="{{ old('training_code', isset($training) ? $training->training_code : '') }}">
                                    @if($errors->has('training_code'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('training_code') }}
                                    </em>
                                    @endif
                                </div>
                            </div>
                            
                            
                            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="name">{{ trans('cruds.training.fields.name') }}*</label>
                                <div class="col-md-9">
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($training) ? $training->name : '') }}" required>
                                    @if($errors->has('name'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('name') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('village_id') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="village">{{ trans('cruds.training.fields.village') }}</label>
                                <div class="col-md-9">
                                    <select name="village_id" id="village" class="form-control select2-ajax-search" ajax-search-url="{{ route('admin.villages.search') }}">
                                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                                    </select>
                                    @if($errors->has('village_id'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('village_id') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div style="display: none;" id="ward-details" class="form-group row {{ $errors->has('ward_id') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="ward">{{ trans('cruds.village.fields.ward') }}*</label>
                                <div class="col-md-9">
                                    <select name="ward_id" id="ward" class="form-control select2-ajax-search" ajax-search-url="{{ route('admin.wards.search') }}">
                                        <option value="">{{ trans('global.pleaseSelect') }}</option>
                                    </select>
                                    @if($errors->has('ward_id'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('ward_id') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('venue') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="venue">{{ trans('cruds.training.fields.venue') }}*</label>
                                <div class="col-md-9">
                                    <input type="text" id="venue" name="venue" class="form-control" value="{{ old('venue', isset($training) ? $training->venue : '') }}" required>
                                    @if($errors->has('venue'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('venue') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            Main Details - 2
                        </div>

                        <div class="card-body">
                            <div class="form-group row {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="start_date">{{ trans('cruds.training.fields.start_date') }}*</label>
                                <div class="col-md-9">
                                    <input type="text" id="start_date" name="start_date" class="form-control date" value="{{ old('start_date', isset($training) ? $training->start_date : '') }}" required>
                                    @if($errors->has('start_date'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('start_date') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="end_date">{{ trans('cruds.training.fields.end_date') }}</label>
                                <div class="col-md-9">
                                    <input type="text" id="end_date" name="end_date" class="form-control date" value="{{ old('end_date', isset($training) ? $training->end_date : '') }}">
                                    @if($errors->has('end_date'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('end_date') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('prepared_by') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="prepared_by">{{ trans('cruds.training.fields.prepared_by') }}</label>
                                <div class="col-md-9">
                                    <input type="text" id="prepared_by" name="prepared_by" class="form-control" value="{{ old('prepared_by', isset($training) ? $training->prepared_by : '') }}">
                                    @if($errors->has('prepared_by'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('prepared_by') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                           Training Details
                        </div>

                        <div class="card-body">
                            <div class="form-group row {{ $errors->has('value_chains') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="value_chains">Value chains*</label>
                                <div class="col-md-9">
                                    <select name="value_chains[]" id="value_chains" class="form-control select2" multiple="multiple" required>
                                        @foreach($value_chains as $id => $value_chain)
                                            <option value="{{ $id }}" {{ (in_array($id, old('value_chains', [])) || isset($training) && $training->value_chains->contains($id)) ? 'selected' : '' }}>{{ $value_chain }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('value_chains'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('value_chains') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('thematic_areas') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="thematic_areas">{{ trans('cruds.training.fields.thematic_areas') }}*</label>
                                <div class="col-md-9">
                                    <select name="thematic_areas[]" id="thematic_areas" class="form-control select2" multiple="multiple" required>
                                        @foreach($thematic_areas as $thematic_area)
                                            <option value="{{ $thematic_area->id }}" {{ (in_array($thematic_area->id, old('thematic_areas', [])) || isset($training) && $training->thematic_areas->contains($thematic_area->id)) ? 'selected' : '' }}>{{ $thematic_area->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('thematic_areas'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('thematic_areas') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="training_categories">Training Categories</label>
                                <div class="col-md-9">
                                    <select name="training_categories[]" id="training_categories" class="form-control select2" multiple="multiple" required>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('topics') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="topics">{{ trans('cruds.training.fields.topics') }}*</label>
                                <div class="col-md-9">
                                    <select class="form-control select2 {{ $errors->has('topics') ? 'is-invalid' : '' }}" name="topics[]" id="topics"
                                        multiple>
                                    </select>
                                    @if($errors->has('topics'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('topics') }}
                                        </em>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('facilitators') ? 'has-error' : '' }}">
                                <label class="col-md-3 col-form-label" for="facilitators">{{ trans('cruds.training.fields.facilitators') }}*</label>
                                <div class="col-md-9">
                                    <select id="facilitators" class="form-control select2-ajax-search"
                                        ajax-search-url="{{ route('admin.facilitators.search') }}" multiple="multiple"></select>
                                    @if($errors->has('facilitators'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('facilitators') }}
                                        </em>
                                    @endif
                                    <table class="mt-2 table table-bordered table-striped" id="facilitators_table">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('cruds.facilitator.fields.name') }}</th>
                                                <th>{{ trans('cruds.facilitator.fields.type') }}</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody id="facilitators_tbody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card-footer">
            <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
            <button class="btn btn-danger" type="reset"> Reset</button>
            <a class="btn btn-default" href="{{ route('admin.beneficiaries.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </form>
</div>

@endsection

@section('scripts')
@parent

<script>
    $(document).ready(function(){
        var thematic_areas = {!! json_encode($thematic_areas) !!};
        var selected_facilitator_ids = [];

        $('.select2-ajax-search').on('select2:select', function (e) {
            if ('village' == $(e.target).attr('id')) {
                if (e.params.data.newTag) {
                    $('#ward-details').find('#ward').attr('required', 'required');
                    $('#ward-details').show();
                } else {
                    $('#ward-details').hide();
                    $('#ward-details').find('#ward').removeAttr('required');
                }
            }
            if ('facilitators' == $(e.target).attr('id')) {
                var data = e.params.data;
                var tableBody = $('#facilitators_tbody');
                if ($.inArray(data.id, selected_facilitator_ids) != -1) {
                    alert(data.text + ' has been added already');
                }
                else {
                    var lastIndex = parseInt(tableBody.find('tr').last().data('index'));
                    if (isNaN(lastIndex)) {
                        lastIndex = 0;
                    }
                    
                    addFacilitatorDataToTable(tableBody, lastIndex + 1, data);
                    selected_facilitator_ids.push(data.id);
                }
                $('#facilitators').val(null).trigger('change');
                $('#facilitators').focus();
                return false;
            }
        });

        $('#thematic_areas').on('change', function (e) {
            thematic_area_ids = $.map( $('#thematic_areas').val(), function( n ) {
                return parseInt( n );
            });

            if(thematic_area_ids.length == 0) {
                $('#training_categories').val(null).trigger('change');
            }
            else {
                var training_category_options = '';
                training_category_ids = $.map( $('#training_categories').val(), function( n ) {
                    return parseInt( n );
                });
                $.each(thematic_areas, function(index, thematic_area) {
                    if ($.inArray( thematic_area.id, thematic_area_ids) != -1) {
                        training_category_options += '<optgroup label="' + thematic_area.name + '">';
                        $.each(thematic_area.training_categories, function(index, training_category) {
                            selected_string = ($.inArray( training_category.id, training_category_ids) != -1) ? 'selected' : '';
                            training_category_options += ('<option value="' + training_category.id + '" ' + selected_string + '>' + training_category.name + '</option>');
                        });
                        training_category_options += '</optgroup>';
                    }
                });
                $('#training_categories').html(training_category_options);
            }

            return false;
        });
        
        $('#training_categories').on('change', function (e) {
            thematic_area_ids = $.map( $('#thematic_areas').val(), function( n ) {
                return parseInt( n );
            });
            
            training_category_ids = $.map( $('#training_categories').val(), function( n ) {
                return parseInt( n );
            });

            if(training_category_ids.length == 0) {
                $('#topics').val(null).trigger('change');
            }
            else {
                var topic_options = '';
                topic_ids = $.map( $('#topics').val(), function( n ) {
                    return parseInt( n );
                });
                $.each(thematic_areas, function(index, thematic_area) {
                    if ($.inArray( thematic_area.id, thematic_area_ids) != -1) {
                        $.each(thematic_area.training_categories, function(index, training_category) {
                            if ($.inArray( training_category.id, training_category_ids) != -1) {
                                topic_options += '<optgroup label="' + training_category.name + '">';
                                $.each(training_category.topics, function(index, topic) {
                                    selected_string = ($.inArray( topic.id, topic_ids) != -1) ? 'selected' : '';
                                    topic_options += ('<option value="' + topic.id + '" ' + selected_string + '>' + topic.name + '</option>');
                                });
                                topic_options += '</optgroup>';
                            }
                        });
                    }
                });
                $('#topics').html(topic_options);
            }

            return false;
        });
        
        $(document).on('click', '.remove', function () {
            var row = $(this).parentsUntil('tr').parent();
            row.remove();
            return false;
        });

        function addFacilitatorDataToTable(table_body, index, data) {
            table_body.append(
                '<tr data-index=' + index + '>' +
                    '<td>' +
                        '<input type="text" class="form-control" value="' + (data.newTag ? data.id : data.name) + '" disabled>' +
                    '</td>' +
                    ' <td>' +
                        '<input type="text" name="facilitators[' + data.id + '][position]" class="form-control" value="Facilitator">' +
                    '</td>' +
                    '<td>' +
                        '<a href="#" class="remove btn btn-xs btn-danger">{!! trans('global.delete') !!}</a>' +
                    '</td>' +
                '</tr>'
            );
        }
    });
</script>
@endsection