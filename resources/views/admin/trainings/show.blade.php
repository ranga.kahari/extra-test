@extends('layouts.admin')
@section('content')

<div id="accordion">
    <div class="card">
        <div class="card-header" id="main-details-heading">
            {{ trans('global.show') }} {{ trans('cruds.training.title_singular') }} Details
            <div class="card-header-actions">
                <a class="card-header-action" href="#" data-toggle="collapse" data-target="#main-details" aria-expanded="true">
                    <small class="text-muted">show/hide</small>
                </a>
            </div>
        </div>

        <div class="collapse" id="main-details" role="tabpanel" aria-labelledby="main-details-heading" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Activity Data
                            </div>

                            <div class="card-body">
                                <dl class="row">
                                    
                                    <dt class="col-md-4">{{ trans('cruds.training.fields.training_code') }}</dt>
                                    <dd class="col-md-8">{{ $training->training_code }}</dd>
                                    
                                    <dt class="col-md-4">{{ trans('cruds.training.fields.name') }}</dt>
                                    <dd class="col-md-8">{{ $training->name ?? '' }}</dd>

                                    <dt class="col-md-4">{{ trans('cruds.village.fields.ward') }}</dt>
                                    <dd class="col-md-8">{{ $training->village->ward->name ?? '' }}</dd>
                                    
                                    <dt class="col-md-4">{{ trans('cruds.training.fields.village') }}</dt>
                                    <dd class="col-md-8">{{ $training->village->name ?? '' }}</dd>

                                    <dt class="col-md-4">{{ trans('cruds.training.fields.venue') }}</dt>
                                    <dd class="col-md-8">{{ $training->venue }}</dd>

                                    <dt class="col-md-4">{{ trans('cruds.training.fields.start_date') }}</dt>
                                    <dd class="col-md-8">{{ $training->start_date }}</dd>

                                    <dt class="col-md-4">{{ trans('cruds.training.fields.end_date') }}</dt>
                                    <dd class="col-md-8">{{ $training->end_date }}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Additional Data
                            </div>

                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-md-4">{{ trans('cruds.training.fields.thematic_areas') }}</dt>
                                    <dd class="col-md-8">
                                        @foreach ($training->thematicAreas as $thematic_area)
                                            <span class="badge badge-info badge-many">{{ $thematic_area->name }}</span>
                                        @endforeach
                                    </dd>
                                    <dt class="col-md-4">{{ trans('cruds.training.fields.value_chains') }}</dt>
                                    <dd class="col-md-8">
                                        @foreach ($training->valueChains as $value_chain)
                                        <span class="badge badge-info badge-many">{{ $value_chain->name }}</span>
                                        @endforeach
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" id="topics-heading">
                                Topics
                                <div class="card-header-actions">
                                    <a class="card-header-action" href="#" data-toggle="collapse" data-target="#topics-data" aria-expanded="true">
                                        <small class="text-muted">show/hide</small>
                                    </a>
                                </div>
                            </div>

                            <div class="collapse" id="topics-data" role="tabpanel" aria-labelledby="topics-heading">
                                <div class="card-body">
                                    <table class="table table-bordered table-striped" id="topics_table">
                                        <thead>
                                            <tr class="">
                                                <th>{{ trans('cruds.topic.fields.training_category') }}</th>
                                                <th>{{ trans('cruds.topic.title_singular') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody id="topics_tbody">
                                            @foreach ($training->topics as $topic)
                                                <tr data-index="{{ $topic->id }}">
                                                    <td>
                                                        {{ $topic->trainingCategory->name ?? '' }}
                                                    </td>
                                                    <td>
                                                        {{ $topic->name }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" id="facilitators-heading">
                                Facilitators
                                <div class="card-header-actions">
                                    <a class="card-header-action" href="#" data-toggle="collapse" data-target="#facilitators-data" aria-expanded="true">
                                        <small class="text-muted">show/hide</small>
                                    </a>
                                </div>
                            </div>

                            <div class="collapse" id="facilitators-data" role="tabpanel" aria-labelledby="facilitators-heading">
                                <div class="card-body">
                                    <table class="table table-bordered table-striped" id="facilitators_table">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('cruds.facilitator.fields.name') }}</th>
                                                <th>{{ trans('cruds.facilitator.fields.type') }}</th>
                                                <th>Position</th>
                                            </tr>
                                        </thead>
                                        <tbody id="facilitators_tbody">
                                            @foreach ($training->facilitators as $facilitator)
                                                <tr data-index="{{ $facilitator->id }}">
                                                    <td>
                                                        {{ $facilitator->name}}
                                                    </td>
                                                    <td>
                                                        {{ $facilitator->type ?? '' }}
                                                    </td>
                                                    <td>
                                                        {{ $facilitator->pivot->position }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div>
                    <a class="btn btn-primary" href="{{ route("admin.trainings.index") }}">
                        Back
                    </a>
                    <a class="btn btn-primary" href="{{ route("admin.trainings.edit", $training->id) }}">
                        {{ trans('global.edit') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    @can('attendance_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.training.attendances.create", ['training_id' => $training->id]) }}">
                    {{ trans('global.add') }} {{ trans('cruds.attendance.title_singular') }}
                </a>
                {{-- <button class="btn btn-warning" data-toggle="modal" data-target="#csvImportModal">
                    {{ trans('global.app_excelImport') }}
                </button>
                @include('admin.trainings.excelImport', ['model' => 'Training', 'route' => 'admin.trainings.parseCsvImport']) --}}
            </div>
        </div>
    @endcan

    <div class="card">
        <div class="card-header" id="attendance-data-details-heading">
            Attendance Register
            <div class="card-header-actions">
                <a class="card-header-action" href="#" data-toggle="collapse" data-target="#attendance-data-details" aria-expanded="true">
                    <small class="text-muted">show/hide</small>
                </a>
            </div>
        </div>

        <div class="collapse show" id="attendance-data-details" role="tabpanel" aria-labelledby="attendance-data-details-heading" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    @foreach ($genderCounts as $genderKPI)
                    <div class="col-sm-6 col-md-{{ ceil(12 / ($loop->count)) }}">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-value">{{ $genderKPI->count }}</div>
                                <small class="text-muted text-uppercase font-weight-bold">
                                    {{ $genderKPI->name . ' - ' . $genderKPI->percent . '%' }}
                                </small>
                                <div class="progress progress-xs mt-3 mb-0">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: {{ $genderKPI->percent }}%"
                                        aria-valuenow="{{ $genderKPI->percent }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @includeIf('admin.trainings.relationships.trainingAttendances', ['attendances' => $training->attendances])

                <div>
                    <a class="btn btn-primary" href="{{ route("admin.trainings.index") }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection