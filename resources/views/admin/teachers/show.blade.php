@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.facilitator.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.teachers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.facilitator.fields.id') }}
                        </th>
                        <td>
                            {{ $teacher->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.facilitator.fields.name') }}
                        </th>
                        <td>
                            {{ $teacher->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.facilitator.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Teacher::TYPE_SELECT[$teacher->type] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.facilitator.fields.contact') }}
                        </th>
                        <td>
                            {{ $teacher->contact }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.facilitator.fields.district') }}
                        </th>
                        <td>
                            {{ $teacher->district }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.facilitator.fields.ward') }}
                        </th>
                        <td>
                            {{ $teacher->wards }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.teachers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection