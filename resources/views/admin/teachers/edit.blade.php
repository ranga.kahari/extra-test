@extends('layouts.admin')
@section('content')

<div class="card">
    <form method="POST" action="{{ route("admin.teachers.update", [$teacher->id]) }}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="card-header">
            {{ trans('global.edit') }} {{ trans('cruds.facilitator.title_singular') }}
        </div>

        <div class="card-body">
            <div class="form-group row">
                <label class="required col-md-3 col-form-label" for="name">{{ trans('cruds.facilitator.fields.name') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $teacher->name) }}" required>
                    @if($errors->has('name'))
                        <div class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="required col-md-3 col-form-label">{{ trans('cruds.facilitator.fields.type') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                        <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                        @foreach(App\Models\Teacher::TYPE_SELECT as $key => $label)
                            <option value="{{ $key }}" {{ old('type', $teacher->type) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('type'))
                        <div class="invalid-feedback">
                            {{ $errors->first('type') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="contact">{{ trans('cruds.facilitator.fields.contact') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('contact') ? 'is-invalid' : '' }}" type="text" name="contact" id="contact" value="{{ old('contact', $teacher->contact) }}">
                    @if($errors->has('contact'))
                        <div class="invalid-feedback">
                            {{ $errors->first('contact') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="ward">{{ trans('cruds.facilitator.fields.ward') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('contact') ? 'is-invalid' : '' }}" type="text" name="ward"
                        id="ward" value="{{ old('contact', $teacher->wards) }}">
                    @if($errors->has('ward'))
                    <div class="invalid-feedback">
                        {{ $errors->first('ward') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="contact">{{ trans('cruds.facilitator.fields.district') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('district') ? 'is-invalid' : '' }}" type="text" name="district"
                        id="district" value="{{ old('district', $teacher->district) }}">
                    @if($errors->has('district'))
                    <div class="invalid-feedback">
                        {{ $errors->first('district') }}
                    </div>
                    @endif
                </div>
            </div>
            
        </div>
        
        <div class="card-footer">
            <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
            <a class="btn btn-default" href="{{ route('admin.teachers.index') }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </form>
</div>



@endsection