@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.attendance.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.attendances.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                     <tr>
                        <th>
                            {{ trans('cruds.attendance.fields.id') }}
                        </th>
                        <td>
                            {{ $attendance->id }}
                        </td>
                    </tr> 
                    
                    <tr>
                        <th>
                            {{ trans('cruds.attendance.fields.training') }}
                        </th>
                        <td>
                            <a href="{{ route('admin.trainings.show',$attendance->training->id) }}">
                            {{ $attendance->training->name ?? '' }}
                            </a>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                            {{ trans('cruds.attendance.fields.beneficiary') }}
                        </th>
                        <td>
                            <a href="{{ route('admin.beneficiaries.show', $attendance->beneficiary->id) }}">
                            {{ $attendance->beneficiary->name ?? '' }}
                            </a>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ url()->previous() }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection