@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.attendance.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.attendances.update", [$attendance->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="beneficiary_id">{{ trans('cruds.attendance.fields.beneficiary') }}</label>
                <select class="form-control select2 {{ $errors->has('beneficiary') ? 'is-invalid' : '' }}" name="beneficiary_id" id="beneficiary_id" required>
                    @foreach($beneficiaries as $id => $beneficiary)
                        <option value="{{ $id }}" {{ ($attendance->beneficiary ? $attendance->beneficiary->id : old('beneficiary_id')) == $id ? 'selected' : '' }}>{{ $beneficiary }}</option>
                    @endforeach
                </select>
                @if($errors->has('beneficiary'))
                    <div class="invalid-feedback">
                        {{ $errors->first('beneficiary') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.attendance.fields.beneficiary_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="training_id">{{ trans('cruds.attendance.fields.training') }}</label>
                <select class="form-control select2 {{ $errors->has('training') ? 'is-invalid' : '' }}" name="training_id" id="training_id" required>
                    @foreach($trainings as $id => $training)
                        <option value="{{ $id }}" {{ ($attendance->training ? $attendance->training->id : old('training_id')) == $id ? 'selected' : '' }}>{{ $training }}</option>
                    @endforeach
                </select>
                @if($errors->has('training'))
                    <div class="invalid-feedback">
                        {{ $errors->first('training') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.attendance.fields.training_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection