@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.beneficiary.title') }}
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Demographic Data
                    </div>

                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.first_name') }}</dt>
                            <dd class="col-md-8">{{ $person->first_name }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.surname') }}</dt>
                            <dd class="col-md-8">{{ $person->surname }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.gender') }}</dt>
                            <dd class="col-md-8">{{ App\Models\Beneficiary::GENDER_SELECT[$person->gender] ?? '' }}
                            </dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.national_id_number') }}</dt>
                            <dd class="col-md-8">{{ $person->national_id_number }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.year_of_birth') }}</dt>
                            <dd class="col-md-8">{{ $person->year_of_birth }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.phone_number') }}</dt>
                            <dd class="col-md-8">{{ $person->phone_number }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.village') }}</dt>
                            <dd class="col-md-8">{{ $person->village->name ?? '' }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.village') }}</dt>
                            <dd class="col-md-8">
                                @can('village_show')
                                <a href="{{ route('admin.villages.show', $person->village->id) }}">
                                    {{ $person->village->name ?? '' }}
                                </a> (
                                <a href="{{ route('admin.wards.show', $person->village->ward_id) }}">
                                    {{ $person->village->ward->name ?? '' }}
                                </a>)
                                @else
                                {{ ($person->village->name ?? '') . ' (' . ($person->village->ward->name ?? '') . ')' }}
                                @endcan
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Additional Data
                    </div>

                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.is_household_head') }}</dt>
                            <dd class="col-md-8"><span
                                    class="badge badge-{{ $person->is_household_head ? "success" : "danger" }}">{{ $person->is_household_head ? "Yes" : "No" }}</span>
                            </dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.name_of_household_head') }}</dt>
                            <dd class="col-md-8">{{ $person->name_of_household_head }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.id_of_household_head') }}</dt>
                            <dd class="col-md-8">{{ $person->id_of_household_head }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.size_of_household') }}</dt>
                            <dd class="col-md-8">{{ $person->size_of_household }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.females_in_household') }}</dt>
                            <dd class="col-md-8">{{ $person->females_in_household }}</dd>

                            <dt class="col-md-4">
                                {{ trans('cruds.beneficiary.fields.children_under_five_in_household') }}</dt>
                            <dd class="col-md-8">{{ $person->children_under_five_in_household }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.production_area') }}</dt>
                            <dd class="col-md-8">{{ $person->production_area }}</dd>

                            <dt class="col-md-4">{{ trans('cruds.beneficiary.fields.farmer_category') }}</dt>
                            <dd class="col-md-8">
                                {{ App\Models\Beneficiary::FARMER_CATEGORY_SELECT[$person->farmer_category] ?? '' }}
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header" id="value-chains-heading">
                        Value Chains & Groups
                        <div class="card-header-actions">
                            <a class="card-header-action" href="#" data-toggle="collapse"
                                data-target="#value-chains-and-groups-data" aria-expanded="true">
                                <small class="text-muted">show/hide</small>
                            </a>
                        </div>
                    </div>

                    <div class="collapse" id="value-chains-and-groups-data" role="tabpanel"
                        aria-labelledby="groups-heading">
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-md-3">{{ trans('cruds.beneficiary.fields.value_chains') }}</dt>
                                <dd class="col-md-9">
                                    @foreach($beneficiary->value_chains as $key => $value_chains)
                                    <span class="badge badge-info">{{ $value_chains->name }}</span>
                                    @endforeach
                                </dd>

                                <dt class="col-md-3">{{ trans('cruds.beneficiary.fields.groups') }}</dt>
                                <dd class="col-md-9">
                                    <table class="table table-bordered table-striped table-sm" id="groups_table">
                                        <thead>
                                            <tr>
                                                <th>@lang('cruds.group.fields.name')</th>
                                                <th>@lang('cruds.beneficiary.fields.position')</th>
                                            </tr>
                                        </thead>
                                        <tbody id="groups_tbody">
                                            @foreach ($beneficiary->groups as $item)
                                            <tr>
                                                <td>
                                                    @can('group_show')
                                                    <a href="{{ route('admin.groups.show', $item->id) }}">
                                                        {{ $item->name ?? '' }}
                                                    </a>
                                                    @else
                                                    {{ $item->name ?? '' }}
                                                    @endcan
                                                </td>
                                                <td>{{ $item->pivot->position }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                @includeIf('admin.beneficiaries.relationships.beneficiaryAttendances', [
                'attendances' => $beneficiary->beneficiaryAttendances
                ])
            </div>
        </div> --}}
    </div>
</div>
@endsection
