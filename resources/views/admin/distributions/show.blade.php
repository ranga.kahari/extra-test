@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.distribution.title_singular') }}
    </div>
    
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        Details
                    </div>
    
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.id') }}</dt>
                            <dd class="col-md-8">{{ $distribution->id }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.district') }}</dt>
                            <dd class="col-md-8">{{ $distribution->district }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.project') }}</dt>
                            <dd class="col-md-8">{{ $distribution->project }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.item_distributed_type') }}</dt>
                            <dd class="col-md-8">{{ $distribution->item_distributed_type }}</dd>
    
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.date') }}</dt>
                            <dd class="col-md-8">{{ $distribution->date }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.item_distributed') }}</dt>
                            <dd class="col-md-8">{{ $distribution->item_distributed }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.variety') }}</dt>
                            <dd class="col-md-8">{{ $distribution->variety }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.quantity') }}</dt>
                            <dd class="col-md-8">{{ $distribution->quantity }}</dd>
                            
                            <dt class="col-md-4">{{ trans('cruds.distribution.fields.beneficiary_name') }}</dt>
                            <a href="{{ route('admin.beneficiaries.show', $distribution->beneficiaries[0]->id) }}">
                                <dd class="col-md-8">{{ $distribution->beneficiaries[0]['name'] }}</dd>
                            </a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div> 
        
    </div>
    
    <div class="form-group">
        <a class="btn btn-default" href="{{ route('admin.distributions.index') }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    
    </div>
    
</div>


@endsection
