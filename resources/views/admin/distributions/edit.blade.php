@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.distribution.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.distributions.update", [$distribution->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.distribution.fields.project') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('project') ? 'is-invalid' : '' }}" name="project"
                        id="project">
                        <option value disabled {{ old('project', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach(App\Models\Distribution::PROJECT_OWNER as $key => $label)
                        <option value="{{ $key }}" {{ old('project', $distribution->project) === (string) $key ? 'selected' : '' }}>
                            {{ $label }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('project'))
                    <div class="invalid-feedback">
                        {{ $errors->first('project') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label
                    class="col-md-3 col-form-label">{{ trans('cruds.distribution.fields.item_distributed_type') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('item_distributed_type') ? 'is-invalid' : '' }}"
                        name="item_distributed_type" id="item_distributed_type">
                        <option value disabled {{ old('item_distributed_type', $distribution->item_distributed_type) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach (App\Models\Distribution::ITEM_DISTRIBUTED_TYPE as $key => $item_distributed_type)
                        <option value="{{ $key }}">{{ $item_distributed_type}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('item_distributed_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('item_distributed_type') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('date') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="date">{{ trans('cruds.distribution.fields.date') }}*</label>
                <div class="col-md-9">
                    <input type="text" id="date" name="date" class="form-control date"
                        value="{{ old('date', isset($distribution) ? $distribution->date : '') }}" required>
                    @if($errors->has('date'))
                    <em class="invalid-feedback">
                        {{ $errors->first('date') }}
                    </em>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label
                    class="col-md-3 col-form-label required">{{ trans('cruds.distribution.fields.item_distributed') }}</label>
                <div class="col-md-9">
                    <select name="item_distributed[]" id="item_distributed" class="form-control select2"
                        multiple="multiple" required>
                        @foreach(App\Models\Distribution::ITEM_DISTRIBUTED as $id => $item_distributed)
                        <option value="{{ $id }}" {{ in_array($id, old('item_distributed', [])) ? 'selected' : '' }}>
                            {{ $item_distributed}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('item_distributed'))
                    <div class="invalid-feedback">
                        {{ $errors->first('item_distributed') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label required"
                    for="variety">{{ trans('cruds.distribution.fields.variety') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('variety') ? 'is-invalid' : '' }}" type="text"
                        name="variety" id="variety" value="{{ old('variety', $distribution->variety) }}">
                    @if($errors->has('variety'))
                    <div class="invalid-feedback">
                        {{ $errors->first('variety') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label required"
                    for="first_name">{{ trans('cruds.distribution.fields.quantity') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('quantity') ? 'is-invalid' : '' }}" type="text"
                        name="quantity" id="quantity" value="{{ old('quantity', $distribution->quantity) }}">
                    @if($errors->has('quantity'))
                    <div class="invalid-feedback">
                        {{ $errors->first('quantity') }}
                    </div>
                    @endif
                </div>
            </div>
            
            <div class="form-group row {{ $errors->has('beneficiary_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="beneficiary">Beneficiary </label>
                <div class="col-md-9">
                    <input class="form-control" type="text" disabled
                        value="{{ old('beneficiary_id',$distribution->beneficiaries[0]['name']) }}">
                </div>
            </div>

            <div class="form-group row {{ $errors->has('beneficiary_id') ? 'has-error' : '' }}">
                <label class="col-md-3 col-form-label" for="beneficiary">Beneficiary </label>
                <div class="col-md-9">
                    <select name="beneficiary_id" id="attendances" class="form-control select2-ajax-search"
                        ajax-search-url="{{ route('admin.beneficiaries.search') }}"></select>
                </div>
            </div>

            <div class="card-footer">
                <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
                <button class="btn btn-danger" type="reset"> Reset</button>
                <a class="btn btn-default" href="{{ route('admin.distributions.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>

        </form>
    </div>

</div>


@endsection