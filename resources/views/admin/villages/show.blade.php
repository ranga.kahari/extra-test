@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.village.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.villages.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.village.fields.id') }}
                        </th>
                        <td>
                            {{ $village->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.village.fields.name') }}
                        </th>
                        <td>
                            {{ $village->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.ward.fields.district') }}
                        </th>
                        <td>
                            {{ $village->ward->district ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.village.fields.ward') }}
                        </th>
                        <td>
                            {{ $village->ward->ward_number ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.villages.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endsection