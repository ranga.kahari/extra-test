@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.distribution.title_singular') }} {{ trans('cruds.report.title_singular') }}
    </div>
    
    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <div class="row">
                           
                        </div>
                    </tr>
                    <tr>
                        <th width="10">
    
                        </th>
                        <th>
                            {{ trans('cruds.distribution.fields.item_distributed') }}
                        </th>
                        <th>
                            {{ trans('cruds.distribution.fields.total_number_of_farmers') }}
                        </th>
    
                    </tr>
                </thead>
                <tbody>
                    @foreach($distributions as $distribution)
                    <tr>
                        <td>
    
                        </td>
                        <td>
                           {{  $distribution->item_distributed  }}
                        </td>
                        <td>
                           {{  $distribution->count  }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>
@endsection
