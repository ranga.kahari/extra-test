@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{--            {{ trans('cruds.careGroup.title_singular') }} {{ trans('cruds.report.title_singular') }}--}}
        ISAL Report By District
    </div>

    <form action="{{ route('admin.reports.isal-search') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-row align-items-center mb-2">
                <div class="col-8 my-2">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-auto my-4">
                    <button type="submit" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>

    </form>

    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th>District</th>
                        <th>
                            Group Category
                            {{--                            {{ trans('cruds.group.fields.group_number') }}--}}
                        </th>
                        <th>
                            Total number of members
                            {{--                            {{ trans('cruds.careGroup.fields.number_of_members') }}--}}
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($isals as $isal)
                    @foreach($isal as $isa)
                    <tr>
                        <th>{{$isa[0]->group_district}}</th>
                        <th></th>
                        <td></td>
                    <tr>
                        <th></th>
                        <th>{{$isa[0]->isals_categories}}</th>
                        <td></td>
                    </tr>
                    @foreach($isa as $is)
                    <tr>
                        <th></th>
                        <td>{{$is->name}}</td>
                        <td>{{ $is->members->count() }}</td>
                    </tr>
                    @endforeach
                    @endforeach
                    @endforeach
                    </tr>
                </tbody>
            </table>

        </div>
    </div>

    @endsection