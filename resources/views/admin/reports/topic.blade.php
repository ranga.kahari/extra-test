@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.topic.title_singular') }} {{ trans('cruds.report.title_singular') }}
    </div>

    <div class="card-body">
        <div class="form-row align-items-center mb-2">
            <div class="col-auto my-1">
                <select name="district" class="custom-select mr-sm-2" id="district">
                    <option value="">Select  District</option>
                    @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                    <option value="{{ $key }}">{{ $district }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md my-1">
                <select name="topic_ids[]" class="custom-select mr-sm-2 select2 form-control js-example-basic-multiple" id="topic_ids" multiple>
                    {{-- <option value="">Select Topic</option> --}}
                    @foreach ($topics as $id => $topic)
                    <option value="{{ $id }}">{{ $topic }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm my-1">
                <input class="custom-select mr-sm-2" type="date" name="start_date" id="start_date">
            </div>
            <div class="col-sm my-1">
                <input class="custom-select mr-sm-2" type="date" name="end_date" id="end_date">
            </div>

            <div class="col-auto my-1">
                <button type="button" class="btn btn-primary" id="search-button">Search</button>
            </div>
        </div>
        
        <div class="row" id="genderCounts">
        </div>

<div class="table-responsive">
    <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-TopicReport">
        <thead>
            <tr>
                <th>
                    {{ trans('cruds.report.district') }}
                </th>
                <th>
                    {{ trans('cruds.topic.fields.name')  }}
                </th>
                <th>
                    {{ trans('cruds.beneficiary.fields.first_name') }}
                </th>
                <th>
                    {{ trans('cruds.beneficiary.fields.surname') }}
                </th>
                <th>
                    {{ trans('cruds.beneficiary.fields.gender') }}
                </th>
                <th>
                    {{ trans('cruds.beneficiary.fields.national_id_number') }}
                </th>
                <th>
                    {{ trans('cruds.beneficiary.fields.year_of_birth') }}
                </th>
            </tr>
        </thead>
    </table>
</div>
</div>
</div>

@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        $('.js-example-basic-multiple').select2({
        placeholder: " Select Topic * ",
        allowClear: true
        });
        topics = {!! json_encode($topics) !!}
        tableData = '';
        district = 'All';
        topic = 'Any';
        
        $('#district').on('change', function() {
            if ($(this).val() == '') {
                district = 'All';
            } else {
                district = $(this).val();
            }
            console.log(district);
        })
        
        $('#topic_id').on('change', function() {
            topicId = $(this).val();
            if (topicId == '') {
                topic = 'Any';
            } else {
                $.each(topics, function(key, value) {
                    if(key == topicId) {
                        topic = value;
                    }
                })
            }
            
            console.log(topic);
        })
       
        $("#search-button").on('click', function() {
            
            // update the genderCounts
            $.ajax({
                method: "GET",
                url: "{{ route('admin.reports.topic-data-gender-counts') }}",
                data: {
                district: $('#district').val(),
                topic_ids: $('#topic_ids').val(),
                start_date: $('#start_date').val(),
                end_date: $('#end_date').val()
                }
            })
            .done(function( result ) {
            $("#genderCounts").html(result);
            });
            
            if ( $.fn.dataTable.isDataTable( '.datatable-TopicReport' ) ) {
                table = $('.datatable-TopicReport').DataTable();
                table.destroy();
                $('.datatable-TopicReport').find('tbody').remove();
            }
            
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            let dtOverrideGlobals = {
                buttons: dtButtons,
                processing: true,
                serverSide: false,
                retrieve: true,
                aaSorting: [],
                ajax: {
                    url: "{{ route('admin.reports.topic-data') }}",
                    method: "GET",
                    data: function ( d ) {
                        d.district = $('#district').val();
                        d.topic_ids = $('#topic_ids').val();
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columnDefs: [
                    {
                        orderable: false,
                        searchable: false,
                        className: '',
                        targets: 0
                    },
                    {
                        orderable: true,
                        searchable: true,
                        targets: -1
                    }
                ],
                select: null,
                columns: [
                    { data: 'district', name: 'district' },
                    { data: 'topic', name: 'topics.name' },
                    { data: 'first_name', name: 'beneficiaries.first_name' },
                    { data: 'surname', name: 'beneficiaries.surname' },
                    { data: 'gender', name: 'beneficiaries.gender' },
                    { data: 'national_id_number', name: 'beneficiaries.national_id_number' },
                    { data: 'year_of_birth', name: 'beneficiaries.year_of_birth' },
                ],
                orderCellsTop: true,
                pageLength: 50,
            };
            
            table = $('.datatable-TopicReport').DataTable(dtOverrideGlobals);
        });

    });
    
    
</script>
@endsection