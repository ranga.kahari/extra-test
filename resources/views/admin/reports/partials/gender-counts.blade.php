@foreach ($genderCounts as $genderKPI)
<div class="col-sm-6 col-md-{{ ceil(12 / ($loop->count)) }}">
    <div class="card">
        <div class="card-body">
            <div class="text-value">{{ number_format($genderKPI->count) }}</div>
            <small class="text-muted text-uppercase font-weight-bold">
                {{ $genderKPI->name . ' - ' . $genderKPI->percent . '%' }}
            </small>
            <div class="progress progress-xs mt-3 mb-0">
                <div class="progress-bar bg-info" role="progressbar" style="width: {{ $genderKPI->percent }}%"
                    aria-valuenow="{{ $genderKPI->percent }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
</div>
@endforeach