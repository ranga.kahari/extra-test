@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.careGroup.title_singular') }} {{ trans('cruds.report.title_singular') }}
    </div>

    <form action="{{ route('admin.reports.care-group-search') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-row align-items-center mb-2">
                <div class="col-5 my-2">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-5 my-2">
                    <select name="wards" class="custom-select mr-sm-2" id="district">
                        <option value="">Select Ward</option>
                        @foreach($wards as $id => $ward)
                        <option value="{{ old('wards', $id ?? null) }}">{{ $ward }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-auto my-1">
                    <button type="submit" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>

    </form>

    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover">
                <thead>
                     <tr>
                         <div class="row">
                            <div class="col-xs-6 mx-auto" >
                                   <h4>Total number of Groups : {{ $groupCount }}</h4>
                            </div>
                            <div class="col-xs-6 mx-auto">
                                    <h4>Total number of members : {{ $memberCount}}</h4>
                            </div>
                        </div>
                    </tr>
                <tr>
                <th width="10">

                </th>
                <th>
                    {{ trans('cruds.group.fields.id') }}
                </th>
                <th>
                    {{ trans('cruds.group.fields.group_number') }}
                </th>
                <th>
                    {{ trans('cruds.careGroup.fields.number_of_members') }}
                </th>

            </tr>
        </thead>
        <tbody>
            @foreach($careGroups as $careGroup)
            <tr>
                <td>

                </td>
                <td>
                    {{ $careGroup->id }}
                </td>
                <td>
                    <a href="{{ route('admin.care-groups.show',$careGroup->id ) }}">
                        {{ $careGroup->group_number }}
                   </a>
                </td>
                <td>
                    {{ $careGroup->memberCount($careGroup->id ) }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

        </div>
    </div>






@endsection


{{-- @section('scripts')

<script>
    $(document).ready(function(){
            $('.js-example-basic-multiple').select2({
                placeholder: "        Select Value Chain * ",
                allowClear: true
            });
        valueChains = {!! json_encode($valueChains) !!}
        tableData = '';
        district = 'All';
        valueChain = 'Any';

        $('#district').on('change', function() {
            if ($(this).val() == '') {
                district = 'All';
            } else {
                district = $(this).val();
            }
            console.log(district);
        })

        $('#value-chain').on('change', function() {
            valueChainId = $(this).val();
            if (valueChainId == '') {
                valueChain = 'Any';
            } else {
                $.each(valueChains, function(key, value) {
                    if(key == valueChainId) {
                        valueChain = value;
                    }
                })
            }
            console.log(valueChain);
        })

        $("#search-button").on('click', function() {

            if ( $.fn.dataTable.isDataTable( '.datatable-ValueChainReport' ) ) {
                table = $('.datatable-ValueChainReport').DataTable();
                table.destroy();
                $('.datatable-ValueChainReport').find('tbody').remove();
            }

            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            let dtOverrideGlobals = {
                buttons: dtButtons,
                processing: true,
                serverSide: false,
                retrieve: true,
                aaSorting: [],
                ajax: {
                    url: "{{ route('admin.reports.value-chain-data') }}",
                    method: "GET",
                    data: function ( d ) {
                        d.district = $('#district').val();
                        d.value_chain_ids = $('#value_chain_ids').val();
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columnDefs: [
                    {
                        orderable: false,
                        searchable: false,
                        className: '',
                        targets: 0
                    },
                    {
                        orderable: true,
                        searchable: true,
                        targets: -1
                    }
                ],
                select: null,
                columns: [
                    { data: 'district', name: 'district' },
                    { data: 'value_chain', name: 'value_chains.name' },
                    { data: 'first_name', name: 'beneficiaries.first_name' },
                    { data: 'surname', name: 'beneficiaries.surname' },
                    { data: 'gender', name: 'beneficiaries.gender' },
                    { data: 'national_id_number', name: 'beneficiaries.national_id_number' },
                    { data: 'year_of_birth', name: 'beneficiaries.year_of_birth' },
                ],
                orderCellsTop: true,
                pageLength: 50,
            };

            table = $('.datatable-ValueChainReport').DataTable(dtOverrideGlobals);
        });

    });


</script>
@endsection --}}
