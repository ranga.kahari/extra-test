@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{--            {{ trans('cruds.careGroup.title_singular') }} {{ trans('cruds.report.title_singular') }}--}}
        Interventions Report By District
    </div>

    <form action="{{ route('admin.reports.intervention-search') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-row align-items-center mb-2">
                <div class="col-8 my-2">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-auto my-4">
                    <button type="submit" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>

    </form>

    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped  table-sm">
                <thead>
                    <tr>
                        <th>District</th>
                        <th>
                            Interventions Mapped
                            {{--                            {{ trans('cruds.group.fields.group_number') }}--}}
                        </th>
                        <th>
                            Total
                            {{--                            {{ trans('cruds.careGroup.fields.number_of_members') }}--}}
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($interventions as $intervention)
                    @foreach($intervention as $interven)
                    @endforeach
                    <tr class="bg-dark">
                        <th>{{$interven[0]->district}}</th>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach($intervention as $interven)
                    <tr>
                        <th></th>
                        <td>{{ $interven[0]->intervention }}</td>
                        <td>{{$intervents->total($interven[0]->intervention,$interven[0]->district)}}</td>
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @endsection