@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.careGroup.title_singular') }} {{ trans('cruds.report.title_singular') }}
    </div>


    <div class="card-body">
        <form action="{{ route('admin.reports.care-group-facilitator-search') }}" method="POST">
            @csrf
            <div class="form-row align-items-center mb-2">
                <div class="col-5 my-1">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="col-5 my-1">
                    <select name="wards" class="custom-select mr-sm-2" id="district">
                        <option value="">Select Ward</option>
                        @foreach($wards as $id => $ward)
                        <option value="{{ old('wards', $id ?? null) }}">{{ $ward }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-auto my-1">
                    <button type="submit" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>
        </form>


        <div class="card">
            <div class="card-body">
                <table class=" table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <div class="row">
                                <div class="col-xs-6 mx-auto">
                                    <h4>Total number of Village Health Workers :  {{ sizeof($facilitators) }}</h4>
                                </div>
                                <div class="col-xs-6 mx-auto">
                                    <h4>Total number of Lead Mothers : </h4>
                                </div>
                            </div>
                        </tr>
                        <tr>
                            <th width="10">

                            </th>
                            {{-- <th>
                              Group
                            </th>
                            <th>
                                {{ trans('cruds.group.fields.id') }}
                            </th> --}}
                            <th>
                                {{ trans('cruds.careGroup.fields.health_worker_name') }}
                            </th>
                            <th>
                                {{ trans('cruds.careGroup.fields.number_of_members') }}
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($facilitators as $facilitator)
                        @if($facilitator->countLeadMothers($facilitator->id)>0)
                        <tr>
                            <td>
                                
                            </td>
                            <td>
                                {{ $facilitator->name }}
                            </td>
                            <td>
                                {{ $facilitator->countLeadMothers($facilitator->id )}}
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                {{ $facilitators->links() }}
            </div>
        </div>

    </div>


@endsection