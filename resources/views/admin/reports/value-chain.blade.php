@extends('layouts.admin')

@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('cruds.valueChain.title_singular') }} {{ trans('cruds.report.title_singular') }}
        </div>
        
        <div class="card-body">
            
            <div class="form-row align-items-center mb-2">
                <div class="col-auto my-1">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md my-1 ">
                    <select name="value_chain_ids[]" class="custom-select mr-sm-2 select2 form-control js-example-basic-multiple" id="value_chain_ids" multiple >
                        @foreach ($valueChains as $id => $valueChain)
                        <option value="{{ $id }}">{{ $valueChain }}</option>
                        @endforeach
                    </select> 
          
                </div>
            
                <div class="col-sm my-1">
                    <input class="custom-select mr-sm-2" type="date" name="start_date" id="start_date">
                </div>
                <div class="col-sm my-1">
                    <input class="custom-select mr-sm-2" type="date" name="end_date" id="end_date">
                </div>
            
                <div class="col-auto my-1">
                    <button type="button" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>
            
            <div class="row" id="genderCounts">
            </div>
            
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-ValueChainReport">
                    <thead>
                        <tr>
                            <th>
                                {{ trans('cruds.report.district') }}
                            </th>
                            <th>
                                {{ trans('cruds.valueChain.title_singular') }}
                            </th>
                            <th>
                                {{ trans('cruds.beneficiary.fields.first_name') }}
                            </th>
                            <th>
                                {{ trans('cruds.beneficiary.fields.surname') }}
                            </th>
                            <th>
                                {{ trans('cruds.beneficiary.fields.gender') }}
                            </th>
                            <th>
                                {{ trans('cruds.beneficiary.fields.national_id_number') }}
                            </th>
                            <th>
                                {{ trans('cruds.beneficiary.fields.year_of_birth') }}
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function(){
            $('.js-example-basic-multiple').select2({
                placeholder: "        Select Value Chain * ",
                allowClear: true
            });
        valueChains = {!! json_encode($valueChains) !!}
        tableData = '';
        district = 'All';
        valueChain = 'Any';
        
        $('#district').on('change', function() {
            if ($(this).val() == '') {
                district = 'All';
            } else {
                district = $(this).val();
            }
            console.log(district);
        })
        
        $('#value-chain').on('change', function() {
            valueChainId = $(this).val();
            if (valueChainId == '') {
                valueChain = 'Any';
            } else {
                $.each(valueChains, function(key, value) {
                    if(key == valueChainId) {
                        valueChain = value;
                    }
                })
            }
            console.log(valueChain);
        })
       
        $("#search-button").on('click', function() {
            
            // update the genderCounts
            $.ajax({
                method: "GET",
                url: "{{ route('admin.reports.value-chain-data-gender-counts') }}",
                data: { 
                    district: $('#district').val(),
                    value_chain_ids: $('#value_chain_ids').val(),
                    start_date: $('#start_date').val(),
                    end_date: $('#end_date').val()
                 }
            })
            .done(function( result ) {
                $("#genderCounts").html(result);
            });
            
            if ( $.fn.dataTable.isDataTable( '.datatable-ValueChainReport' ) ) {
                table = $('.datatable-ValueChainReport').DataTable();
                table.destroy();
                $('.datatable-ValueChainReport').find('tbody').remove();
            }
            
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            let dtOverrideGlobals = {
                buttons: dtButtons,
                processing: true,
                serverSide: true,
                retrieve: true,
                aaSorting: [],
                ajax: {
                    url: "{{ route('admin.reports.value-chain-data') }}",
                    method: "GET",
                    data: function ( d ) {
                        d.district = $('#district').val();
                        d.value_chain_ids = $('#value_chain_ids').val();
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columnDefs: [
                    {
                        orderable: false,
                        searchable: false,
                        className: '',
                        targets: 0
                    },
                    {
                        orderable: true,
                        searchable: true,
                        targets: -1
                    }
                ],
                select: null,
                columns: [
                    { data: 'district', name: 'district' },
                    { data: 'value_chain', name: 'value_chains.name' },
                    { data: 'first_name', name: 'beneficiaries.first_name' },
                    { data: 'surname', name: 'beneficiaries.surname' },
                    { data: 'gender', name: 'beneficiaries.gender' },
                    { data: 'national_id_number', name: 'beneficiaries.national_id_number' },
                    { data: 'year_of_birth', name: 'beneficiaries.year_of_birth' },
                ],
                orderCellsTop: true,
                pageLength: 50,
            };
            
            table = $('.datatable-ValueChainReport').DataTable(dtOverrideGlobals);
        });

    });
    
    
    </script>
    @endsection