@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{--            {{ trans('cruds.careGroup.title_singular') }} {{ trans('cruds.report.title_singular') }}--}}
        Yield Per Value Chain of Specific Season
    </div>

    {{--        <form action="{{ route('admin.reports.yieldData-search') }}" method="POST">--}}
    {{--            @csrf--}}
    {{--            <div class="card-body">--}}
    {{--                <div class="form-row align-items-center mb-2">--}}
    {{--                    <div class="col-8 my-2">--}}
    {{--                        <select name="district" class="custom-select mr-sm-2" id="district">--}}
    {{--                            <option value="">Select District</option>--}}
    {{--                            @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)--}}
    {{--                                <option value="{{ $key }}">{{ $district }}</option>--}}
    {{--                            @endforeach--}}
    {{--                        </select>--}}
    {{--                    </div>--}}

    {{--                    <div class="col-auto my-4">--}}
    {{--                        <button type="submit" class="btn btn-primary" id="search-button">Search</button>--}}
    {{--                    </div>--}}
    {{--                </div>--}}

    {{--        </form>--}}

    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th>District</th>
                        <th>Season</th>
                        <th>
                            Value Chain
                            {{--                            {{ trans('cruds.group.fields.group_number') }}--}}
                        </th>
                        <th>
                            Sum of Yield (ha)
                            {{--                            {{ trans('cruds.careGroup.fields.number_of_members') }}--}}
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($yieldData as $data)
                    @foreach($data as $dat)
                    @foreach($dat as $dats)
                    @endforeach
                    <th>{{$dats[0]->district}}</th>
                    <th>{{$dats[0]->season}}</th>
                    @foreach($dat as $da)
                    <tr>
                        <td></td>
                        <td></td>
                        <td>{{$da[0]->value_chain}}</td>
                        <th>{{$da[0]->total($da[0]->value_chain,$da[0]->season,$da[0]->district)}}</th>
                    </tr>
                    @endforeach
                    @endforeach

                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @endsection