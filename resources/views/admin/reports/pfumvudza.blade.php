@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.pfumvudza.fields.number_of_farmers_per_each_agronomic') }}
    </div>

    <form action="{{ route('admin.reports.pfumvudza-search') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-row align-items-center mb-2">
                <div class="col-8 my-2">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-auto my-4">
                    <button type="submit" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>

    </form>

    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th>District</th>
                        <th>
                            {{ trans('cruds.pfumvudza.fields.agronomic_practice') }}
                        </th>
                        <th>
                            {{ trans('cruds.pfumvudza.fields.number_of_farmers') }}
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($pfumvudzas as $pfumvudza)
                    @foreach($pfumvudza as $pfu)
                    <tr>
                        <th>{{$pfu[0]->district}}</th>
                        <th></th>
                        <th>{{  $pfumvu->totalFarmers($pfu[0]->agronomic_practice,$pfu[0]->district)    }}</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>{{ $pfu[0]->agronomic_practice }}</th>
                        <th>{{  $pfumvu->totalFarmers($pfu[0]->agronomic_practice,$pfu[0]->district)    }}</th>
                    </tr>
                    <tr>
                        <td></td>
                        <td>{{ trans('cruds.pfumvudza.fields.male') }}</td>
                        <td>{{$pfumvu->males($pfu[0]->agronomic_practice,$pfu[0]->district)}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>{{ trans('cruds.pfumvudza.fields.female') }}</td>
                        <td>{{$pfumvu->females($pfu[0]->agronomic_practice,$pfu[0]->district)}}</td>
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @endsection