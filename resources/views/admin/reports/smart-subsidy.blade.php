@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.smartSubsidies.fields.number_of_beneficiaries') }}
    </div>

    <form action="{{ route('admin.reports.smart-subsidies-search') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-row align-items-center mb-2">
                <div class="col-8 my-2">
                    <select name="district" class="custom-select mr-sm-2" id="district">
                        <option value="">Select District</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}">{{ $district }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-auto my-4">
                    <button type="submit" class="btn btn-primary" id="search-button">Search</button>
                </div>
            </div>

    </form>

    <div class="card">
        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.smartSubsidies.fields.district') }}
                        </th>
                        <th>{{ trans('cruds.smartSubsidies.fields.valuechain') }}</th>
                        <th>
                            {{ trans('cruds.smartSubsidies.fields.total') }}
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($smart as $smar)
                    @foreach($smar as $sma)
                    @endforeach
                    <tr class="bg-dark">
                        <th>{{$sma[0]->ward->district}}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($smar as $sma)
                    <tr>
                        <th></th>
                        <td>{{$sma[0]->value_chain}}</td>
                        <td>{{$smartSubsidies->total($sma[0]->ward->district,$sma[0]->value_chain)}}</td>
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @endsection