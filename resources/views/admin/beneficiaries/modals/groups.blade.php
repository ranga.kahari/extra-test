<div class="modal fade" id="groupsModal" tabindex="-1" role="dialog" aria-labelledby="groupsModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="groupsModalLabel">Select Groups</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row {{ $errors->has('groups') ? 'has-error' : '' }}">
                    <label class="col-md-3 col-form-label"
                        for="groups">{{ trans('cruds.beneficiary.fields.groups') }}</label>
                    <div class="col-md-9">
                        <select name="groups[]" id="groups" class="form-control modal-select select2-ajax-search"
                            ajax-search-url="{{ route('admin.groups.search') }}" multiple="multiple"
                            placeholder="{{ trans('global.pleaseSelect') }}"></select>
                        @if($errors->has('groups'))
                        <em class="invalid-feedback">
                            {{ $errors->first('groups') }}
                        </em>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>