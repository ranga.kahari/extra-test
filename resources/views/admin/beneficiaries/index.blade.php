@extends('layouts.admin')
@section('content')
@can('beneficiary_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.beneficiaries.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.beneficiary.title_singular') }}
            </a>
            <a class="btn btn-success" href="{{ route('admin.beneficiaries.export') }}">
                Export all to Excel
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.beneficiary.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="row">
            @foreach ($genderCounts as $genderKPI)
            <div class="col-sm-6 col-md-{{ ceil(12 / ($loop->count)) }}">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ number_format($genderKPI->count) }}</div>
                        <small class="text-muted text-uppercase font-weight-bold">
                            {{ $genderKPI->name . ' - ' . $genderKPI->percent . '%' }}
                        </small>
                        <div class="progress progress-xs mt-3 mb-0">
                            <div class="progress-bar bg-info" role="progressbar"
                                style="width: {{ $genderKPI->percent }}%"
                                aria-valuenow="{{ $genderKPI->percent }}"
                                aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Beneficiary">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.beneficiary.fields.first_name') }}
                    </th>
                    <th>
                        {{ trans('cruds.beneficiary.fields.surname') }}
                    </th>
                    <th>
                        {{ trans('cruds.beneficiary.fields.gender') }}
                    </th>
                    <th>
                        {{ trans('cruds.beneficiary.fields.year_of_birth') }}
                    </th>
                    <th>
                        {{ trans('cruds.beneficiary.fields.phone_number') }}
                    </th>
                    <th style="min-width: 115px">
                        &nbsp;
                    </th>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <select class="search" strict="true">
                            <option value>{{ trans('global.all') }}</option>
                            @foreach(App\Models\Beneficiary::GENDER_SELECT as $key => $item)
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                    </td>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('beneficiary_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.beneficiaries.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.beneficiaries.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'first_name', name: 'first_name' },
{ data: 'surname', name: 'surname' },
{ data: 'gender', name: 'gender' },
{ data: 'year_of_birth', name: 'year_of_birth' },
{ data: 'phone_number', name: 'phone_number' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-Beneficiary').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
});

</script>
@endsection