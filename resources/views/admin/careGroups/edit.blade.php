@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.careGroup.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.care-groups.update", [$group->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <input type="hidden" name="id" value="{{ $group->id }}">

            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.group.fields.group_categories') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('group_categories') ? 'is-invalid' : '' }}" type="text"
                        name="group_categories" id="group_categories"
                        value="{{ App\Models\Group::GROUP_CATEGORIES_SELECT['Care Group'] }}">
                    @if($errors->has('group_categories'))
                    <div class="invalid-feedback">
                        {{ $errors->first('group_categories') }}
                    </div>
                    @endif
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.ward.fields.district') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('group_district') ? 'is-invalid' : '' }}" name="group_district"
                        id="group_district">
                        <option value disabled {{ old('group_district', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach (App\Models\Ward::DISTRICT_SELECT as $key => $district)
                        <option value="{{ $key }}"
                            {{ old('group_district', $group->group_district) === (string) $key ? 'selected' : '' }}>
                            {{ $district }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('group_district'))
                    <div class="invalid-feedback">
                        {{ $errors->first('group_district') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"
                    for="group_number">{{ trans('cruds.group.fields.group_number') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('group_number') ? 'is-invalid' : '' }}" type="text"
                        name="group_number" id="group_number" value="{{ old('group_number', $group->group_number) }}">
                    @if($errors->has('group_number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('group_number') }}
                    </div>
                    @endif
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-3 col-form-label">{{ trans('cruds.careGroup.fields.health_centre_ward') }}</label>
                <div class="col-md-9">
                    <select class="form-control {{ $errors->has('health_centre_ward') ? 'is-invalid' : '' }}"
                        name="health_centre_ward" id="health_centre_ward">
                        <option value disabled {{ old('ward', null) === null ? 'selected' : '' }}>
                            {{ trans('global.pleaseSelect') }}</option>
                        @foreach($wards as $id => $ward)
                        <option value="{{ $id}}" {{ old('health_centre_ward', $group->health_centre_ward) == $id ? 'selected' : '' }}>{{ $ward }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('health_centre_ward'))
                    <div class="invalid-feedback">
                        {{ $errors->first('health_centre_ward') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="required col-md-3 col-form-label"
                    for="name_of_health_centre">{{ trans('cruds.careGroup.fields.health_centre') }}</label>
                <div class="col-md-9">
                    <input class="form-control {{ $errors->has('name_of_health_centre') ? 'is-invalid' : '' }}"
                        type="text" name="name_of_health_centre" id="name_of_health_centre"
                        value="{{ old('name_of_health_centre', $group->name_of_health_centre) }}" required>
                    @if($errors->has('name_of_health_centre'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_of_health_centre') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="facilitators">{{ trans('cruds.group.fields.facilitators') }}</label>
                <div class="col-md-9">
                    <table class="table table-bordered table-striped" id="facilitators_table">
                        <thead>
                            <tr>
                                <th>@lang('cruds.facilitator.fields.name')</th>
                                <th>@lang('cruds.group.fields.position')</th>
            
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody id="facilitators_tbody">
                            @foreach ($group->facilitators as $key => $item)
                            <tr>
                                <td>
                                    <input type="text" class="form-control" value="{{ $item->name }}" disabled="">
                                </td>
                                <td>
                                    <input type="text" name="facilitators[{{ $item->id }}][position]" class="form-control"
                                        value="{{ $item->pivot->position }}">
                                </td>
                                <td>
                                    <a href="#" class="remove btn btn-xs btn-danger">{{ trans('global.delete') }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#facilitatorsModal">
                        {{ trans('global.add')}}
                    </button>
                </div>
            </div>


            <div class="card-footer">
                <button class="btn btn-primary" type="submit"> {{ trans('global.save') }}</button>
                <button class="btn btn-danger" type="reset"> Reset</button>
                <a class="btn btn-default" href="{{ url()->previous() }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>

        </form>
    </div>

</div>

@include('admin.careGroups.modals.facilitators')

@endsection



@section('scripts')
@parent
<script>
    $(document).ready(function(){
        $('.modal-select').on('select2:select', function (e) {
            var selected_id = $(e.target).attr('id');
            var tableBody = $('#' + selected_id + '_tbody');
            var lastIndex = parseInt(tableBody.find('tr').last().data('index'));

            if (isNaN(lastIndex)) {
                lastIndex = 0;
            }

            if ('facilitators' == selected_id) {
                addFacilitatorDataToTable(tableBody, lastIndex + 1, e.params.data);
            }

            return false;
        });

        $(document).on('click', '.remove', function () {
            var row = $(this).parentsUntil('tr').parent();
            row.remove();
            return false;
        });

        function addFacilitatorDataToTable(table_body, index, data) {
            console.log(data);
            table_body.append(
                '<tr data-index=' + index + '>' +
                    '<td>' +
                        '<input type="text" class="form-control" value="' + (data.newTag ? data.id : data.name) + '" disabled>' +
                    '</td>' +
                    ' <td>' +
                        '<input type="text" name="facilitators[' + data.id + '][position]" class="form-control" value="Facilitator">' +
                    '</td>' +
                    '<td>' +
                        '<a href="#" class="remove btn btn-xs btn-danger">{!! trans('global.delete') !!}</a>' +
                    '</td>' +
                '</tr>'
            );
        }
    });
</script>
@endsection