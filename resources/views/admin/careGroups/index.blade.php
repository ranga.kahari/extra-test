@extends('layouts.admin')
@section('content')

@can('vcts_create')
<div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
        <a class="btn btn-success" href="{{ route('admin.care-groups.create') }}">
            {{ trans('global.add') }} {{ trans('cruds.careGroup.title_singular') }}
        </a>
         <a class="btn btn-warning" href="#" data-toggle="modal" data-target="#csvImportModal">
            {{ trans('cruds.importCareGroups.title') }}
        </a>

        @include('admin.careGroups.import')
    </div>
</div>
@endcan

<div class="card">
    <div class="card-header">
        {{ trans('cruds.careGroup.title_singular') }} {{ trans('global.list') }}
    </div>
    
    <div class="card-body">
        
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-CareGroup">
            <thead>
                <tr>
                    <th width="10">
    
                    </th>
                    <th>
                        {{ trans('cruds.group.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.group.fields.group_number') }}
                    </th>
                    <th>
                        {{ trans('cruds.group.fields.group_categories') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <select class="search" strict="true">
                            <option value="Care Group">Care Group</option>
                        </select>
                    </td>
                    <td>
                    </td>
                </tr>
            </thead>
        </table>
    </div>
    
</div>

@endsection

@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('group_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.care-groups.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.care-groups.index') }}",
    columns: [
        { data: 'placeholder', name: 'placeholder' },
        { data: 'id', name: 'id' },
        // { data: 'name', name: 'name' },
        { data: 'group_number', name: 'group_number' },
        { data: 'group_categories', name: 'group_categories' },
        { data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 2, 'desc' ]],
    pageLength: 100,
  };
  
  let table = $('.datatable-CareGroup').DataTable(dtOverrideGlobals);
  
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
});

</script>
@endsection