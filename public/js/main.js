$(document).ready(function () {
    window._token = $('meta[name="csrf-token"]').attr('content')
    
    // Pace.on("done", function () {
    //     $('#page_overlay').delay(300).fadeOut(600);
    // });

    $(document).on('submit', 'form', function () {
        $('button').attr('disabled', 'disabled');
    });

    moment.updateLocale('en', {
        week: { dow: 1 } // Monday is the first day of the week
    })

    $('.date').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'en',
        icons: {
            up: 'fas fa-chevron-up',
            down: 'fas fa-chevron-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right'
        }
    })

    $('.datetime').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        locale: 'en',
        sideBySide: true,
        icons: {
            up: 'fas fa-chevron-up',
            down: 'fas fa-chevron-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right'
        }
    })

    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss',
        icons: {
            up: 'fas fa-chevron-up',
            down: 'fas fa-chevron-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right'
        }
    })

    $('.select-all').click(function () {
        let $select2 = $(this).parent().siblings('.select2')
        $select2.find('option').prop('selected', 'selected')
        $select2.trigger('change')
    })
    $('.deselect-all').click(function () {
        let $select2 = $(this).parent().siblings('.select2')
        $select2.find('option').prop('selected', '')
        $select2.trigger('change')
    })

    $('.select2').select2()

    var ajax_search_url = '';

    $('.select2-ajax-search').on('select2:open', function (e) {
        ajax_search_url = $(e.target).attr('ajax-search-url');
    });

    $('.select2-ajax-search').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);

            if (term === '' || term.length < 3) {
                return null;
            }

            return {
                id: term,
                text: term,
                newTag: true // add additional parameters
            }
        },
        closeOnSelect: true,
        minimumInputLength: 3,
        ajax: {
            url: function (params) {
                return ajax_search_url;
            },
            data: function (params) {
                var query = {
                    search: params.term,
                    page: params.page || 1
                }
                return query;
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            processResults: function (data) {
                data.page = data.page || 1;
                return {
                    results: data.data.map(function (item) {
                        item.text = item.name;
                        return item;
                    }),
                    pagination: {
                        more: data.next_page_url ? true : false
                    }
                }
            }
        }
    });
    
    $('.popover-dismiss').popover({
        trigger: 'focus'
    })

    $('.treeview').each(function () {
        var shouldExpand = false
        $(this).find('li').each(function () {
            if ($(this).hasClass('active')) {
                shouldExpand = true
            }
        })
        if (shouldExpand) {
            $(this).addClass('active')
        }
    })

    $('button.sidebar-toggler').click(function () {
        setTimeout(function () {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        }, 275);
    })
})
