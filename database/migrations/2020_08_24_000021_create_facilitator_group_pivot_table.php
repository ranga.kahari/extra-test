<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitatorGroupPivotTable extends Migration
{
    public function up()
    {
        Schema::create('facilitator_group', function (Blueprint $table) {
            $table->unsignedInteger('group_id');
            $table->foreign('group_id', 'group_id_fk_2051928')->references('id')->on('groups')->onDelete('cascade');
            $table->unsignedInteger('facilitator_id');
            $table->foreign('facilitator_id', 'facilitator_id_fk_2051928')->references('id')->on('facilitators')->onDelete('cascade');
            $table->string('position')->nullable();
        });
    }
}
