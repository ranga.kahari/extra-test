<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsTable extends Migration
{
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('venue');
            $table->string('training_code')->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->string('prepared_by');
            $table->string('reviewed_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
