<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitatorPfumvudzaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilitator_pfumvudza', function (Blueprint $table) {
            $table->unsignedBigInteger('pfumvudza_id');
            $table->foreign('pfumvudza_id')->references('id')->on('pfumvudzas')->onDelete('cascade');
            $table->unsignedInteger('facilitator_id');
            $table->foreign('facilitator_id')->references('id')->on('facilitators')->onDelete('cascade');
            $table->string('position')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilitator_pfumvudza');
    }
}
