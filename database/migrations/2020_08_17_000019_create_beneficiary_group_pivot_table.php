<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiaryGroupPivotTable extends Migration
{
    public function up()
    {
        Schema::create('beneficiary_group', function (Blueprint $table) {
            $table->unsignedInteger('beneficiary_id');
            $table->foreign('beneficiary_id', 'beneficiary_id_fk_2015227')->references('id')->on('beneficiaries')->onDelete('cascade');
            $table->unsignedInteger('group_id');
            $table->foreign('group_id', 'group_id_fk_2015227')->references('id')->on('groups')->onDelete('cascade');
            $table->string('position')->nullable();
        });
    }
}
