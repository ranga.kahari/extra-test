<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWardsTable extends Migration
{
    public function up()
    {
        Schema::create('wards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('district');
            $table->string('ward_number');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
