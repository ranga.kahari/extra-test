<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicTrainingPivotTable extends Migration
{
    public function up()
    {
        Schema::create('topic_training', function (Blueprint $table) {
            $table->unsignedInteger('training_id');
            $table->foreign('training_id', 'training_id_fk_2015365')->references('id')->on('trainings')->onDelete('cascade');
            $table->unsignedInteger('topic_id');
            $table->foreign('topic_id', 'topic_id_fk_2015365')->references('id')->on('topics')->onDelete('cascade');
        });
    }
}
