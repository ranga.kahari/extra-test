<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_categories')->nullable();
            $table->string('name')->nullable();
            $table->string('group_number')->unique()->nullable();
            $table->string('name_of_health_centre')->nullable();
            $table->string('health_centre_ward')->nullable();
            $table->string('group_district')->nullable();
            $table->string('demo_type')->nullable();
            $table->date('creation_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
