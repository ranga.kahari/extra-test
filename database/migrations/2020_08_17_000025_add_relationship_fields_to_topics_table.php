<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTopicsTable extends Migration
{
    public function up()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->unsignedInteger('training_category_id');
            $table->foreign('training_category_id', 'training_category_fk_2014911')->references('id')->on('training_categories');
        });
    }
}
