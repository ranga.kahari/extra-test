<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValueChainsTable extends Migration
{
    public function up()
    {
        Schema::create('value_chains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('categories')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
