<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToVillagesTable extends Migration
{
    public function up()
    {
        Schema::table('villages', function (Blueprint $table) {
            $table->unsignedInteger('ward_id');
            $table->foreign('ward_id', 'ward_fk_2014485')->references('id')->on('wards');
        });
    }
}
