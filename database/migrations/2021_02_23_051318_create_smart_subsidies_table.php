<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmartSubsidiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smart_subsidies', function (Blueprint $table) {
            $table->id();
            // $table->string('district');
            $table->string('custodian')->nullable();
            $table->integer('males')->nullable();
            $table->integer('females')->nullable();
            $table->integer('total')->nullable();
            $table->string('value_chain')->nullable();
            $table->float('farmer_contribution_value_usd')->nullable();
            $table->float('farmer_contribution_value_rtgs')->nullable();
            $table->float('extra_contribution')->nullable();
            $table->float('subsidy_allocated_per_hh')->nullable();
            $table->string('supplier')->nullable();
            $table->string('officer')->nullable();
            $table->float('collected')->nullable();
            $table->float('outstanding')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->unsignedInteger('ward_id');
            $table->foreign('ward_id', )->references('id')->on('wards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smart_subsidies');
    }
}
