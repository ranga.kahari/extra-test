<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('surname')->nullable();
            $table->string('name')->nullable();
            $table->string('gender');
            $table->string('national_id_number')->nullable();
            $table->string('year_of_birth')->nullable();
            $table->string('phone_number')->nullable();
            $table->boolean('is_household_head')->default(0);
            $table->string('name_of_household_head')->nullable();
            $table->string('id_of_household_head')->nullable();
            $table->string('size_of_household')->nullable();
            $table->string('females_in_household')->nullable();
            $table->string('children_under_five_in_household')->nullable();
            $table->string('production_area')->nullable();
            $table->string('farmer_category')->default('Food Security & Commercial');
            $table->timestamps();
            $table->softDeletes();
            
            $table->unsignedInteger('village_id');
            $table->foreign('village_id')->references('id')->on('villages');

            $table->unsignedInteger('age_band_id')->nullable();
            $table->foreign('age_band_id')->references('id')->on('age_bands')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
