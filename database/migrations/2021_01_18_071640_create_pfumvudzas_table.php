<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePfumvudzasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pfumvudzas', function (Blueprint $table) {
            $table->id();
            $table->string('agronomic_practice');
            $table->string('value_chain');
            $table->string('variety');
            $table->float('area_planted');
            $table->timestamps();
            $table->softDeletes();
            
            $table->unsignedInteger('beneficiary_id');
            $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pfumvudzas');
    }
}
