<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitatorTrainingPivotTable extends Migration
{
    public function up()
    {
        Schema::create('facilitator_training', function (Blueprint $table) {
            $table->unsignedInteger('training_id');
            $table->foreign('training_id', 'training_id_fk_2015366')->references('id')->on('trainings')->onDelete('cascade');
            $table->unsignedInteger('facilitator_id');
            $table->foreign('facilitator_id', 'facilitator_id_fk_2015366')->references('id')->on('facilitators')->onDelete('cascade');
            $table->string('position')->nullable();
        });
    }
}
