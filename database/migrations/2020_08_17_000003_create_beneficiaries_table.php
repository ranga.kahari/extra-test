<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiariesTable extends Migration
{
    public function up()
    {
        Schema::create('beneficiaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('surname')->nullable();
            $table->string('name')->nullable();
            $table->string('gender');
            $table->string('national_id_number')->nullable();
            $table->integer('year_of_birth')->nullable();
            $table->string('phone_number')->nullable();
            $table->boolean('is_household_head')->default(0);
            $table->string('name_of_household_head')->nullable();
            $table->string('id_of_household_head')->nullable();
            $table->integer('size_of_household')->nullable();
            $table->integer('females_in_household')->nullable();
            $table->integer('children_under_five_in_household')->nullable();
            $table->integer('production_area')->nullable();
            $table->string('farmer_category')->default('Food Security & Commercial');
            $table->string('farmer_group_membership')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
