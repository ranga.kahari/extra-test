<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToBeneficiariesTable extends Migration
{
    public function up()
    {
        Schema::table('beneficiaries', function (Blueprint $table) {
            $table->unsignedInteger('village_id');
            $table->foreign('village_id', 'village_fk_2015226')->references('id')->on('villages');

            $table->unsignedInteger('age_band_id')->nullable();
            $table->foreign('age_band_id', 'age_band_fk_20015226')->references('id')->on('age_bands')->onDelete('set null');
        });
    }
}
