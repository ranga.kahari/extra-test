<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAttendancesTable extends Migration
{
    public function up()
    {
        Schema::table('attendances', function (Blueprint $table) {
            $table->unsignedInteger('beneficiary_id');
            $table->foreign('beneficiary_id', 'beneficiary_fk_2015392')->references('id')->on('beneficiaries');
            $table->unsignedInteger('training_id')->nullable();
            $table->foreign('training_id', 'training_fk_2015393')->references('id')->on('trainings');
        });
    }
}
