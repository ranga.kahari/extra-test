<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgeBandsTable extends Migration
{
    public function up()
    {
        Schema::create('age_bands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('lower_limit');
            $table->integer('upper_limit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
