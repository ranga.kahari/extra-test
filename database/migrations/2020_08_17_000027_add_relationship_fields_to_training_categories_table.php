<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTrainingCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('training_categories', function (Blueprint $table) {
            $table->unsignedInteger('thematic_area_id')->nullable();
            $table->foreign('thematic_area_id', 'thematic_area_fk_2014896')->references('id')->on('thematic_areas');
        });
    }
}
