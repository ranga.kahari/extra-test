<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiaryValueChainPivotTable extends Migration
{
    public function up()
    {
        Schema::create('beneficiary_value_chain', function (Blueprint $table) {
            $table->unsignedInteger('beneficiary_id');
            $table->foreign('beneficiary_id', 'beneficiary_id_fk_2015228')->references('id')->on('beneficiaries')->onDelete('cascade');
            $table->unsignedInteger('value_chain_id');
            $table->foreign('value_chain_id', 'value_chain_id_fk_2015228')->references('id')->on('value_chains')->onDelete('cascade');
        });
    }
}
