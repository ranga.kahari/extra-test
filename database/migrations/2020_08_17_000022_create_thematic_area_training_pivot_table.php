<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThematicAreaTrainingPivotTable extends Migration
{
    public function up()
    {
        Schema::create('thematic_area_training', function (Blueprint $table) {
            $table->unsignedInteger('training_id');
            $table->foreign('training_id', 'training_id_fk_2015363')->references('id')->on('trainings')->onDelete('cascade');
            $table->unsignedInteger('thematic_area_id');
            $table->foreign('thematic_area_id', 'thematic_area_id_fk_2015363')->references('id')->on('thematic_areas')->onDelete('cascade');
        });
    }
}
