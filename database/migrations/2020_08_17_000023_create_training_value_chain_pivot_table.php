<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingValueChainPivotTable extends Migration
{
    public function up()
    {
        Schema::create('training_value_chain', function (Blueprint $table) {
            $table->unsignedInteger('training_id');
            $table->foreign('training_id', 'training_id_fk_2015364')->references('id')->on('trainings')->onDelete('cascade');
            $table->unsignedInteger('value_chain_id');
            $table->foreign('value_chain_id', 'value_chain_id_fk_2015364')->references('id')->on('value_chains')->onDelete('cascade');
        });
    }
}
