<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitatorsTable extends Migration
{
    public function up()
    {
        Schema::create('facilitators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('type')->default('Facilitator');
            $table->string('contact')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
