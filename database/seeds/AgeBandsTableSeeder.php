<?php

use App\Models\AgeBand;
use Illuminate\Database\Seeder;

class AgeBandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $age_bands = [
            ['id' => 1, 'name' => '0 - 10yrs', 'lower_limit' => 0, 'upper_limit' => 10, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 2, 'name' => '10 - 20yrs', 'lower_limit' => 10, 'upper_limit' => 20, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 3, 'name' => '20 - 30yrs', 'lower_limit' => 20, 'upper_limit' => 30, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 4, 'name' => '30 - 40yrs', 'lower_limit' => 30, 'upper_limit' => 40, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 5, 'name' => '40 - 50yrs', 'lower_limit' => 40, 'upper_limit' => 50, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 6, 'name' => '50 - 60yrs', 'lower_limit' => 50, 'upper_limit' => 60, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 7, 'name' => '60 - 70yrs', 'lower_limit' => 60, 'upper_limit' => 70, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 8, 'name' => '70 - 80yrs', 'lower_limit' => 70, 'upper_limit' => 80, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 9, 'name' => '80 - 90yrs', 'lower_limit' => 80, 'upper_limit' => 90, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 10, 'name' => '90 - 100yrs', 'lower_limit' => 90, 'upper_limit' => 100, 'created_at' => '2018-04-14 16:00:38'],
            ['id' => 11, 'name' => 'above 100yrs', 'lower_limit' => 100, 'upper_limit' => 1000, 'created_at' => '2018-04-14 16:00:38'],
        ];

        AgeBand::insert($age_bands);
    }
}
