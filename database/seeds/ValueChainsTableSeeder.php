<?php

use App\Models\ValueChain;
use Illuminate\Database\Seeder;

class ValueChainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $value_chains = [
            ['id' => 1, 'name' => 'Maize', 'categories' => 'Food Security & Commercial'],
            ['id' => 2, 'name' => 'Orange maize ', 'categories' => 'Food Security & Commercial'],
            ['id' => 3, 'name' => 'Ground nuts ', 'categories' => 'Food Security & Commercial'],
            ['id' => 4, 'name' => 'Groundnut seed', 'categories' => 'Food Security & Commercial'],
            ['id' => 5, 'name' => 'Soyabeans', 'categories' => 'Food Security & Commercial'],
            ['id' => 6, 'name' => 'Sugarbeans', 'categories' => 'Food Security & Commercial'],
            ['id' => 7, 'name' => 'Nua 45 Iron & Zinc Sugarbeans', 'categories' => 'Food Security & Commercial'],
            ['id' => 8, 'name' => 'Cowpea', 'categories' => 'Food Security & Commercial'],
            ['id' => 9, 'name' => 'Cowpea seed', 'categories' => 'Food Security & Commercial'],
            ['id' => 10, 'name' => 'Small Grains', 'categories' => 'Food Security & Commercial'],
            ['id' => 11, 'name' => 'Pearl millet', 'categories' => 'Food Security & Commercial'],
            ['id' => 12, 'name' => 'Sesame', 'categories' => 'Food Security & Commercial'],
            ['id' => 13, 'name' => 'Paprika', 'categories' => 'Food Security & Commercial'],
            ['id' => 14, 'name' => 'Horticulture', 'categories' => 'Food Security & Commercial'],
            ['id' => 15, 'name' => 'Sweet potatoes', 'categories' => 'Food Security & Commercial'],
            ['id' => 16, 'name' => 'Mungbean', 'categories' => 'Food Security & Commercial'],
            ['id' => 17, 'name' => 'Goats', 'categories' => 'Food Security & Commercial'],
            ['id' => 18, 'name' => 'Cattle', 'categories' => 'Food Security & Commercial'],
            ['id' => 19, 'name' => 'Piggery', 'categories' => 'Food Security & Commercial'],
            ['id' => 20, 'name' => 'Fisheries', 'categories' => 'Food Security & Commercial'],
            ['id' => 21, 'name' => 'Apiculture', 'categories' => 'Food Security & Commercial'],
            ['id' => 22, 'name' => 'Broilers', 'categories' => 'Food Security & Commercial'],
            ['id' => 23, 'name' => 'Indigenous Chicken', 'categories' => 'Food Security & Commercial'],
            ['id' => 24, 'name' => 'Layers', 'categories' => 'Food Security & Commercial'],
            ['id' => 25, 'name' => 'Other (Specify)', 'categories' => 'Food Security & Commercial'],
        ];

        ValueChain::insert($value_chains);
    }
}
