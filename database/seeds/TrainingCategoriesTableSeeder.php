<?php

use App\Models\TrainingCategory;
use Illuminate\Database\Seeder;

class TrainingCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $training_categories = [
            ['id' => 1, 'thematic_area_id' => 3, 'name' => 'Biofortification'],
            ['id' => 2, 'thematic_area_id' => 2, 'name' => 'Enterprise or Business Development Training'],
            ['id' => 3, 'thematic_area_id' => 1, 'name' => 'Farmer Field Schools'],
            ['id' => 4, 'thematic_area_id' => 2, 'name' => 'Financial Literacy'],
            ['id' => 5, 'thematic_area_id' => 4, 'name' => 'Gender Mainstreaming'],
            ['id' => 6, 'thematic_area_id' => 2, 'name' => 'ISAL methodology training'],
            ['id' => 7, 'thematic_area_id' => 1, 'name' => 'Livestock Management'],
            ['id' => 8, 'thematic_area_id' => 3, 'name' => 'Nutrition Awareness'],
            ['id' => 9, 'thematic_area_id' => 3, 'name' => 'Nutrition Sensitive Agriculture'],
            ['id' => 10, 'thematic_area_id' => 3, 'name' => 'Post Harvest Handling and Storage- PHHS'],
            ['id' => 11, 'thematic_area_id' => 1, 'name' => 'Post harvest management'],
            ['id' => 12, 'thematic_area_id' => 1, 'name' => 'Value chain Training(GAP)'],
        ];

        TrainingCategory::insert($training_categories);
    }
}
