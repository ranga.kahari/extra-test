<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            // User Management
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,

            // System Config
            WardsTableSeeder::class,
            VillagesTableSeeder::class,

            // Dimensions
            ThematicAreasTableSeeder::class,
            AgeBandsTableSeeder::class,
            ValueChainsTableSeeder::class,
            TrainingCategoriesTableSeeder::class,
            TopicsTableSeeder::class,
            
            // Database
            //GroupsTableSeeder::class,
            Beneficiaries1TableSeeder::class,
            Beneficiaries2TableSeeder::class,
            // Beneficiaries3TableSeeder::class,
            // Beneficiaries4TableSeeder::class,
            // Beneficiaries5TableSeeder::class,
            
            
            //BeneficiaryGroupTableSeeder::class,
            FacilitatorsTableSeeder::class,
            //FacilitatorGroupTableSeeder::class,
        ]);
    }
}
