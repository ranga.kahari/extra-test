<?php

use App\Models\Topic;
use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topics = [
            ['id' => 1, 'training_category_id' => 12, 'name' => 'Land Preparation and planting'],
            ['id' => 2, 'training_category_id' => 12, 'name' => 'Soil fertility management'],
            ['id' => 3, 'training_category_id' => 12, 'name' => 'Weed management'],
            ['id' => 4, 'training_category_id' => 12, 'name' => 'Integrated pest and disease management'],
            ['id' => 5, 'training_category_id' => 12, 'name' => 'Climate Smart Agriculture'],
            ['id' => 6, 'training_category_id' => 12, 'name' => 'Water Management'],
            ['id' => 7, 'training_category_id' => 12, 'name' => 'Fertilizer Application'],
            ['id' => 8, 'training_category_id' => 12, 'name' => 'Preplanting'],
            ['id' => 9, 'training_category_id' => 12, 'name' => 'Soil sampling'],
            ['id' => 10, 'training_category_id' => 12, 'name' => 'Wether forecast'],
            ['id' => 11, 'training_category_id' => 11, 'name' => 'Cleaning and grading'],
            ['id' => 12, 'training_category_id' => 11, 'name' => 'Grain storage methods'],
            ['id' => 13, 'training_category_id' => 3, 'name' => 'How to select a FFS'],
            ['id' => 14, 'training_category_id' => 3, 'name' => 'Selection of participants in FFS'],
            ['id' => 15, 'training_category_id' => 3, 'name' => 'Committee selection'],
            ['id' => 16, 'training_category_id' => 3, 'name' => 'How to conduct an Agro-ecosystems analysis'],
            ['id' => 17, 'training_category_id' => 7, 'name' => 'Dipping and vaccination'],
            ['id' => 18, 'training_category_id' => 7, 'name' => 'Housing '],
            ['id' => 19, 'training_category_id' => 7, 'name' => 'Supplimentary Feeding'],
            ['id' => 20, 'training_category_id' => 7, 'name' => 'Feed formulation'],
            ['id' => 21, 'training_category_id' => 7, 'name' => 'Beef survival, drought mitigation'],
            ['id' => 22, 'training_category_id' => 7, 'name' => 'Improved breeding'],
            ['id' => 23, 'training_category_id' => 7, 'name' => 'Cattle handling facilities'],
            ['id' => 24, 'training_category_id' => 8, 'name' => 'Food fairs'],
            ['id' => 25, 'training_category_id' => 8, 'name' => 'Garden days'],
            ['id' => 26, 'training_category_id' => 8, 'name' => 'Dry shows'],
            ['id' => 27, 'training_category_id' => 8, 'name' => 'Agriculture Shows'],
            ['id' => 28, 'training_category_id' => 8, 'name' => 'Seed fairs'],
            ['id' => 29, 'training_category_id' => 8, 'name' => 'Field days'],
            ['id' => 30, 'training_category_id' => 8, 'name' => 'Livestock days'],
            ['id' => 31, 'training_category_id' => 8, 'name' => 'Food processing'],
            ['id' => 32, 'training_category_id' => 8, 'name' => 'Farmer’s day'],
            ['id' => 33, 'training_category_id' => 10, 'name' => 'Aflatoxin Management Training'],
            ['id' => 34, 'training_category_id' => 10, 'name' => 'Grain grading and cleaning'],
            ['id' => 35, 'training_category_id' => 10, 'name' => 'Preparing for the harvest'],
            ['id' => 36, 'training_category_id' => 10, 'name' => 'Transportation'],
            ['id' => 37, 'training_category_id' => 10, 'name' => 'Drying'],
            ['id' => 38, 'training_category_id' => 10, 'name' => 'Shelling/ Threshing'],
            ['id' => 39, 'training_category_id' => 10, 'name' => 'Preservation'],
            ['id' => 40, 'training_category_id' => 10, 'name' => 'Storage structures-metal silos, brick granary, krib, hermetic bags'],
            ['id' => 41, 'training_category_id' => 10, 'name' => 'Technologies- Solar driers, groundnut shellers, millet threshers, maize threshers'],
            ['id' => 42, 'training_category_id' => 9, 'name' => 'Healthy Harvest(Foodgroups, planning what to produce, malnutrition, nutrition in the life cycle, health and hygiene)'],
            ['id' => 43, 'training_category_id' => 9, 'name' => 'Food Security Value Chains'],
            ['id' => 44, 'training_category_id' => 1, 'name' => 'Nua45 beans'],
            ['id' => 45, 'training_category_id' => 1, 'name' => ' Pro-vitamin A maize'],
            ['id' => 46, 'training_category_id' => 6, 'name' => 'Introduction to the study circle methodology'],
            ['id' => 47, 'training_category_id' => 6, 'name' => 'Introduction to the rotational learning systems'],
            ['id' => 48, 'training_category_id' => 6, 'name' => 'Money'],
            ['id' => 49, 'training_category_id' => 6, 'name' => 'Savings'],
            ['id' => 50, 'training_category_id' => 6, 'name' => 'Investments'],
            ['id' => 51, 'training_category_id' => 6, 'name' => 'ISAL model and the required steps: Member selection, leadership, constitution, fund development and record keeping.'],
            ['id' => 52, 'training_category_id' => 6, 'name' => 'Loan application and appraisals'],
            ['id' => 53, 'training_category_id' => 6, 'name' => 'Record keeping'],
            ['id' => 54, 'training_category_id' => 4, 'name' => 'Personal financial management'],
            ['id' => 55, 'training_category_id' => 4, 'name' => 'Savings'],
            ['id' => 56, 'training_category_id' => 4, 'name' => 'Managing Loans'],
            ['id' => 57, 'training_category_id' => 4, 'name' => 'Farming as a business'],
            ['id' => 58, 'training_category_id' => 4, 'name' => 'Risk management and insurance'],
            ['id' => 59, 'training_category_id' => 4, 'name' => 'Financial service providers'],
            ['id' => 60, 'training_category_id' => 4, 'name' => 'Planning for old age / retirement'],
            ['id' => 61, 'training_category_id' => 2, 'name' => 'Entrepreneurship'],
            ['id' => 62, 'training_category_id' => 2, 'name' => 'Business planning'],
            ['id' => 63, 'training_category_id' => 2, 'name' => 'Costing and cost management'],
            ['id' => 64, 'training_category_id' => 2, 'name' => 'Buying'],
            ['id' => 65, 'training_category_id' => 2, 'name' => 'Stock management'],
            ['id' => 66, 'training_category_id' => 2, 'name' => 'Record Keeping'],
            ['id' => 67, 'training_category_id' => 2, 'name' => 'Market research'],
            ['id' => 68, 'training_category_id' => 2, 'name' => 'Marketing'],
            ['id' => 69, 'training_category_id' => 2, 'name' => 'Commodity Associations'],
            ['id' => 70, 'training_category_id' => 2, 'name' => 'Market information '],
            ['id' => 71, 'training_category_id' => 2, 'name' => 'Risks and risk management'],
            ['id' => 72, 'training_category_id' => 2, 'name' => 'Contract farming '],
            ['id' => 73, 'training_category_id' => 5, 'name' => 'Likes and Dislikes'],
            ['id' => 74, 'training_category_id' => 5, 'name' => ' Men and women diamond'],
            ['id' => 75, 'training_category_id' => 5, 'name' => ' Mother Diamond '],
            ['id' => 76, 'training_category_id' => 5, 'name' => 'Challenge action tree '],
            ['id' => 77, 'training_category_id' => 5, 'name' => 'Vision Journey '],
            ['id' => 78, 'training_category_id' => 5, 'name' => 'Gender balance tree '],
            ['id' => 79, 'training_category_id' => 5, 'name' => 'Social empowerment map '],
            ['id' => 80, 'training_category_id' => 5, 'name' => 'Multi-lane highway'],
            ['id' => 81, 'training_category_id' => 5, 'name' => 'Value Chain mapping'],
            ['id' => 82, 'training_category_id' => 5, 'name' => 'Gender Dialogue'],
            ['id' => 83, 'training_category_id' => 5, 'name' => 'Rural women transformational leadership'],
        ];

        Topic::insert($topics);
    }
}
