<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'support@luwami.com',
                'password'       => bcrypt('$vbK7(e!H[T$tmR9'),
                'remember_token' => null,
                'created_at'     => '2019-05-25 17:47:19',
                'updated_at'     => '2019-05-25 17:47:19',
                'deleted_at'     => null,
            ],
            [
                'id'             => 2,
                'name'           => 'Japhet Muchazondida',
                'email'          => 'muchazondidaj@gmail.com',
                'password'       => bcrypt('$vbK77(e!H[T$tmR9'),
                'remember_token' => null,
                'created_at'     => '2019-05-25 17:47:19',
                'updated_at'     => '2019-05-25 17:47:19',
                'deleted_at'     => null,
            ]
        ];

        User::insert($users);
    }
}
