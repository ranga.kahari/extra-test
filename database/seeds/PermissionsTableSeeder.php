<?php

use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'name' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'name' => 'permission_create',
            ],
            [
                'id'    => 3,
                'name' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'name' => 'permission_show',
            ],
            [
                'id'    => 5,
                'name' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'name' => 'permission_access',
            ],
            [
                'id'    => 7,
                'name' => 'role_create',
            ],
            [
                'id'    => 8,
                'name' => 'role_edit',
            ],
            [
                'id'    => 9,
                'name' => 'role_show',
            ],
            [
                'id'    => 10,
                'name' => 'role_delete',
            ],
            [
                'id'    => 11,
                'name' => 'role_access',
            ],
            [
                'id'    => 12,
                'name' => 'user_create',
            ],
            [
                'id'    => 13,
                'name' => 'user_edit',
            ],
            [
                'id'    => 14,
                'name' => 'user_show',
            ],
            [
                'id'    => 15,
                'name' => 'user_delete',
            ],
            [
                'id'    => 16,
                'name' => 'user_access',
            ],
            [
                'id'    => 17,
                'name' => 'system_config_access',
            ],
            [
                'id'    => 18,
                'name' => 'ward_show',
            ],
            [
                'id'    => 19,
                'name' => 'ward_access',
            ],
            [
                'id'    => 20,
                'name' => 'village_create',
            ],
            [
                'id'    => 21,
                'name' => 'village_edit',
            ],
            [
                'id'    => 22,
                'name' => 'village_show',
            ],
            [
                'id'    => 23,
                'name' => 'village_delete',
            ],
            [
                'id'    => 24,
                'name' => 'village_access',
            ],
            [
                'id'    => 25,
                'name' => 'dimension_access',
            ],
            [
                'id'    => 26,
                'name' => 'thematic_area_create',
            ],
            [
                'id'    => 27,
                'name' => 'thematic_area_edit',
            ],
            [
                'id'    => 28,
                'name' => 'thematic_area_show',
            ],
            [
                'id'    => 29,
                'name' => 'thematic_area_delete',
            ],
            [
                'id'    => 30,
                'name' => 'thematic_area_access',
            ],
            [
                'id'    => 31,
                'name' => 'age_band_create',
            ],
            [
                'id'    => 32,
                'name' => 'age_band_edit',
            ],
            [
                'id'    => 33,
                'name' => 'age_band_show',
            ],
            [
                'id'    => 34,
                'name' => 'age_band_delete',
            ],
            [
                'id'    => 35,
                'name' => 'age_band_access',
            ],
            [
                'id'    => 36,
                'name' => 'value_chain_create',
            ],
            [
                'id'    => 37,
                'name' => 'value_chain_edit',
            ],
            [
                'id'    => 38,
                'name' => 'value_chain_show',
            ],
            [
                'id'    => 39,
                'name' => 'value_chain_delete',
            ],
            [
                'id'    => 40,
                'name' => 'value_chain_access',
            ],
            [
                'id'    => 41,
                'name' => 'training_category_create',
            ],
            [
                'id'    => 42,
                'name' => 'training_category_edit',
            ],
            [
                'id'    => 43,
                'name' => 'training_category_show',
            ],
            [
                'id'    => 44,
                'name' => 'training_category_delete',
            ],
            [
                'id'    => 45,
                'name' => 'training_category_access',
            ],
            [
                'id'    => 46,
                'name' => 'topic_create',
            ],
            [
                'id'    => 47,
                'name' => 'topic_edit',
            ],
            [
                'id'    => 48,
                'name' => 'topic_show',
            ],
            [
                'id'    => 49,
                'name' => 'topic_delete',
            ],
            [
                'id'    => 50,
                'name' => 'topic_access',
            ],
            [
                'id'    => 51,
                'name' => 'database_access',
            ],
            [
                'id'    => 52,
                'name' => 'facilitator_create',
            ],
            [
                'id'    => 53,
                'name' => 'facilitator_edit',
            ],
            [
                'id'    => 54,
                'name' => 'facilitator_show',
            ],
            [
                'id'    => 55,
                'name' => 'facilitator_delete',
            ],
            [
                'id'    => 56,
                'name' => 'facilitator_access',
            ],
            [
                'id'    => 57,
                'name' => 'group_create',
            ],
            [
                'id'    => 58,
                'name' => 'group_edit',
            ],
            [
                'id'    => 59,
                'name' => 'group_show',
            ],
            [
                'id'    => 60,
                'name' => 'group_delete',
            ],
            [
                'id'    => 61,
                'name' => 'group_access',
            ],
            [
                'id'    => 62,
                'name' => 'beneficiary_create',
            ],
            [
                'id'    => 63,
                'name' => 'beneficiary_edit',
            ],
            [
                'id'    => 64,
                'name' => 'beneficiary_show',
            ],
            [
                'id'    => 65,
                'name' => 'beneficiary_delete',
            ],
            [
                'id'    => 66,
                'name' => 'beneficiary_access',
            ],
            [
                'id'    => 67,
                'name' => 'training_create',
            ],
            [
                'id'    => 68,
                'name' => 'training_edit',
            ],
            [
                'id'    => 69,
                'name' => 'training_show',
            ],
            [
                'id'    => 70,
                'name' => 'training_delete',
            ],
            [
                'id'    => 71,
                'name' => 'training_access',
            ],
            [
                'id'    => 72,
                'name' => 'attendance_create',
            ],
            [
                'id'    => 73,
                'name' => 'attendance_edit',
            ],
            [
                'id'    => 74,
                'name' => 'attendance_show',
            ],
            [
                'id'    => 75,
                'name' => 'attendance_delete',
            ],
            [
                'id'    => 76,
                'name' => 'attendance_access',
            ],
            [
                'id'    => 77,
                'name' => 'profile_password_edit',
            ],
            [
                'id'    => 78,
                'name' => 'report_access',
            ],
            [
                'id'    => 79,
                'name' => 'valuechainreport_access',
            ],
            [
                'id'    => 80,
                'name' => 'topicreport_access',
            ],
            [
                'id'    => 81,
                'name' => 'vcts_create',
            ],
            [
                'id'    => 82,
                'name' => 'vcts_edit',
            ],
            [
                'id'    => 83,
                'name' => 'vcts_show',
            ],
            [
                'id'    => 84,
                'name' => 'vcts_delete',
            ],
            [
                'id'    => 85,
                'name' => 'vcts_access',
            ],
            
             [
                'id'    => 86,
                'name' => 'caregroup_create',
            ],
            [
                'id'    => 87,
                'name' => 'caregroup_edit',
            ],
            [
                'id'    => 88,
                'name' => 'caregroup_show',
            ],
            [
                'id'    => 89,
                'name' => 'caregroup_delete',
            ],
            [
                'id'    => 90,
                'name' => 'caregroup_access',
            ],
            [
                'id'    => 91,
                'name' => 'intervention_create',
            ],
            [
                'id'    => 92,
                'name' => 'intervention_edit',
            ],
            [
                'id'    => 93,
                'name' => 'intervention_show',
            ],
            [
                'id'    => 94,
                'name' => 'intervention_delete',
            ],
            [
                'id'    => 95,
                'name' => 'intervention_access',
            ],
            
            [
                'id'    => 96,
                'name' => 'distribution_create',
            ],
            [
                'id'    => 97,
                'name' => 'distribution_edit',
            ],
            [
                'id'    => 98,
                'name' => 'distribution_show',
            ],
            [
                'id'    => 99,
                'name' => 'distribution_delete',
            ],
            [
                'id'    => 100,
                'name' => 'distribution_access',
            ],
            
            [
                'id'    => 101,
                'name' => 'crop_create',
            ],
            [
                'id'    => 102,
                'name' => 'crop_edit',
            ],
            [
                'id'    => 103,
                'name' => 'crop_show',
            ],
            [
                'id'    => 104,
                'name' => 'crop_delete',
            ],
            [
                'id'    => 105,
                'name' => 'crop_access',
            ],
            
            [
                'id'    => 106,
                'name' => 'pfumvudza_create',
            ],
            [
                'id'    => 107,
                'name' => 'pfumvudza_edit',
            ],
            [
                'id'    => 108,
                'name' => 'pfumvudza_show',
            ],
            [
                'id'    => 109,
                'name' => 'pfumvudza_delete',
            ],
            [
                'id'    => 110,
                'name' => 'pfumvudza_access',
            ],
            
            [
                'id'    => 111,
                'name' => 'isals_create',
            ],
            [
                'id'    => 112,
                'name' => 'isals_edit',
            ],
            [
                'id'    => 113,
                'name' => 'isals_show',
            ],
            [
                'id'    => 114,
                'name' => 'isals_delete',
            ],
            [
                'id'    => 115,
                'name' => 'isals_access',
            ],
            
            [
                'id'    => 116,
                'name' => 'teacher_create',
            ],
            [
                'id'    => 117,
                'name' => 'teacher_edit',
            ],
            [
                'id'    => 118,
                'name' => 'teacher_show',
            ],
            [
                'id'    => 119,
                'name' => 'teacher_delete',
            ],
            [
                'id'    => 120,
                'name' => 'teacher_access',
            ],
            [
                'id'    => 121,
                'name' => 'people_create',
            ],
            [
                'id'    => 122,
                'name' => 'people_edit',
            ],
            [
                'id'    => 123,
                'name' => 'people_show',
            ],
            [
                'id'    => 124,
                'name' => 'people_delete',
            ],
            [
                'id'    => 125,
                'name' => 'people_access',
            ],
            
            [
                'id'    => 126,
                'name' => 'smart_create',
            ],
            [
                'id'    => 127,
                'name' => 'smart_edit',
            ],
            [
                'id'    => 128,
                'name' => 'smart_show',
            ],
            [
                'id'    => 129,
                'name' => 'smart_delete',
            ],
            [
                'id'    => 130,
                'name' => 'smart_access',
            ],
            
        ];
        
        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission['name']
            ]);
        }
    }
}
