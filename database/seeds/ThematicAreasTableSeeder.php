<?php

use App\Models\ThematicArea;
use Illuminate\Database\Seeder;

class ThematicAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $thematic_areas = [
            ['id' => 1, 'name' => 'Extension & Advisory Services', 'label' => 'EAS', 'slug' => 'eas', 'show_in_side_menu' => 1, 'order' => 1],
            ['id' => 2, 'name' => 'Biofortification', 'label' => 'Biofortification', 'slug' => 'biofortification', 'show_in_side_menu' => 1, 'order' => 2],
            ['id' => 3, 'name' => 'Nutrition', 'label' => 'Nutrition', 'slug' => 'nutrition', 'show_in_side_menu' => 1, 'order' => 3],
            ['id' => 4, 'name' => 'Rural Finance', 'label' => 'Rural Finance', 'slug' => 'rural-finance', 'show_in_side_menu' => 1, 'order' => 4],
            ['id' => 5, 'name' => 'Markets', 'label' => 'Markets', 'slug' => 'markets', 'show_in_side_menu' => 1, 'order' => 5],
            ['id' => 6, 'name' => 'GALs', 'label' => 'GALs', 'slug' => 'gals', 'show_in_side_menu' => 1, 'order' => 6],
            ['id' => 7, 'name' => 'Gender', 'label' => 'Gender', 'slug' => 'gender', 'show_in_side_menu' => 1, 'order' => 7],
        ];

        ThematicArea::insert($thematic_areas);
    }
}
