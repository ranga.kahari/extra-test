<?php

namespace App\Http\Controllers\Admin;

use App\Exports\YieldDataExport;
use App\Models\Crop;
use App\Models\Beneficiary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\CropImport;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Symfony\Component\HttpFoundation\Response;

class CropsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('crop_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Crop::query()->select(sprintf('%s.*', (new Crop)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'crop_show';
                $editGate      = 'crop_edit';
                $deleteGate    = 'crop_delete';
                $crudRoutePart = 'crops';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('season', function ($row) {
                return $row->season ? $row->season : "";
            });
            $table->editColumn('value_chain', function ($row) {
                return $row->value_chain ? Crop::ValueChain[$row->value_chain] : '';
            });
            $table->editColumn('area_planted', function ($row) {
                return $row->area_planted ? $row->area_planted : "";
            });

            $table->editColumn('yield', function ($row) {
                return $row->yield ? $row->yield : "";
            });

            // $table->editColumn('production', function ($row) {
            //     return $row->production ? $row->production : "";
            // });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }


        return view('admin.crops.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.crops.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $beneficiary = Beneficiary::find($request->input('beneficiary_id')) ?? Beneficiary::updateOrCreate([
            'beneficiary_id' => $request->input('beneficiary_id')
        ]);

        $crops = Crop::create([
            'season' => $request->input('season'),
            'value_chain' => $request->input('value_chain'),
            'area_planted' => $request->input('area_planted'),
            'yield' => $request->input('yield'),
            'production' => $request->input('production'),
            'beneficiary_id' => $beneficiary->id,
            'district'=>$beneficiary->village->ward->district,
        ]);

        return redirect()->route('admin.crops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crop  $crop
     * @return \Illuminate\Http\Response
     */
    public function show(Crop $crop)
    {
        abort_if(Gate::denies('crop_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $crop->load('beneficiaries');

        return view('admin.crops.show', compact('crop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Crop  $crop
     * @return \Illuminate\Http\Response
     */
    public function edit(Crop $crop)
    {
        abort_if(Gate::denies('crop_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $crop->load('beneficiaries');
        //dd($crop);

        return view('admin.crops.edit', compact('crop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Crop  $crop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crop $crop)
    {

         $crop->update($request->all());

        return redirect()->route('admin.crops.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crop  $crop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crop $crop)
    {
        abort_if(Gate::denies('crop_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $crop->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Crop::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new CropImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }

    public function export(Excel $excel)
    {
        return Excel::download(new YieldDataExport, 'Yield-Data.xlsx');
        // return $excel->download(new TeacherExport, 'facilitators.xlsx');
        // return(new BeneficiariesExport)->download('beneficiaries.xlsx');
    }
}
