<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\Models\Ward;
use App\Models\Group;
use App\Models\Member;
use App\Models\ValueChain;
use App\Models\Facilitator;
use Illuminate\Http\Request;
use App\Imports\CareGroupImport;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\StoreGroupRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\UpdateGroupRequest;
use App\Http\Requests\MassDestroyGroupRequest;
use Symfony\Component\HttpFoundation\Response;

class CareGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         abort_if(Gate::denies('group_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');


        if ($request->ajax()) {
            $query = Group::select(sprintf('%s.*', (new Group)->table))
                ->whereIn('group_categories', [
                    'Care Group' => 'Care Group',
                ]);
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {

                $viewGate      = 'caregroup_show';
                $editGate      = 'caregroup_edit';
                $deleteGate    = 'caregroup_delete';
               $crudRoutePart = 'care-groups';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('group_categories', function ($row) {
                return $row->group_categories ? Group::GROUP_CATEGORIES_SELECT[$row->group_categories] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);

        }


        return view('admin.careGroups.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wards = Ward::all()->pluck('name', 'id');
        $facilitators = Facilitator::all()->pluck('name', 'id');

        return view('admin.careGroups.create', compact('facilitators', 'wards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroupRequest $request)
    {
        //dd($request);

        $group = Group::create($request->all());

        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $group->facilitators()->save($facilitator, $position);
        }

        return redirect("admin/care-groups/" . $group->id);

        //return redirect()->route('admin.care-groups.show', ['group' => $group->id]);
        //return redirect()->route('admin.care-groups.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {

        abort_if(Gate::denies('caregroup_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $wards = Ward::all()->pluck('name', 'id');
        $group->load('facilitators', 'members');

        return view('admin.careGroups.show', compact('group', 'wards'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        abort_if(Gate::denies('caregroup_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        //dd($group);

        $wards = Ward::all()->pluck('name', 'id');
        $facilitators = Facilitator::all()->pluck('name', 'id');
        //$value_chains = ValueChain::all()->pluck('name', 'id');

        $group->load('facilitators');

        return view('admin.careGroups.edit', compact('facilitators', 'group', 'wards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $group = group::find($id);

        $group->update($request->all());

        $group->facilitators()->detach();
        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $group->facilitators()->save($facilitator, $position);
        }

        return redirect()->route('admin.care-groups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        abort_if(Gate::denies('group_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $group->delete();

        return back();
    }

    public function massDestroy(MassDestroyGroupRequest $request)
    {
        Group::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new CareGroupImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
}
