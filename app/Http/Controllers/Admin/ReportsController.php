<?php

namespace App\Http\Controllers\Admin;

use App\Report;
use DataTables;
use App\Models\Crop;
use App\Models\Ward;
use App\Models\Group;
use App\Models\Topic;
use App\Models\Member;
use App\Models\Village;
use App\Models\Training;
use App\Models\Pfumvudza;
use App\Models\ValueChain;
use App\Models\Beneficiary;
use App\Models\Facilitator;
use App\Models\Distribution;
use App\Models\Intervention;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Models\SmartSubsidies;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }


    public function valueChainReport(Request $request)
    {

        $valueChains = ValueChain::all()->pluck('name', 'id');


        return view('admin.reports.value-chain', compact('valueChains'));
    }

    public function valueChainReportData(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('trainings')
                ->join('attendances', 'attendances.training_id', '=', 'trainings.id')
                ->join('beneficiaries', 'beneficiaries.id', '=', 'attendances.beneficiary_id')
                ->join('training_value_chain', 'training_value_chain.training_id', '=', 'trainings.id')
                ->join('value_chains', 'training_value_chain.value_chain_id', '=', 'value_chains.id')
                ->when(!empty($request->value_chain_ids), function ($query) use ($request) {
                    $query->whereIn('training_value_chain.value_chain_id', $request->value_chain_ids);
                })
                ->when($request->district, function ($query) use ($request) {
                    $query->join('villages', 'villages.id', '=', 'trainings.village_id')
                        ->join('wards', 'wards.id', '=', 'villages.ward_id')
                        ->where('wards.district', $request->district);
                })
                ->when($request->start_date, function ($query) use ($request) {
                    $query->where('start_date', '>=', $request->start_date)
                        ->when($request->end_date, function ($query) use ($request) {
                            $query->where('end_date', '<=', $request->end_date);
                        });
                })
                ->select('beneficiaries.first_name', 'beneficiaries.surname', 'beneficiaries.gender', 'beneficiaries.national_id_number', 'beneficiaries.year_of_birth', 'value_chains.name');

            $table = Datatables::of($query);

            $table->addColumn('district', function ($row) use ($request) {
                return $request->district ?? 'All';
            });

            $table->addColumn('value_chain', function ($row) use ($request) {
                return $row->name ?? 'Any';
            });


            return $table->make(true);

        }

    }

    public function valueChainReportDataGenderCounts(Request $request)
    {
        if ($request->ajax()) {
            $genderCounts = DB::table('trainings')
                ->join('attendances', 'attendances.training_id', '=', 'trainings.id')
                ->join('beneficiaries', 'beneficiaries.id', '=', 'attendances.beneficiary_id')
                ->join('training_value_chain', 'training_value_chain.training_id', '=', 'trainings.id')
                ->join('value_chains', 'training_value_chain.value_chain_id', '=', 'value_chains.id')
                ->when(!empty($request->value_chain_ids), function ($query) use ($request) {
                    $query->whereIn('training_value_chain.value_chain_id', $request->value_chain_ids);
                })
                ->when($request->district, function ($query) use ($request) {
                    $query->join('villages', 'villages.id', '=', 'trainings.village_id')
                        ->join('wards', 'wards.id', '=', 'villages.ward_id')
                        ->where('wards.district', $request->district);
                })
                ->when($request->start_date, function ($query) use ($request) {
                    $query->where('start_date', '>=', $request->start_date)
                        ->when($request->end_date, function ($query) use ($request) {
                            $query->where('end_date', '<=', $request->end_date);
                        });
                })
                ->select('beneficiaries.gender', DB::raw('count(*) as count'))
                ->groupBy('beneficiaries.gender')
                ->get();

            $total = $genderCounts->sum('count');
            $genderCounts = $genderCounts->push((object)[
                'gender' => 'total',
                'count' => $total
            ])->map(function ($item) use ($total) {
                return (object)[
                    'name' => $item->gender,
                    'count' => $item->count,
                    'percent' => $total > 0 ? round(($item->count / $total) * 100) : 0.0

                ];
            });

            return view('admin.reports.partials.gender-counts', compact(
                'genderCounts',
            ));
        }

    }


    public function topicReport(Request $request)
    {
        $topics = Topic::all()->pluck('name', 'id');

        return view('admin.reports.topic', compact('topics'));
    }

    public function topicReportData(Request $request)
    {

        if ($request->ajax()) {
            $query = DB::table('trainings')
                ->join('attendances', 'attendances.training_id', '=', 'trainings.id')
                ->join('beneficiaries', 'beneficiaries.id', '=', 'attendances.beneficiary_id')
                ->join('topic_training', 'topic_training.training_id', '=', 'trainings.id')
                ->join('topics', 'topic_training.topic_id', '=', 'topics.id')
                ->when(!empty($request->topic_ids), function ($query) use ($request) {
                    $query->whereIn('topic_training.topic_id', $request->topic_ids);
                })
                ->when($request->district, function ($query) use ($request) {
                    $query->join('villages', 'villages.id', '=', 'trainings.village_id')
                        ->join('wards', 'wards.id', '=', 'villages.ward_id')
                        ->where('wards.district', $request->district);
                })
                ->when($request->start_date, function ($query) use ($request) {
                    $query->where('start_date', '>=', $request->start_date)
                        ->when($request->end_date, function ($query) use ($request) {
                            $query->where('end_date', '<=', $request->end_date);
                        });
                })
                ->select('beneficiaries.first_name', 'beneficiaries.surname', 'beneficiaries.gender', 'beneficiaries.national_id_number', 'beneficiaries.year_of_birth', 'topics.name');

            $table = Datatables::of($query);

            $table->addColumn('district', function ($row) use ($request) {
                return $request->district ?? 'All';
            });

            $table->addColumn('topic', function ($row) use ($request) {
                return $row->name ?? 'Any';
            });

        }

        return $table->make(true);
    }

    public function topicReportDataGenderCounts(Request $request)
    {
        if ($request->ajax()) {
            $genderCounts = DB::table('trainings')
                ->join('attendances', 'attendances.training_id', '=', 'trainings.id')
                ->join('beneficiaries', 'beneficiaries.id', '=', 'attendances.beneficiary_id')
                ->join('topic_training', 'topic_training.training_id', '=', 'trainings.id')
                ->join('topics', 'topic_training.topic_id', '=', 'topics.id')
                ->when(!empty($request->topic_ids), function ($query) use ($request) {
                    $query->whereIn('topic_training.topic_id', $request->topic_ids);
                })
                ->when($request->district, function ($query) use ($request) {
                    $query->join('villages', 'villages.id', '=', 'trainings.village_id')
                        ->join('wards', 'wards.id', '=', 'villages.ward_id')
                        ->where('wards.district', $request->district);
                })
                ->when($request->start_date, function ($query) use ($request) {
                    $query->where('start_date', '>=', $request->start_date)
                        ->when($request->end_date, function ($query) use ($request) {
                            $query->where('end_date', '<=', $request->end_date);
                        });
                })
                ->select('beneficiaries.gender', DB::raw('count(*) as count'))
                ->groupBy('beneficiaries.gender')
                ->get();

            $total = $genderCounts->sum('count');
            $genderCounts = $genderCounts->push((object)[
                'gender' => 'total',
                'count' => $total
            ])->map(function ($item) use ($total) {
                return (object)[
                    'name' => $item->gender,
                    'count' => $item->count,
                    'percent' => $total > 0 ? round(($item->count / $total) * 100) : 0.0

                ];
            });

            return view('admin.reports.partials.gender-counts', compact(
                'genderCounts',
            ));

        }
    }


    public function careGroupReport(Request $request)
    {
        $careGroupCounts = DB::table('groups')
            ->select('group_categories', DB::raw('count(*) as count'))
            ->groupBy('group_categories')
            ->whereIn('group_categories', [
                'Care Group' => 'Care Group',
            ])->groupBy('group_district')->get();

        $total = $careGroupCounts->sum('count');
        $careGroupCounts = $careGroupCounts->push((object)[
            'group_categories' => 'total',
            'count' => $total
        ])->map(function ($item) use ($total) {
            return (object)[
                'name' => $item->group_categories,
                'count' => $item->count,
                'percent' => $total > 0 ? round(($item->count / $total) * 100) : 0.0
            ];
        });

        $wards = Ward::all()->pluck('name', 'id');

        $members = "";
        $memberCount = 0;
        $groupCount = 0;
        $careGroups = Group::query()->where('health_centre_ward', '')->get();

        return view('admin.reports.care-group', compact('wards', 'members', 'memberCount', 'groupCount', 'careGroups'));
    }

    public function careGroupReportSearch(Request $request)
    {

        $district = $request->input('district');
        $ward = $request->input('wards');


        if ($district != "" && $ward != "") {

            $careGroups = Group::query()->where('group_district', $district)->where('group_categories', 'Care Group')->where('health_centre_ward', $ward)->get();
            $members = "";
            $memberCount = 0;
            $groupCount = 0;
            foreach ($careGroups as $careGroup) {
                $memberCount += Member::query()->where('group_id', $careGroup->id)->get()->count();
                $groupCount++;
            }

            $wards = Ward::all()->pluck('name', 'id');

            return view('admin.reports.care-group', compact('wards', 'members', 'memberCount', 'groupCount', 'careGroups'));

        } elseif ($district != "" && $ward == null) {

            $careGroups = Group::query()->where('group_district', $district)->where('group_categories', 'Care Group')->get();
            $members = "";
            $memberCount = 0;
            $groupCount = 0;
            foreach ($careGroups as $careGroup) {
                $memberCount += Member::query()->where('group_id', $careGroup->id)->get()->count();
                $groupCount++;
            }

            $wards = Ward::all()->pluck('name', 'id');

            return view('admin.reports.care-group', compact('wards', 'members', 'memberCount', 'groupCount', 'careGroups'));
        } else {
            return redirect()->route('admin.reports.care-group');
        }

    }

    public function careGroupFacilitatorReport(Request $request)
    {

        $leadMothers = array();
        $counting = 0;
        $facilitators = Facilitator::query()->where("type", 'Village Health Worker')->paginate(1000);


        $wards = Ward::all()->pluck('name', 'id');

        return view('admin.reports.care-group-facilitator', compact('leadMothers', 'wards', 'facilitators'));
    }

    public function careGroupReportSearchByFacilitator(Request $request)
    {
        $district = $request->input('district');
        $ward = $request->input('wards');

        $wards = Ward::all()->pluck('name', 'id');

        //dd($district);
        //dd($wards);

        if ($district != null && $ward != null) {

            $facilitators = Facilitator::query()->where('district', $district)->where('wards', $ward)->where('type', 'Village Health Worker')->paginate(1000);

        } elseif ($district != null && $ward == null) {

            $facilitators = Facilitator::query()->where("district", $district)->where('type', 'Village Health Worker')->paginate(1000);
        } else {
            return redirect()->route('admin.reports.care-group-facilitator');;
        }

        return view('admin.reports.care-group-facilitator', compact('facilitators', 'wards'));


    }

    public function distribution()
    {
        // $distributions = Distribution::all();

        // $distribution = DB::table('distributions')
        //     ->select('item_distributed' ,DB::raw('count(*) as count'))
        //     ->groupBy('item_distributed')
        //     ->get();
        //     $distributions->load('beneficiaries');
        //     $distributions=$distributions->groupBy('item_distributed');
        //     dd($distributions);
        //     foreach ($distributions as $d) {
        //         dd($d);
        //     }
        //    dd($distribution);

        $distributions = DB::table('distributions')
            ->select('item_distributed', DB::raw('count(*) as count'))
            ->groupBy('item_distributed')
            ->get();


        return view('admin.reports.distribution', compact('distributions'));
    }

    public function intervention()
    {
        $interventions = [];
        return view('admin.reports.intervention', compact('interventions'));
    }

    public function interventionSearch(Request $request)
    {
        $district = $request->input('district');
        // $wards = Ward::query()->where('district', $district)->get();
        // $villages = array();
        // $interventions = array();
        // $check = array();
        // foreach ($wards as $ward) {
        //     $villages[] = Village::query()->where('ward_id', $ward->id)->get();
        // }
        // for ($i = 0; $i < count($villages); $i++) {
        //     if (count($villages[$i]) > 0) {
        //         for ($j = 0; $j < count($villages[$i]); $j++) {
        //             $inter = Intervention::query()->find($villages[$i][$j]->id);
        //             if ($inter != null) {
        //                 $name = $inter->intervention;
        //                 if (!in_array($name, $check)) {
        //                     $check[] = $name;
        //                     $interventions[] = $inter;
        //                 }
        //             }
        //         }
        //     }
        // }
        $intervents=new Intervention();
        $interventions=Intervention::with('village')->get()->groupBy(['village.ward.district','intervention']);
        //dd($interventions);
        return view('admin.reports.intervention', compact('interventions','intervents'));
    }
    
    public function vcts()
    {
        $isals = array();
        return view('admin.reports.isal', compact('isals'));
    }

    public function vctsSearch(Request $request)
    {
        // $query = Group::select(sprintf('%s.*', (new Group)->table))
        //         ->whereIn('group_categories', [
        //             'ISALS Group' => 'ISALS Group',
        //         ]);
                
        $isals = Group::all()->groupBy(['group_district','isals_categories']);
        
       // dd($isals);
        return view('admin.reports.isal', compact('isals'));
    }

    public function isal()
    {
        $isals = array();
        return view('admin.reports.isal', compact('isals'));
    }

    public function isalSearch(Request $request)
    {     
        // $isals = DB::table('groups')
        //         ->select('group_categories')
        //         ->groupBy('group_categories')
        //         ->whereIn('group_categories', [
        //         'ISALS Group' => 'ISALS Group',
        //     ])
        //     ->groupBy(['group_district','isals_categories']);
                
        $isals = Group::all()->groupBy(['group_district','isals_categories']);
        
       // dd($isals);
        return view('admin.reports.isal', compact('isals'));
    }

    public function yieldData()
    {
        $yieldData = Crop::all()->groupBy(['district','season', 'value_chain']);
        return view('admin.reports.yield-data', compact('yieldData'));
    }

    public function yieldDataSearch(Request $request)
    {

        $yieldData = Crop::all()->groupBy(['district','season', 'value_chain']);
        return view('admin.reports.yield-data', compact('yieldData'));
    }

    public function pfumvudza()
    {
        $pfumvudzas = array();
        return view('admin.reports.pfumvudza', compact('pfumvudzas'));
    }

    public function pfumvudzaSearch(Request $request)
    {

        $pfumvudzas = Pfumvudza::all()->groupBy(['district','agronomic_practice']);
        $pfumvu = new Pfumvudza();
        return view('admin.reports.pfumvudza', compact('pfumvudzas', 'pfumvu'));
    }
     public function pfumvudzaValueChain()
    {

        $valuechain=array();
        
        return view('admin.reports.pfumvudza-value-chain', compact( 'valuechain'));
    }
     public function pfumvudzaValueChainSearch(Request $request)
    {

        $valuechain= Pfumvudza::all()->groupBy(['district','value_chain']);
        $pfumvu = new Pfumvudza();

        return view('admin.reports.pfumvudza-value-chain', compact('valuechain', 'pfumvu'));
    }

    public function smartSubsidy()
    {
        $smart = array();
        return view('admin.reports.smart-subsidy', compact('smart'));
    }

    public function smartSubsidySearch(Request $request)
    {
        $smart = SmartSubsidies::with('ward')->get()->groupBy(['ward.district', 'value_chain']);
        $smartSubsidies = new SmartSubsidies();
        return view('admin.reports.smart-subsidy', compact('smart', 'smartSubsidies'));
    }

}
