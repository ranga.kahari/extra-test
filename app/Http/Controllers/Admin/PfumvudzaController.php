<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PfumvudzaExport;
use Gate;
use App\Models\Pfumvudza;
use App\Models\Beneficiary;
use App\Models\Facilitator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\PfumvudzaImport;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Symfony\Component\HttpFoundation\Response;


class PfumvudzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('pfumvudza_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Pfumvudza::query()->select(sprintf('%s.*', (new Pfumvudza)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'pfumvudza_show';
                $editGate = 'pfumvudza_edit';
                $deleteGate = 'pfumvudza_delete';
                $crudRoutePart = 'pfumvudza';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });

            $table->editColumn('agronomic_practice', function ($row) {
                return $row->agronomic_practice ? Pfumvudza::AGRONOMIC_PRACTICE[$row->agronomic_practice] : '';
            });

            $table->editColumn('value_chain', function ($row) {
                return $row->value_chain ? Pfumvudza::ValueChain[$row->value_chain] : '';
            });

            $table->editColumn('variety', function ($row) {
                return $row->variety ? $row->variety : "";
            });


            $table->editColumn('area_planted', function ($row) {
                return $row->area_planted ? $row->area_planted : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.pfumvudza.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facilitators = Facilitator::all()->pluck('name', 'id');

        return view('admin.pfumvudza.create', compact('facilitators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $beneficiary = Beneficiary::find($request->input('beneficiary_id')) ?? Beneficiary::updateOrCreate([
                'beneficiary_id' => $request->input('beneficiary_id')
            ]);

        $pfumvu = new Pfumvudza();
        $pfumvu = $request->all();
        $pfumvu['district'] = $beneficiary->village->ward->district;
        $pfumvudza = Pfumvudza::create($pfumvu);

        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $pfumvudza->facilitators()->save($facilitator, $position);
        }


        return redirect()->route('admin.pfumvudza.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Pfumvudza $pfumvudza
     * @return \Illuminate\Http\Response
     */
    public function show(Pfumvudza $pfumvudza)
    {
        abort_if(Gate::denies('pfumvudza_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pfumvudza->load('beneficiaries', 'facilitators');

        //dd($pfumvudza);

        return view('admin.pfumvudza.show', compact('pfumvudza'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Pfumvudza $pfumvudza
     * @return \Illuminate\Http\Response
     */
    public function edit(Pfumvudza $pfumvudza)
    {
        abort_if(Gate::denies('pfumvudza_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $facilitators = Facilitator::all()->pluck('name', 'id');

        $pfumvudza->load('beneficiaries');

        return view('admin.pfumvudza.edit', compact('pfumvudza', 'facilitators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pfumvudza $pfumvudza
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pfumvudza $pfumvudza)
    {
        $beneficiary = Beneficiary::find($request->input('beneficiary_id')) ?? Beneficiary::updateOrCreate([
                'beneficiary_id' => $request->input('beneficiary_id')
            ]);

        $pfumvudza->update($request->all());

        $pfumvudza->facilitators()->detach();
        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $pfumvudza->facilitators()->save($facilitator, $position);
        }


        return redirect()->route('admin.pfumvudza.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Pfumvudza $pfumvudza
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pfumvudza $pfumvudza)
    {
        abort_if(Gate::denies('pfumvudza_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pfumvudza->delete();

        return back();
    }

    public function massDestroy(Request $request)
    {
        Pfumvudza::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new PfumvudzaImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }

    public function export(Excel $excel)
    {
        return Excel::download(new PfumvudzaExport, 'Pfumvudza.xlsx');
        // return $excel->download(new TeacherExport, 'facilitators.xlsx');
        // return(new BeneficiariesExport)->download('beneficiaries.xlsx');
    }
}
