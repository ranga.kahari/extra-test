<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyThematicAreaRequest;
use App\Http\Requests\StoreThematicAreaRequest;
use App\Http\Requests\UpdateThematicAreaRequest;
use App\Models\ThematicArea;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ThematicAreasController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('thematic_area_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = ThematicArea::query()->select(sprintf('%s.*', (new ThematicArea)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'thematic_area_show';
                $editGate      = 'thematic_area_edit';
                $deleteGate    = 'thematic_area_delete';
                $crudRoutePart = 'thematic-areas';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('show_in_side_menu', function ($row) {
                return '<span class="badge badge-' . ($row->show_in_side_menu ? 'success' : 'danger') . '">' . ($row->show_in_side_menu ? 'Yes' : 'No') . '</span>';
            });
            $table->editColumn('order', function ($row) {
                return $row->order ? $row->order : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'show_in_side_menu']);

            return $table->make(true);
        }

        return view('admin.thematicAreas.index');
    }

    public function create()
    {
        abort_if(Gate::denies('thematic_area_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.thematicAreas.create');
    }

    public function store(StoreThematicAreaRequest $request)
    {
        $thematicArea = ThematicArea::create($request->all());

        return redirect()->route('admin.thematic-areas.index');
    }

    public function edit(ThematicArea $thematicArea)
    {
        abort_if(Gate::denies('thematic_area_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.thematicAreas.edit', compact('thematicArea'));
    }

    public function update(UpdateThematicAreaRequest $request, ThematicArea $thematicArea)
    {
        $thematicArea->update($request->all());

        return redirect()->route('admin.thematic-areas.index');
    }

    public function show(ThematicArea $thematicArea)
    {
        abort_if(Gate::denies('thematic_area_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.thematicAreas.show', compact('thematicArea'));
    }

    public function destroy(ThematicArea $thematicArea)
    {
        abort_if(Gate::denies('thematic_area_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $thematicArea->delete();

        return back();
    }

    public function massDestroy(MassDestroyThematicAreaRequest $request)
    {
        ThematicArea::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
