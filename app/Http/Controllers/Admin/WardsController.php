<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ward;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class WardsController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('ward_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Ward::query()->select(sprintf('%s.*', (new Ward)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'ward_show';
                $editGate      = 'ward_edit';
                $deleteGate    = 'ward_delete';
                $crudRoutePart = 'wards';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('district', function ($row) {
                return $row->district ? Ward::DISTRICT_SELECT[$row->district] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.wards.index');
    }

    public function show(Ward $ward)
    {
        abort_if(Gate::denies('ward_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ward->load('villages');

        return view('admin.wards.show', compact('ward'));
    }

    public function search(Request $request)
    {
        abort_if(Gate::denies('ward_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $results = [];

        if ($request->has('search')) {
            $searchTerm = $request->search;
            $results = Ward::search(['ward_number', 'district'], $searchTerm)
                ->simplePaginate($request->perPage ?? 10);
        }

        return response()->json($results);
    }
}
