<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyGroupRequest;
use App\Http\Requests\StoreGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Models\Facilitator;
use App\Models\Group;
use App\Models\Member;
use App\Models\ValueChain;
use Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class GroupsController extends Controller
{
    public function index(Request $request)
    {
        //dd($request);

        abort_if(Gate::denies('group_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Group::select(sprintf('%s.*', (new Group)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'group_show';
                $editGate      = 'group_edit';
                $deleteGate    = 'group_delete';
                $crudRoutePart = 'groups';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('group_categories', function ($row) {
                return $row->group_categories ? Group::GROUP_CATEGORIES_SELECT[$row->group_categories] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

             return $table->make(true);
         }

        return view('admin.groups.index');
    }

    public function search(Request $request)
    {
        abort_unless(\Gate::allows('group_access'), 403);

        $results = [];

        if ($request->has('search')) {
            $searchTerm = $request->search;
            $results = Group::search(['name', 'group_number'], $searchTerm)
                ->simplePaginate($request->perPage ?? 10);
        }

        return response()->json($results);
    }

    public function create()
    {
        abort_if(Gate::denies('group_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $facilitators = Facilitator::all()->pluck('name', 'id');
        $value_chains = ValueChain::all()->pluck('name', 'id');

        return view('admin.groups.create', compact('facilitators', 'value_chains'));
    }

    // public function createForGroup(Request $request){

    //     $group = group::find($request->group_id);


    //     return view('admin.groups.create_for_group', compact('group'));
    // }

    public function store(StoreGroupRequest $request)
    {
        $group = Group::create($request->all());

        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $group->facilitators()->save($facilitator, $position);
        }

        return redirect()->route('admin.groups.index');
    }

      public function massStore(Request $request)
     {
    //     // abort_unless(\Gate::allows('attendance_create'), 403);

    //     //dd($request);
    //     $group = Group::find($request->input('group_id'));

    //     if ($group) {


    //         foreach ($request->input('attendances') ?? [] as $value) {

    //             $member = Member::updateOrCreate([
    //                 'beneficiary_id' => $value['beneficiary_id'],
    //                 'group_id' => $group->id
    //             ], []);

    //         }

    //         $member->save();

    //         return redirect()->route('admin.groups.show', $group->id);
    //     } else {
    //         return redirect()->route('admin.groups.index')->with('error', 'Members register not updated! The group you tried updating no longer exists');
    //     }
     }

    public function edit(Group $group)
    {
        abort_if(Gate::denies('group_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $facilitators = Facilitator::all()->pluck('name', 'id');

        $group->load('facilitators');

        return view('admin.groups.edit', compact('facilitators', 'group'));
    }

    public function update(UpdateGroupRequest $request, Group $group)
    {
        //ddd($group);
        $group->update($request->all());

        $group->facilitators()->detach();
        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $group->facilitators()->save($facilitator, $position);
        }

        return redirect()->route('admin.groups.index');
    }

    public function show(Group $group)
    {
        //ddd($group);

        abort_if(Gate::denies('group_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $group->load('facilitators', 'members');

       //dd($group);

        return view('admin.groups.show', compact('group'));

    }

    public function destroy(Group $group)
    {
        abort_if(Gate::denies('group_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $group->delete();

        return back();
    }

    public function massDestroy(MassDestroyGroupRequest $request)
    {
        Group::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function massDestroyMember(Request $request)
    {
        Member::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
