<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyAgeBandRequest;
use App\Http\Requests\StoreAgeBandRequest;
use App\Http\Requests\UpdateAgeBandRequest;
use App\Models\AgeBand;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AgeBandsController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('age_band_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = AgeBand::query()->select(sprintf('%s.*', (new AgeBand)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'age_band_show';
                $editGate      = 'age_band_edit';
                $deleteGate    = 'age_band_delete';
                $crudRoutePart = 'age-bands';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.ageBands.index');
    }

    public function create()
    {
        abort_if(Gate::denies('age_band_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.ageBands.create');
    }

    public function store(StoreAgeBandRequest $request)
    {
        $ageBand = AgeBand::create($request->all());

        return redirect()->route('admin.age-bands.index');
    }

    public function edit(AgeBand $ageBand)
    {
        abort_if(Gate::denies('age_band_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.ageBands.edit', compact('ageBand'));
    }

    public function update(UpdateAgeBandRequest $request, AgeBand $ageBand)
    {
        $ageBand->update($request->all());

        return redirect()->route('admin.age-bands.index');
    }

    public function show(AgeBand $ageBand)
    {
        abort_if(Gate::denies('age_band_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.ageBands.show', compact('ageBand'));
    }

    public function destroy(AgeBand $ageBand)
    {
        abort_if(Gate::denies('age_band_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ageBand->delete();

        return back();
    }

    public function massDestroy(MassDestroyAgeBandRequest $request)
    {
        AgeBand::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
