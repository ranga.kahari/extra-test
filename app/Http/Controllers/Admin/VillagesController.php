<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyVillageRequest;
use App\Http\Requests\StoreVillageRequest;
use App\Http\Requests\UpdateVillageRequest;
use App\Models\Village;
use App\Models\Ward;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class VillagesController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('village_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Village::with(['ward'])->select(sprintf('%s.*', (new Village)->table));
            $query->when($request->ward_id, function ($query) use ($request) {
                $query->where('ward_id', $request->ward_id);
            });
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'village_show';
                $editGate      = 'village_edit';
                $deleteGate    = 'village_delete';
                $crudRoutePart = 'villages';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->addColumn('district', function ($row) {
                return $row->ward ? $row->ward->district : '';
            });
            $table->addColumn('ward_number', function ($row) {
                return $row->ward ? $row->ward->ward_number : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.villages.index');
    }

    public function search(Request $request)
    {
        abort_unless(\Gate::allows('village_access'), 403);

        $results = [];

        if ($request->has('search')) {
            $searchTerm = $request->search;
            $results = Village::search('name', $searchTerm)
                ->simplePaginate($request->perPage ?? 10);
        }

        return response()->json($results);
    }

    public function create()
    {
        abort_if(Gate::denies('village_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $wards = Ward::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.villages.create', compact('wards'));
    }

    public function store(StoreVillageRequest $request)
    {
        $village = Village::create($request->all());

        return redirect()->route('admin.villages.index');
    }

    public function edit(Village $village)
    {
        abort_if(Gate::denies('village_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $wards = Ward::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $village->load('ward');

        return view('admin.villages.edit', compact('wards', 'village'));
    }

    public function update(UpdateVillageRequest $request, Village $village)
    {
        $village->update($request->all());

        return redirect()->route('admin.villages.index');
    }

    public function show(Village $village)
    {
        abort_if(Gate::denies('village_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $village->load('ward');

        return view('admin.villages.show', compact('village'));
    }

    public function destroy(Village $village)
    {
        abort_if(Gate::denies('village_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $village->delete();

        return back();
    }

    public function massDestroy(MassDestroyVillageRequest $request)
    {
        Village::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
