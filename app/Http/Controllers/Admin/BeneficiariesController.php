<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\Models\Group;
use App\Models\Village;
use App\Models\ValueChain;
use App\Models\Beneficiary;
use App\Traits\AgeFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Exports\BeneficiariesExport;
use App\Http\Controllers\Controller;
// use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StoreBeneficiaryRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\UpdateBeneficiaryRequest;
use App\Http\Requests\MassDestroyBeneficiaryRequest;

class BeneficiariesController extends Controller
{
    use AgeFunctions;
    
    public function index(Request $request)
    {
        abort_if(Gate::denies('beneficiary_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genderCounts = DB::table('beneficiaries')->select('gender', DB::raw('count(*) as count'))->groupBy('gender')->get();
        $total = $genderCounts->sum('count');
        $genderCounts = $genderCounts->push((object)[
            'gender' => 'total',
            'count' => $total
        ])->map(function($item) use($total) {
            return (object)[
                'name' => $item->gender,
                'count' => $item->count,
                'percent' => $total > 0 ? round(($item->count / $total) * 100) : 0.0

            ];
        });

        if ($request->ajax()) {
            $query = Beneficiary::select(sprintf('%s.*', (new Beneficiary)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'beneficiary_show';
                $editGate      = 'beneficiary_edit';
                $deleteGate    = 'beneficiary_delete';
                $crudRoutePart = 'beneficiaries';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('gender', function ($row) {
                return $row->gender ? Beneficiary::GENDER_SELECT[$row->gender] : '';
            });
            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.beneficiaries.index', compact('genderCounts'));
    }

    public function search(Request $request)
    {
        abort_unless(\Gate::allows('beneficiary_access'), 403);

        $results = [];

        if ($request->has('search')) {
            $searchTerm = $request->search;
            //name is equivalent to "first_name surname (national_id_number)
            $results = Beneficiary::search(['name', 'first_name', 'surname'], $searchTerm)
                ->simplePaginate($request->perPage ?? 10);
        }

        return response()->json($results);
    }

    public function create()
    {
        abort_if(Gate::denies('beneficiary_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $value_chains = ValueChain::all()->pluck('name', 'id');

        return view('admin.beneficiaries.create', compact('value_chains'));
    }

    public function store(StoreBeneficiaryRequest $request)
    {
        //dd($request);
        $village = Village::find($request->input('village_id')) ?? Village::updateOrCreate([
            'name' => $request->input('village_id'), 
            'ward_id' => $request->input('ward_id')
        ]);
        
        $beneficiary = Beneficiary::updateOrCreate([
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'gender' => $request->gender,
            'national_id_number' => $request->national_id_number,
            'year_of_birth' => $request->year_of_birth,
        ],[
            'name' => $request->first_name . ' ' . $request->surname . ' (' . $request->national_id_number . ')',
            'phone_number' => $request->phone_number,
            'is_household_head' => $request->is_household_head,
            'name_of_household_head' => $request->is_household_head ? ($request->first_name . ' ' . $request->surname) : $request->name_of_household_head,
            'id_of_household_head' => $request->is_household_head ? $request->national_id_number : $request->id_of_household_head,
            'size_of_household' => $request->size_of_household,
            'females_in_household' => $request->females_in_household,
            'children_under_five_in_household' => $request->children_under_five_in_household,
            'production_area' => $request->production_area,
            'village_id' => $village->id,
            'farmer_category' => $request->farmer_category,
            'farmer_group_membership' => $request->farmer_group_membership,
        ]);

        $this->setAgeBand($beneficiary);
        
        foreach ($request->input('groups', []) as $groupId => $position) {
            $group = Group::find($groupId) ?? Group::firstOrCreate(['name' => $groupId]);
            $beneficiary->groups()->save($group, $position);
        }
        $beneficiary->value_chains()->sync($request->input('value_chains', []));
        
        if ($request->ajax()) {
            return $beneficiary->toJson();
        }

        return redirect()->route('admin.beneficiaries.index');
    }

    public function edit(Beneficiary $beneficiary)
    {
        abort_if(Gate::denies('beneficiary_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $value_chains = ValueChain::all()->pluck('name', 'id');

        $beneficiary->load('village', 'groups', 'value_chains');

        return view('admin.beneficiaries.edit', compact('value_chains', 'beneficiary'));
    }

    public function update(UpdateBeneficiaryRequest $request, Beneficiary $beneficiary)
    {
        $village = Village::find($request->input('village_id')) ?? Village::updateOrCreate(['name' => $request->input('village_id'), 'ward_id' => $request->input('ward_id')]);

        $beneficiary->update([
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'gender' => $request->gender,
            'national_id_number' => $request->national_id_number,
            'year_of_birth' => $request->year_of_birth,
            'name' => $request->first_name . ' ' . $request->surname . ' (' . $request->national_id_number . ')',
            'phone_number' => $request->phone_number,
            'is_household_head' => $request->is_household_head,
            'name_of_household_head' => $request->is_household_head ? ($request->first_name . ' ' . $request->surname) : $request->name_of_household_head,
            'id_of_household_head' => $request->is_household_head ? $request->national_id_number : $request->id_of_household_head,
            'size_of_household' => $request->size_of_household,
            'females_in_household' => $request->females_in_household,
            'children_under_five_in_household' => $request->children_under_five_in_household,
            'production_area' => $request->production_area,
            'village_id' => $village->id,
            'farmer_category' => $request->farmer_category,
            'farmer_group_membership' => $request->farmer_group_membership,
        ]);

        $this->setAgeBand($beneficiary);

        $beneficiary->groups()->detach();
        foreach ($request->input('groups', []) as $groupId => $position) {
            $group = Group::find($groupId) ?? Group::firstOrCreate(['name' => $groupId]);
            $beneficiary->groups()->save($group, $position);
        }
        $beneficiary->value_chains()->sync($request->input('value_chains', []));

        return redirect()->route('admin.beneficiaries.index');
    }

    public function show(Beneficiary $beneficiary)
    {
        abort_if(Gate::denies('beneficiary_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $beneficiary->load('village', 'groups', 'value_chains', 'beneficiaryAttendances');

        return view('admin.beneficiaries.show', compact('beneficiary'));
    }

    public function destroy(Beneficiary $beneficiary)
    {
        abort_if(Gate::denies('beneficiary_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $beneficiary->delete();

        return back();
    }

    public function massDestroy(MassDestroyBeneficiaryRequest $request)
    {
        Beneficiary::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
    public function export(Excel $excel)
    {
        return $excel->download(new BeneficiariesExport, 'bene.xlsx');
        // return(new BeneficiariesExport)->download('beneficiaries.xlsx');
    }
}
