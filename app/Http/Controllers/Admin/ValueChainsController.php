<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyValueChainRequest;
use App\Http\Requests\StoreValueChainRequest;
use App\Http\Requests\UpdateValueChainRequest;
use App\Models\ValueChain;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ValueChainsController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('value_chain_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = ValueChain::query()->select(sprintf('%s.*', (new ValueChain)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'value_chain_show';
                $editGate      = 'value_chain_edit';
                $deleteGate    = 'value_chain_delete';
                $crudRoutePart = 'value-chains';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('categories', function ($row) {
                return $row->categories ? ValueChain::CATEGORIES_SELECT[$row->categories] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.valueChains.index');
    }

    public function create()
    {
        abort_if(Gate::denies('value_chain_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.valueChains.create');
    }

    public function store(StoreValueChainRequest $request)
    {
        $valueChain = ValueChain::create($request->all());

        return redirect()->route('admin.value-chains.index');
    }

    public function edit(ValueChain $valueChain)
    {
        abort_if(Gate::denies('value_chain_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.valueChains.edit', compact('valueChain'));
    }

    public function update(UpdateValueChainRequest $request, ValueChain $valueChain)
    {
        $valueChain->update($request->all());

        return redirect()->route('admin.value-chains.index');
    }

    public function show(ValueChain $valueChain)
    {
        abort_if(Gate::denies('value_chain_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.valueChains.show', compact('valueChain'));
    }

    public function destroy(ValueChain $valueChain)
    {
        abort_if(Gate::denies('value_chain_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $valueChain->delete();

        return back();
    }

    public function massDestroy(MassDestroyValueChainRequest $request)
    {
        ValueChain::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
