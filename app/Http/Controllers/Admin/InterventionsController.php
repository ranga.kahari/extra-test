<?php

namespace App\Http\Controllers\Admin;

use App\Models\Village;
use App\Models\Facilitator;
use App\Models\Intervention;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreInterventionRequest;
use App\Imports\InterventionsImport;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Symfony\Component\HttpFoundation\Response;

class InterventionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('intervention_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        if ($request->ajax()) {
            $query = Intervention::query()->select(sprintf('%s.*', (new Intervention)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'intervention_show';
                $editGate      = 'intervention_edit';
                $deleteGate    = 'intervention_delete';
                $crudRoutePart = 'interventions';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('custodian', function ($row) {
                return $row->custodian ? $row->custodian : "";
            });
            $table->editColumn('intervention', function ($row) {
                return $row->intervention ? Intervention::INTERVENTION_SELECT[$row->intervention] : '';
            });
            $table->editColumn('latitude', function ($row) {
                return $row->latitude ? $row->latitude : "";
            });
            $table->editColumn('longitude', function ($row) {
                return $row->longitude ? $row->longitude : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }
        
        return view('admin.interventions.index');
    }
    
    public function map(Request $request)
    {
        abort_if(Gate::denies('intervention_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        $data = Intervention::with('village')->get();
        
        return view('admin.interventions.show-map', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('intervention_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        return view('admin.interventions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInterventionRequest $request)
    {
         $village = Village::find($request->input('village_id')) ?? Village::updateOrCreate([
            'name' => $request->input('village_id'), 
            'ward_id' => $request->input('ward_id')
        ]);
        
        $intervention = Intervention::create([
            'custodian' => $request->input('custodian'),
            'intervention' => $request->input('intervention'),
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude'),
            'village_id' => $village->id,
        ]);

        return redirect()->route('admin.interventions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function show(Intervention $intervention)
    {
        abort_if(Gate::denies('intervention_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        $intervention->load('village');

        return view('admin.interventions.show', compact('intervention'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function edit(Intervention $intervention)
    {
        abort_if(Gate::denies('intervention_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.interventions.edit', compact('intervention'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Intervention $intervention)
    {
        $village = Village::find($request->input('village_id')) ?? Village::updateOrCreate([
            'name' => $request->input('village_id'), 
            'ward_id' => $request->input('ward_id')
        ]);
        
            $intervention ->update([
                'custodian' => $request->input('custodian'),
                'intervention' => $request->input('intervention'),
                'latitude' => $request->input('latitude'),
                'longitude' => $request->input('longitude'),
                'village_id' => $village->id,
        ]);

        return redirect()->route('admin.interventions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function destroy(Intervention $intervention)
    {
        abort_if(Gate::denies('intervention_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $intervention->delete();

        return back();
    }
    
    public function massDestroy(Request $request)
    {
        Intervention::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new InterventionsImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
    
}
