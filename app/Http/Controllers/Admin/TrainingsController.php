<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTrainingRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\StoreTrainingRequest;
use App\Http\Requests\UpdateTrainingRequest;
use App\Imports\TrainingsImport;
use App\Models\Facilitator;
use App\Models\ThematicArea;
use App\Models\Topic;
use App\Models\Training;
use App\Models\ValueChain;
use App\Models\Village;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class TrainingsController extends Controller
{
    
    public function index(Request $request)
    {
        abort_if(Gate::denies('training_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Training::withCount('attendances')->select(sprintf('%s.*', (new Training)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'training_show';
                $editGate      = 'training_edit';
                $deleteGate    = 'training_delete';
                $crudRoutePart = 'trainings';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.trainings.index');
    }

    public function create()
    {
        abort_if(Gate::denies('training_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $thematic_areas = ThematicArea::with(['trainingCategories:id,name,thematic_area_id', 'trainingCategories.topics:id,name,training_category_id'])->select('name', 'id')->get();

        $value_chains = ValueChain::all()->pluck('name', 'id');

        return view('admin.trainings.create', compact('thematic_areas', 'value_chains'));
    }

    public function store(StoreTrainingRequest $request)
    {
       // dd($request);
        
        $village = Village::find($request->input('village_id')) ?? Village::updateOrCreate([
            'name' => $request->input('village_id'),
            'ward_id' => $request->input('ward_id')
        ]);
        
        $inputArr = $request->all();
        $inputArr['village_id'] = $village->id;
        $inputArr['reviewed_by'] = Auth::user()->name;
        $inputArr['captured_by'] = Auth::user()->name;
        $training = Training::create($inputArr);
        $training->thematicAreas()->sync($request->input('thematic_areas', []));
        $training->valueChains()->sync($request->input('value_chains', []));
        $training->topics()->sync($request->input('topics', []));

        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $training->facilitators()->save($facilitator, $position);
        }
        
         return redirect("/admin/trainings/".$training->id);
        // return redirect()->route('admin.trainings.index');
       
    }

    public function edit(Training $training)
    {
        abort_if(Gate::denies('training_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $thematic_areas = ThematicArea::with(['trainingCategories:id,name,thematic_area_id', 'trainingCategories.topics:id,name,training_category_id'])->select('name', 'id')->get();

        $value_chains = ValueChain::all()->pluck('name', 'id');

        $training->load('village', 'thematicAreas', 'valueChains', 'topics', 'facilitators');

        return view('admin.trainings.edit', compact('thematic_areas', 'value_chains', 'training'));
    }

    public function update(UpdateTrainingRequest $request, Training $training)
    {
        $village = Village::find($request->input('village_id')) ?? Village::updateOrCreate([
            'name' => $request->input('village_id'),
            'ward_id' => $request->input('ward_id')
        ]);

        $inputArr = $request->all();
        $inputArr['village_id'] = $village->id;
        $inputArr['reviewed_by'] = Auth::user()->name;
        $training->update($inputArr);
        
        $training->thematicAreas()->sync($request->input('thematic_areas', []));
        $training->valueChains()->sync($request->input('value_chains', []));
        $training->topics()->sync($request->input('topics', []));

        $training->facilitators()->detach();
        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $training->facilitators()->save($facilitator, $position);
        }

        return redirect()->route('admin.trainings.index');
    }

    public function show(Training $training)
    {
        abort_if(Gate::denies('training_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $training->load('village', 'thematicAreas', 'valueChains', 'topics', 'facilitators', 'attendances');

        $genderCounts = DB::table('beneficiaries')
            ->join('attendances', 'attendances.beneficiary_id', '=', 'beneficiaries.id')
            ->join('trainings', 'trainings.id', '=', 'attendances.training_id')
            ->where('trainings.id', $training->id)
            ->select('gender', DB::raw('count(*) as count'))->groupBy('gender')->get();
        $total = $genderCounts->sum('count');
        $genderCounts = $genderCounts->push((object)[
            'gender' => 'total',
            'count' => $total
        ])->map(function ($item) use ($total) {
            return (object)[
                'name' => $item->gender,
                'count' => $item->count,
                'percent' => $total > 0 ? round(($item->count / $total) * 100) : 0.0

            ];
        });

        return view('admin.trainings.show', compact('training', 'genderCounts'));
    }

    public function destroy(Training $training)
    {
        abort_if(Gate::denies('training_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $training->delete();

        return back();
    }

    public function massDestroy(MassDestroyTrainingRequest $request)
    {
        Training::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new TrainingsImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
    
    
}
