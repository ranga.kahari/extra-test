<?php

// namespace App\Http\Controllers;
namespace App\Http\Controllers\Admin;



use Carbon\Carbon;
use App\Models\User;
use App\Models\Ward;
use App\Models\Training;
use App\Models\Attendance;
use App\Models\ValueChain;
use App\Models\Beneficiary;
use App\Models\Facilitator;
use App\Models\ThematicArea;
use App\Models\TrainingCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController 
{
 
    public function index(Request $request)
    {
        // $thematicAreas = ThematicArea::with(['trainingCategories:id,name,thematic_area_id', 'trainingCategories.topics:id,name,training_category_id'])->select('name', 'id')->get();
        $thematicAreas = ThematicArea::all();
        $valueChains = ValueChain::all()->pluck('name', 'id');
        $wards = Ward::all();
        
        if ($request->ajax()) {
            $dashboardData = $this->getDashboardData($request);
            return json_encode($dashboardData);
        }
        
        return view('home', compact('thematicAreas', 'valueChains', 'wards'));
        
       
    }
    
     private function getDashboardData($request)
    {
        return [
            'farmers_by_gender' => $this->getFarmersByGender($request),
            'farmers_by_category' => $this->getFarmersByCategory($request),
            'farmers_by_age_band' => $this->getFarmersByAgeBand($request)
        ];
    }
    
    public function getBaseQuery($request)
    {
        $query = Beneficiary::query()
            ->leftJoin('attendances', 'attendances.beneficiary_id', '=', 'beneficiaries.id')
            ->leftJoin('trainings', 'attendances.training_id', '=', 'trainings.id')
            ->leftJoin('thematic_areas', 'thematic_areas.id', '=', 'trainings.thematic_area_id')
            ->leftJoin('training_training_code', 'training_training_code.training_id', '=', 'trainings.id')
            ->leftJoin('training_codes', 'training_training_code.training_code_id', '=', 'training_codes.id')
            ->leftJoin('training_value_chain', 'training_value_chain.training_id', '=', 'trainings.id')
            ->leftJoin('value_chains', 'value_chains.id', '=', 'training_value_chain.value_chain_id')
            ->leftJoin('villages', 'villages.id', '=', 'beneficiaries.village_id')
            ->leftJoin('wards', 'wards.id', '=', 'villages.ward_id');
            
        $query = $this->applyFilters($request, $query);
        
        return $query;
    }
    
    public function applyFilters($request, $query)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $thematicAreas = $request->thematic_areas;
        $trainingCodes = $request->training_codes;
        $valueChains = $request->value_chains;
        $districts = $request->districts;
        $wards = $request->wards;
        $farmerCategory = $request->farmer_category;
        $gender = $request->gender;

        $query->when(($start_date && $end_date), function ($query) use ($start_date, $end_date) {
            return $query->whereBetween('trainings.date', [new Carbon($start_date), (new Carbon($end_date))->endOfDay()]);
        });
        
        $query->when($thematicAreas, function($query) use($thematicAreas) {
            return $query->whereIn('thematic_areas.id', $thematicAreas);
        });
        
        $query->when($trainingCodes, function($query) use($trainingCodes) {
            return $query->whereIn('training_codes.id', $trainingCodes);
        });
        
        $query->when($valueChains, function($query) use($valueChains) {
            return $query->whereIn('value_chains.id', $valueChains);
        });
        
        $query->when($districts, function($query) use($districts) {
            return $query->whereIn('wards.district', $districts);
        });
        
        $query->when($wards, function($query) use($wards) {
            return $query->whereIn('wards.id', $wards);
        });
        
        $query->when($farmerCategory, function($query) use($farmerCategory) {
            return $query->where('beneficiaries.farmer_category', $farmerCategory);
        });
        
        $query->when($gender, function($query) use($gender) {
            return $query->where('beneficiaries.gender', $gender);
        });
        
        return $query;
    }
    
    public function getFarmersByGender($request)
    {
        return $this->getBaseQuery($request)
            ->groupBy('beneficiaries.gender')
            ->select(
                DB::raw('beneficiaries.gender as name'),
                DB::raw('count(*) as value'),
            )
            ->distinct('beneficiaries.id')
            ->get();
    }
    
    public function getFarmersByCategory($request)
    {
        return $this->getBaseQuery($request)
            ->groupBy('beneficiaries.farmer_category')
            ->select(
                DB::raw('beneficiaries.farmer_category as name'),
                DB::raw('count(*) as value')
            )
            ->distinct('beneficiaries.id')
            ->get();
    }
    
    public function getFarmersByAgeBand($request)
    {
        return $this->getBaseQuery($request)
            ->leftJoin('age_bands', 'age_bands.id', '=', 'beneficiaries.age_band_id')
            ->groupBy('age_bands.id')
            ->select(
                DB::raw('age_bands.name as name'),
                DB::raw('count(*) as value')
            )
            ->distinct('beneficiaries.id')
            ->get();
    }
}
