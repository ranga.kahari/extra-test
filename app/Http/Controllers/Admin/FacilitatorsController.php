<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFacilitatorRequest;
use App\Http\Requests\StoreFacilitatorRequest;
use App\Http\Requests\UpdateFacilitatorRequest;
use App\Imports\FacilitatorImport;
use App\Models\Facilitator;
use Maatwebsite\Excel\Facades\Excel;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class FacilitatorsController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('facilitator_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Facilitator::query()->select(sprintf('%s.*', (new Facilitator)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'facilitator_show';
                $editGate      = 'facilitator_edit';
                $deleteGate    = 'facilitator_delete';
                $crudRoutePart = 'facilitators';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('type', function ($row) {
                return $row->type ? Facilitator::TYPE_SELECT[$row->type] : '';
            });
            $table->editColumn('contact', function ($row) {
                return $row->contact ? $row->contact : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }
            
        return view('admin.facilitators.index');
    }

    public function search(Request $request)
    {
        abort_unless(\Gate::allows('facilitator_access'), 403);

        $results = [];

        if ($request->has('search')) {
            $searchTerm = $request->search;
            $results = Facilitator::search(['name', 'type', 'contact'], $searchTerm)
                ->simplePaginate($request->perPage ?? 10);
        }

        return response()->json($results);
    }

    public function create()
    {
        abort_if(Gate::denies('facilitator_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.facilitators.create');
    }

    public function store(StoreFacilitatorRequest $request)
    {
        $facilitator = Facilitator::create($request->all());

        return redirect()->route('admin.facilitators.index');
    }

    public function edit(Facilitator $facilitator)
    {
        abort_if(Gate::denies('facilitator_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.facilitators.edit', compact('facilitator'));
    }

    public function update(UpdateFacilitatorRequest $request, Facilitator $facilitator)
    {
        $facilitator->update($request->all());

        return redirect()->route('admin.facilitators.index');
    }

    public function show(Facilitator $facilitator)
    {
        abort_if(Gate::denies('facilitator_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.facilitators.show', compact('facilitator'));
    }

    public function destroy(Facilitator $facilitator)
    {
        abort_if(Gate::denies('facilitator_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $facilitator->delete();

        return back();
    }

    public function massDestroy(MassDestroyFacilitatorRequest $request)
    {
        Facilitator::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
    
    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');
        
        Excel::queueImport(new FacilitatorImport, $file);
        
        return back()->with('message', 'Imported Successfully!');
    }
}
