<?php

namespace App\Http\Controllers\Admin;

use App\Exports\DistributionExport;
use Illuminate\Support\Str;
use App\Models\Village;
use App\Models\Beneficiary;
use App\Models\Facilitator;
use App\Models\Distribution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\DistributionImport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Expr\AssignOp\Concat;
use Yajra\DataTables\Facades\DataTables;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\StoreDistributionRequest;
use App\Http\Requests\UpdateDistributionRequest;
use App\Http\Requests\MassDestroyDistributionRequest;

class DistributionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('distribution_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        if ($request->ajax()) {
            $query = Distribution::query()->select(sprintf('%s.*', (new Distribution)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'distribution_show';
                $editGate      = 'distribution_edit';
                $deleteGate    = 'distribution_delete';
                $crudRoutePart = 'distributions';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('date', function ($row) {
                return $row->date ? $row->date : "";
            });
            // $table->editColumn('item_distributed_type', function ($row) {
            //     return $row->item_distributed_type ? Distribution::ITEM_DISTRIBUTED_TYPE[$row->item_distributed_type] : '';
            // });
            $table->editColumn('item_distributed', function ($row) {
                return $row->item_distributed ? Distribution::ITEM_DISTRIBUTED[$row->item_distributed] : '';
            });
            $table->editColumn('variety', function ($row) {
                return $row->variety ? $row->variety : "";
            });
            $table->editColumn('quantity', function ($row) {
                return $row->quantity ? $row->quantity : "";
            });
            $table->editColumn('project', function ($row) {
                return $row->project ? Distribution::PROJECT_OWNER[$row->project] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }
        
        return view('admin.distributions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('distribution_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        return view('admin.distributions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDistributionRequest $request)
    {
        //dd($request);
        
        $beneficiary = Beneficiary::find($request->input('beneficiary_id')) ?? Beneficiary::updateOrCreate([
            'beneficiary_id' => $request->input('beneficiary_id')
        ]);
        
        $items_distributed= "";
        
        foreach($request->input('item_distributed', []) as $item){
             $items_distributed.= $item.",";
        }
        
        // $items_distributed = Str::of($items_distributed)
        //     ->explode(' ')
        //     ->trim()
        //     ->__toString();
            
        //     dd($items_distributed);
        
        
        
        $distribution = Distribution::create([
            'project' => $request->input('project'),
            'item_distributed_type' => $request->input('item_distributed_type'),
            'date' => $request->input('date'),
            'item_distributed' => $items_distributed,
            'variety' => $request->input('variety'),
            'quantity' => $request->input('quantity'),
            'beneficiary_id' => $beneficiary->id,
        ]);
        
        return redirect()->route('admin.distributions.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Distribution  $distribution
     * @return \Illuminate\Http\Response
     */
    public function show(Distribution $distribution)
    {
        abort_if(Gate::denies('distribution_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        $distribution->load('beneficiaries');
        
        //dd($distribution);
        
        return view('admin.distributions.show', compact('distribution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Distribution  $distribution
     * @return \Illuminate\Http\Response
     */
    public function edit(Distribution $distribution)
    {
        abort_if(Gate::denies('distribution_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        $distribution->load('beneficiaries');
        
        return view('admin.distributions.edit', compact('distribution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Distribution  $distribution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Distribution $distribution)
    {
        $beneficiary = Beneficiary::find($request->input('beneficiary_id')) ?? Beneficiary::updateOrCreate([
            'beneficiary_id' => $request->input('beneficiary_id')
        ]);
        
        $items_distributed="";
        foreach($request->input('item_distributed', []) as $item){
        
             $items_distributed.= $item.",";
        }
        
        
         $distribution->update([
            'project' => $request->input('project'),
            'item_distributed_type' => $request->input('item_distributed_type'),
            'date' => $request->input('date'),
            'item_distributed' => $items_distributed,
            'variety' => $request->input('variety'),
            'quantity' => $request->input('quantity'),
            'beneficiary_id' => $beneficiary->id,
        ]);
        
        return redirect()->route('admin.distributions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Distribution  $distribution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Distribution $distribution)
    {
        abort_if(Gate::denies('distribution_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $distribution->delete();

        return back();
    }
    
    public function massDestroy(MassDestroyDistributionRequest $request)
    {
        Distribution::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
     public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new DistributionImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
    
    public function export(Excel $excel)
    {
        return Excel::download(new DistributionExport, 'Item-Distributed.xlsx');
        // return $excel->download(new TeacherExport, 'facilitators.xlsx');
        // return(new BeneficiariesExport)->download('beneficiaries.xlsx');
    }
}
