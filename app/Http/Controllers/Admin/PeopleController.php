<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Person;
use Illuminate\Http\Request;
use Gate;
use App\Models\Group;
use App\Models\Village;
use App\Models\ValueChain;
use App\Models\Beneficiary;
use App\Traits\AgeFunctions;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Exports\BeneficiariesExport;
use App\Exports\PeopleExport;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StoreBeneficiaryRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\UpdateBeneficiaryRequest;
use App\Http\Requests\MassDestroyBeneficiaryRequest;
use App\Imports\PeopleImport;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('people_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genderCounts = DB::table('people')->select('gender', DB::raw('count(*) as count'))->groupBy('gender')->get();
        $total = $genderCounts->sum('count');
        $genderCounts = $genderCounts->push((object)[
            'gender' => 'total',
            'count' => $total
        ])->map(function($item) use($total) {
            return (object)[
                'name' => $item->gender,
                'count' => $item->count,
                'percent' => $total > 0 ? round(($item->count / $total) * 100) : 0.0

            ];
        });

        if ($request->ajax()) {
            $query = Person::select(sprintf('%s.*', (new Person())->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'people_show';
                $editGate      = 'people_edit';
                $deleteGate    = 'people_delete';
                $crudRoutePart = 'people';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('gender', function ($row) {
                return $row->gender ? Person::GENDER_SELECT[$row->gender] : '';
            });
            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.people.index', compact('genderCounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        return view('admin.people.show', compact('person'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function edit(People $people)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, People $people)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        abort_if(Gate::denies('people_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $person->delete();

        return back();
    }
    
    public function massDestroy(MassDestroyBeneficiaryRequest $request)
    {
        People::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
     public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new PeopleImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
    
     public function export(Excel $excel)
    {
        return Excel::download(new PeopleExport, 'People.xlsx');
    }
}
