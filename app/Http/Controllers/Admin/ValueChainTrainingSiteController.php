<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyGroupRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\VCTSImport;
use App\Http\Requests\StoreGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use Illuminate\Http\Request;
use App\Models\Facilitator;
use App\Models\Group;
use App\Models\Member;
use App\Models\ValueChain;
use Auth;
use Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ValueChainTrainingSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         abort_if(Gate::denies('vcts_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if ($request->ajax()) {
            $query = Group::select(sprintf('%s.*', (new Group)->table))
                ->whereIn('group_categories', [
                    'VCTS' => 'VCTS',
                ]);
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'vcts_show';
                $editGate      = 'vcts_edit';
                $deleteGate    = 'vcts_delete';
                $crudRoutePart = 'value-chain-training-sites';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('group_categories', function ($row) {
                return $row->group_categories ? Group::GROUP_CATEGORIES_SELECT[$row->group_categories] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);

        }


        return view('admin.valueChainTrainingSite.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //abort_if(Gate::denies('vcts_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $facilitators = Facilitator::all()->pluck('name', 'id');
        $value_chains = ValueChain::all()->pluck('name', 'id');

        return view('admin.valueChainTrainingSite.create', compact('facilitators', 'value_chains'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGroupRequest $request)
    {
        //ddd($request);
        
        $group = Group::create($request->all());

        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $group->facilitators()->save($facilitator, $position);
        }

         return redirect("/admin/value-chain-training-sites/". $group->id);
         
        //return redirect()->route('admin.value-chain-training-sites.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {

    }
    
    public function showGroup(Group $group)
    {
        //dd($group);

        abort_if(Gate::denies('vcts_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $group->load('facilitators', 'members');

       //dd($group);

        return view('admin.valueChainTrainingSite.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //dd($group);
        abort_if(Gate::denies('vcts_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $facilitators = Facilitator::all()->pluck('name', 'id');
        $value_chains = ValueChain::all()->pluck('name', 'id');

        $group->load('facilitators');

        return view('admin.valueChainTrainingSite.edit', compact('facilitators', 'value_chains', 'group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGroupRequest  $request, $id)
    {
        //dd($id);
        $group = group::find($id);
        
        $group->update($request->all());

        $group->facilitators()->detach();
        foreach ($request->input('facilitators', []) as $facilitatorId => $position) {
            $facilitator = Facilitator::find($facilitatorId) ?? Facilitator::firstOrCreate(['name' => $facilitatorId]);
            $group->facilitators()->save($facilitator, $position);
        }

        return redirect()->route('admin.value-chain-training-sites.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        //ddd($group);
        abort_if(Gate::denies('vcts_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $group->delete();

        return back();
    }
    
     public function massDestroy(MassDestroyGroupRequest $request)
    {
        Group::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
     public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');
        
        Excel::queueImport(new VCTSImport, $file);
        
        return back()->with('message', 'Imported Successfully!');
    }
    
   
}
