<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\SmartSubsidiesImport;
use Gate;
use App\Models\SmartSubsidies;
use App\Models\Ward;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Symfony\Component\HttpFoundation\Response;


class SmartSubsidiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('smart_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        if ($request->ajax()) {
            $query = SmartSubsidies::query()->select(sprintf('%s.*', (new SmartSubsidies)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'smart_show';
                $editGate      = 'smart_edit';
                $deleteGate    = 'smart_delete';
                $crudRoutePart = 'smart-subsidies';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });
            
             $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('custodian', function ($row) {
                return $row->custodian ? $row->custodian : "";
            });
            $table->editColumn('value_chain', function ($row) {
                return $row->value_chain ? SmartSubsidies::ValueChain[$row->value_chain] : '';
            });
            // $table->editColumn('latitude', function ($row) {
            //     return $row->latitude ? $row->latitude : "";
            // });
            // $table->editColumn('longitude', function ($row) {
            //     return $row->longitude ? $row->longitude : "";
            // });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }
        
        return view('admin.smartSubsidies.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wards = Ward::all()->pluck('name', 'id');
        
        return view('admin.smartSubsidies.create', compact('wards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $ward = Ward::find($request->input('ward_id')) ?? Ward::updateOrCreate([
            'district' => $request->input('district'),
            'ward_number' => $request->input('ward_number'), 
        ]);
        
        $smartSubsidies = SmartSubsidies::create([
            'ward_id' => $request->ward_id,
            'custodian' => $request->custodian,
            'males' => $request->males,
            'females' => $request->females,
            'total' => $request->total,
            'value_chain' => $request->value_chain,
            'farmer_contribution_value_usd' =>$request->farmer_contribution_value_usd,
            'farmer_contribution_value_rtgs' =>$request->farmer_contribution_value_rtgs,
            'extra_contribution' => $request->extra_contribution,
            'subsidy_allocated_per_hh' => $request->subsidy_allocated_per_hh,
            'collected' => $request->collected,
            'outstanding' => $request->outstanding,
            'status'=> $request->status,
        ]);
        
        return redirect()->route('admin.smart-subsidies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SmartSubsidies  $smartSubsidies
     * @return \Illuminate\Http\Response
     */
    public function show(SmartSubsidies $smartSubsidies)
    {
        abort_if(Gate::denies('smart_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        //dd($smartSubsidies);
        
        return view('admin.smartSubsidies.show', compact('smartSubsidies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SmartSubsidies  $smartSubsidies
     * @return \Illuminate\Http\Response
     */
    public function edit(SmartSubsidies $smartSubsidies)
    {
        abort_if(Gate::denies('smart_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $wards = Ward::all()->pluck('name', 'id');
        
        return view('admin.smartSubsidies.edit', compact('wards','smartSubsidies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SmartSubsidies  $smartSubsidies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmartSubsidies $smartSubsidies)
    {
       // dd($request);
        
        // $ward = Ward::find($request->input('ward_id')) ?? Ward::updateOrCreate([
        //     'district' => $request->input('district'),
        //     'ward_number' => $request->input('ward_number'), 
        // ]);
    
       $smartSubsidies= SmartSubsidies::find($request->input('id'));
        
        $smartSubsidies->update([
            'ward_id' => $request->ward_id,
            'custodian' => $request->custodian,
            'males' => $request->males,
            'females' => $request->females,
            'total' => $request->total,
            'value_chain' => $request->value_chain,
            'farmer_contribution' =>$request->farmer_contribution,
            'extra_contribution' => $request->extra_contribution,
            'subsidy_allocated' => $request->subsidy_allocated,
            'collected' => $request->collected,
            'outstanding' => $request->outstanding,
            'status'=> $request->status,
        ]);
        
        return redirect()->route('admin.smart-subsidies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SmartSubsidies  $smartSubsidies
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmartSubsidies $smartSubsidies)
    {
        abort_if(Gate::denies('smart_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $smartSubsidies->delete();

        return back();
    }
    
    public function massDestroy(Request $request)
    {
        SmartSubsidies::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
    public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new SmartSubsidiesImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
}
