<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Models\Beneficiary;
use App\Models\Distribution;
use App\Models\Training;
use App\Models\Group;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('attendance_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
           if ($request->ajax()) {
            $query = Member::with(['beneficiary', 'group'])->select(sprintf('%s.*', (new Member)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'attendance_show';
                $editGate      = 'attendance_edit';
                $deleteGate    = 'attendance_delete';
                $crudRoutePart = 'attendances';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('beneficiary_first_name', function ($row) {
                return $row->beneficiary ? $row->beneficiary->first_name : '';
            });

            $table->editColumn('beneficiary.surname', function ($row) {
                return $row->beneficiary ? (is_string($row->beneficiary) ? $row->beneficiary : $row->beneficiary->surname) : '';
            });
            // $table->addColumn('group_name', function ($row) {
            //     return $row->group ? $row->group->name : '';
            // });

            $table->rawColumns(['actions', 'placeholder', 'beneficiary', 'group']);

            return $table->make(true);
        }

        return view('admin.members.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function createForGroup(Request $request){

        $group = group::find($request->group_id);
        
        
        return view('admin.members.create_for_group', compact('group'));
    }
    
    public function createForValueChainTrainingSite(Request $request){

        $group = group::find($request->group_id);
        
        
        return view('admin.members.create_for_valuechaintrainingsite', compact('group'));
    }
    
    public function createForCareGroup(Request $request)
    {

        $group = Group::find($request->group_id);
        
        
        return view('admin.members.create_for_caregroup', compact('group'));
    }
    
     public function createForIsals(Request $request)
     {

        $group = Group::find($request->group_id);
        
        
        return view('admin.members.create_for_isal', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
     public function massStore(Request $request)
    {
        
        $group = Group::find($request->input('group_id'));

        if ($group) {

            
            foreach ($request->input('attendances') ?? [] as $value) {
                
                
                $member = Member::updateOrCreate([
                    'beneficiary_id' => $value['beneficiary_id'],
                    'group_id' => $group->id
                ], []);

            }
             
            $member->save();

            return redirect()->route('admin.groups.show', $group->id);
        } else {
            return redirect()->route('admin.groups.index')->with('error', 'Members register not updated! The group you tried updating no longer exists');
        }
    }
    
     public function massStoreValueChainTrainingSite(Request $request)
    {
        
        $group = Group::find($request->input('group_id'));

        if ($group) {

            
            foreach ($request->input('attendances') ?? [] as $value) {
                
                
                $member = Member::updateOrCreate([
                    'beneficiary_id' => $value['beneficiary_id'],
                    'group_id' => $group->id
                ], []);

            }
             
            $member->save();

            return redirect()->route('admin.value-chain-training-sites.show', $group->id);
        } else {
            return redirect()->route('admin.value-chain-training-sites.index')->with('error', 'Members register not updated!');
        }
    }
    
    public function massStoreCareGroup(Request $request)
    {
        
        $group = Group::find($request->input('group_id'));

        if ($group) {

            
            foreach ($request->input('attendances') ?? [] as $value) {
                
                
                $member = Member::updateOrCreate([
                    'beneficiary_id' => $value['beneficiary_id'],
                    'group_id' => $group->id
                ], []);

            }
             
            $member->save();

            return redirect()->route('admin.care-groups.show', $group->id);
        } else {
            return redirect()->route('admin.care-groups.index')->with('error', 'Members register not updated!');
        }
    }
    
    public function massStoreIsal(Request $request)
    {
        
        $group = Group::find($request->input('group_id'));

        if ($group) {

            
            foreach ($request->input('attendances') ?? [] as $value) {
                
                
                $member = Member::updateOrCreate([
                    'beneficiary_id' => $value['beneficiary_id'],
                    'group_id' => $group->id
                ], []);

            }
             
            $member->save();

            return redirect()->route('admin.isals.show', $group->id);
        } else {
            return redirect()->route('admin.isals.index')->with('error', 'Members register not updated!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        abort_if(Gate::denies('attendance_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $beneficiaries = Beneficiary::all()->pluck('first_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $groups = Group::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $member->load('beneficiary', 'group');

        return view('admin.member.edit', compact('beneficiaries', 'group', 'member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        // $member->update($request->all());

        // return redirect()->route('admin.members.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        abort_if(Gate::denies('attendance_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $member->delete();

        return back();
    }
    
    public function massDestroy(Request $request)
    {
        Member::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
    
}
