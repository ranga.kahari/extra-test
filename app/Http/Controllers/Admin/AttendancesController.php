<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyAttendanceRequest;
use App\Http\Requests\StoreAttendanceRequest;
use App\Http\Requests\UpdateAttendanceRequest;
use App\Models\Attendance;
use App\Models\Beneficiary;
use App\Models\Distribution;
use App\Models\Training;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AttendancesController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('attendance_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Attendance::with(['beneficiary', 'training'])->select(sprintf('%s.*', (new Attendance)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'attendance_show';
                $editGate      = 'attendance_edit';
                $deleteGate    = 'attendance_delete';
                $crudRoutePart = 'attendances';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('beneficiary_first_name', function ($row) {
                return $row->beneficiary ? $row->beneficiary->first_name : '';
            });

            $table->editColumn('beneficiary.surname', function ($row) {
                return $row->beneficiary ? (is_string($row->beneficiary) ? $row->beneficiary : $row->beneficiary->surname) : '';
            });
            $table->addColumn('training_name', function ($row) {
                return $row->training ? $row->training->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'beneficiary', 'training']);

            return $table->make(true);
        }

        return view('admin.attendances.index');
    }

    public function create()
    {
        abort_if(Gate::denies('attendance_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $beneficiaries = Beneficiary::all()->pluck('first_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $trainings = Training::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.attendances.create', compact('beneficiaries', 'trainings'));
    }

    public function createForTraining(Request $request){

        $training = Training::find($request->training_id);


        return view('admin.attendances.create_for_training', compact('training'));
    }
    

    public function store(StoreAttendanceRequest $request)
    {
        $attendance = Attendance::create($request->all());

        return redirect()->route('admin.attendances.index');
    }

    public function massStore(StoreAttendanceRequest $request)
    {
        // abort_unless(\Gate::allows('attendance_create'), 403);



        $training = Training::find($request->input('training_id'));

        if ($training) {


            foreach ($request->input('attendances') ?? [] as $value) {

                $attendance = Attendance::updateOrCreate([
                    'beneficiary_id' => $value['beneficiary_id'],
                    'training_id' => $training->id
                ], []);

            }


            $attendance->save();

            return redirect()->route('admin.trainings.show', $training->id);
        } else {
            return redirect()->route('admin.activities.index', [
                'thematic_area_slug' => $request->input('thematic_area_slug')
            ])->with('error', 'Attendance register not updated! The activity you tried updating no longer exists');
        }
    }


    public function edit(Attendance $attendance)
    {
        abort_if(Gate::denies('attendance_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $beneficiaries = Beneficiary::all()->pluck('first_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $trainings = Training::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $attendance->load('beneficiary', 'training');

        return view('admin.attendances.edit', compact('beneficiaries', 'trainings', 'attendance'));
    }

    public function update(UpdateAttendanceRequest $request, Attendance $attendance)
    {
        $attendance->update($request->all());

        return redirect()->route('admin.attendances.index');
    }

    public function show(Attendance $attendance)
    {
        abort_if(Gate::denies('attendance_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $attendance->load('beneficiary', 'training');

        return view('admin.attendances.show', compact('attendance'));
    }

    public function destroy(Attendance $attendance)
    {
        abort_if(Gate::denies('attendance_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $attendance->delete();

        return back();
    }

    public function massDestroy(MassDestroyAttendanceRequest $request)
    {
        Attendance::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
