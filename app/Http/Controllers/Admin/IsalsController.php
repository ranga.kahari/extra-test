<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Http\Request;
use App\Http\Requests\MassDestroyGroupRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\StoreGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Imports\CareGroupImport;
use App\Imports\IsalsImport;;
use App\Models\Ward;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class IsalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('group_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
         
        if ($request->ajax()) {
            $query = Group::select(sprintf('%s.*', (new Group)->table))
                ->whereIn('group_categories', [
                    'ISALS Group' => 'ISALS Group',
                ]);
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {

                $viewGate      = 'isals_show';
                $editGate      = 'isals_edit';
                $deleteGate    = 'isals_delete';
               $crudRoutePart = 'isals';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('group_categories', function ($row) {
                return $row->group_categories ? Group::GROUP_CATEGORIES_SELECT[$row->group_categories] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);

        }

        return view('admin.isals.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.isals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dist=new Group();
        $dist=$request->all();
        // $dist['group_district']=
        $group = Group::create($request->all());
        
        return redirect("/admin/isals/" . $group->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        abort_if(Gate::denies('isals_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $wards = Ward::all()->pluck('name', 'id');
        $group->load('members');

        return view('admin.isals.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        abort_if(Gate::denies('isals_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        return view('admin.isals.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = group::find($id);

        $group->update($request->all());

        return redirect()->route('admin.isals.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        abort_if(Gate::denies('group_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $group->delete();

        return back();
    }
    
    public function massDestroy(MassDestroyGroupRequest $request)
    {
        Group::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
    
     public function import(Request $request)
    {
        $file = $request->file('excel_file')->store('excel_imports');

        Excel::queueImport(new IsalsImport, $file);

        return back()->with('message', 'Imported Successfully!');
    }
}
