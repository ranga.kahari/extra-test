<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAgeBandRequest;
use App\Http\Requests\UpdateAgeBandRequest;
use App\Http\Resources\Admin\AgeBandResource;
use App\Models\AgeBand;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AgeBandsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('age_band_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AgeBandResource(AgeBand::all());
    }

    public function store(StoreAgeBandRequest $request)
    {
        $ageBand = AgeBand::create($request->all());

        return (new AgeBandResource($ageBand))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(AgeBand $ageBand)
    {
        abort_if(Gate::denies('age_band_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AgeBandResource($ageBand);
    }

    public function update(UpdateAgeBandRequest $request, AgeBand $ageBand)
    {
        $ageBand->update($request->all());

        return (new AgeBandResource($ageBand))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(AgeBand $ageBand)
    {
        abort_if(Gate::denies('age_band_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ageBand->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
