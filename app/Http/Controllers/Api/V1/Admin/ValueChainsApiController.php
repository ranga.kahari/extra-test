<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreValueChainRequest;
use App\Http\Requests\UpdateValueChainRequest;
use App\Http\Resources\Admin\ValueChainResource;
use App\Models\ValueChain;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValueChainsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('value_chain_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ValueChainResource(ValueChain::all());
    }

    public function store(StoreValueChainRequest $request)
    {
        $valueChain = ValueChain::create($request->all());

        return (new ValueChainResource($valueChain))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ValueChain $valueChain)
    {
        abort_if(Gate::denies('value_chain_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ValueChainResource($valueChain);
    }

    public function update(UpdateValueChainRequest $request, ValueChain $valueChain)
    {
        $valueChain->update($request->all());

        return (new ValueChainResource($valueChain))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ValueChain $valueChain)
    {
        abort_if(Gate::denies('value_chain_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $valueChain->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
