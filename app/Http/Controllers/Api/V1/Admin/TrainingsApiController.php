<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTrainingRequest;
use App\Http\Requests\UpdateTrainingRequest;
use App\Http\Resources\Admin\TrainingResource;
use App\Models\Training;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TrainingsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('training_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrainingResource(Training::with(['village', 'thematic_areas', 'value_chains', 'topics', 'facilitators'])->get());
    }

    public function store(StoreTrainingRequest $request)
    {
        $training = Training::create($request->all());
        $training->thematic_areas()->sync($request->input('thematic_areas', []));
        $training->value_chains()->sync($request->input('value_chains', []));
        $training->topics()->sync($request->input('topics', []));
        $training->facilitators()->sync($request->input('facilitators', []));

        return (new TrainingResource($training))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Training $training)
    {
        abort_if(Gate::denies('training_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrainingResource($training->load(['village', 'thematic_areas', 'value_chains', 'topics', 'facilitators']));
    }

    public function update(UpdateTrainingRequest $request, Training $training)
    {
        $training->update($request->all());
        $training->thematic_areas()->sync($request->input('thematic_areas', []));
        $training->value_chains()->sync($request->input('value_chains', []));
        $training->topics()->sync($request->input('topics', []));
        $training->facilitators()->sync($request->input('facilitators', []));

        return (new TrainingResource($training))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Training $training)
    {
        abort_if(Gate::denies('training_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $training->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
