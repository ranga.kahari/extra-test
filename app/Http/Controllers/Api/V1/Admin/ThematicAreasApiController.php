<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreThematicAreaRequest;
use App\Http\Requests\UpdateThematicAreaRequest;
use App\Http\Resources\Admin\ThematicAreaResource;
use App\Models\ThematicArea;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ThematicAreasApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('thematic_area_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ThematicAreaResource(ThematicArea::all());
    }

    public function store(StoreThematicAreaRequest $request)
    {
        $thematicArea = ThematicArea::create($request->all());

        return (new ThematicAreaResource($thematicArea))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ThematicArea $thematicArea)
    {
        abort_if(Gate::denies('thematic_area_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ThematicAreaResource($thematicArea);
    }

    public function update(UpdateThematicAreaRequest $request, ThematicArea $thematicArea)
    {
        $thematicArea->update($request->all());

        return (new ThematicAreaResource($thematicArea))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ThematicArea $thematicArea)
    {
        abort_if(Gate::denies('thematic_area_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $thematicArea->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
