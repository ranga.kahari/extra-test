<?php

namespace App\Http\Requests;

use App\Models\ValueChain;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreValueChainRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('value_chain_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'       => [
                'string',
                'required',
                'unique:value_chains',
            ],
            'categories' => [
                'required',
            ],
        ];
    }
}
