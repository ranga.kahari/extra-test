<?php

namespace App\Http\Requests;

use App\Models\Training;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreTrainingRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('training_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'             => [
                'string',
                'required',
            ],
            'village_id'       => [
                'required',
                'integer',
            ],
            'venue'            => [
                'string',
                'required',
            ],
            'start_date'       => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'end_date'         => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'prepared_by'      => [
                'string',
                'required',
            ],
            'reviewed_by'      => [
                'string',
            ],
            'thematic_areas.*' => [
                'integer',
            ],
            'thematic_areas'   => [
                'array',
            ],
            'value_chains.*'   => [
                'integer',
            ],
            'value_chains'     => [
                'array',
            ],
            'topics'           => [
                'array',
            ],
            'facilitators'     => [
                'array',
            ],
        ];
    }
}
