<?php

namespace App\Http\Requests;

use App\Models\AgeBand;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateAgeBandRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('age_band_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'        => [
                'string',
                'required',
                'unique:age_bands,name,' . request()->route('age_band')->id,
            ],
            'lower_limit' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'upper_limit' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
