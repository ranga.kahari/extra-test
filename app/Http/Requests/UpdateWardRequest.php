<?php

namespace App\Http\Requests;

use App\Models\Ward;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateWardRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ward_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'district' => [
                'required',
            ],
            'name'     => [
                'string',
                'required',
            ],
        ];
    }
}
