<?php

namespace App\Http\Requests;

use App\Models\ValueChain;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyValueChainRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('value_chain_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:value_chains,id',
        ];
    }
}
