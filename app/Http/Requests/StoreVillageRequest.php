<?php

namespace App\Http\Requests;

use App\Models\Village;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreVillageRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('village_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ward_id' => [
                'required',
                'integer',
            ],
            'name'    => [
                'string',
                'required',
            ],
        ];
    }
}
