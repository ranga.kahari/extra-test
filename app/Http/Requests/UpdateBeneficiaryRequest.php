<?php

namespace App\Http\Requests;

use App\Models\Beneficiary;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateBeneficiaryRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('beneficiary_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'first_name'                       => [
                'string',
                'required',
            ],
            'surname'                          => [
                'string',
                'nullable',
            ],
            'gender'                           => [
                'required',
            ],
            'national_id_number'               => [
                'string',
                'nullable',
            ],
            'year_of_birth'                    => [
                'nullable',
                'integer',
                'min:0',
                'max:2147483647',
            ],
            'phone_number'                     => [
                'string',
                'nullable',
            ],
            'is_household_head'                => [
                'required',
            ],
            'name_of_household_head'           => [
                'string',
                'nullable',
            ],
            'id_of_household_head'             => [
                'string',
                'nullable',
            ],
            'size_of_household'                => [
                'nullable',
                'integer',
                'min:0',
                'max:2147483647',
            ],
            'females_in_household'             => [
                'nullable',
                'integer',
                'min:0',
                'max:2147483647',
            ],
            'children_under_five_in_household' => [
                'nullable',
                'integer',
                'min:0',
                'max:2147483647',
            ],
            'production_area'                  => [
                'nullable',
                'integer',
                'min:0',
                'max:2147483647',
            ],
            'village_id'                       => [
                'required',
            ],
            'groups'                           => [
                'array',
            ],
            'value_chains.*'                   => [
                'integer',
            ],
            'value_chains'                     => [
                'array',
            ],
            'farmer_category'                  => [
                'required',
            ],
        ];
    }
}
