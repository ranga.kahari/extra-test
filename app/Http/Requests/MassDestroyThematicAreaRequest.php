<?php

namespace App\Http\Requests;

use App\Models\ThematicArea;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyThematicAreaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('thematic_area_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:thematic_areas,id',
        ];
    }
}
