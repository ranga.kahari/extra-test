<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Gate;
use Illuminate\Http\Response;

class UpdateDistributionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('distribution_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project' => [
                'string',
                'required',
            ],
            'item_distributed_type' => [
                'string',
                'required',
            ],
            'date' => [
                'string',
                'required',
            ],
            'itemDistributed' => [
                'array',
            ],
            'variety' => [
                'string',
                'nullable',
            ],
            'quantity' => [
                'string',
                // 'required',
            ],
        ];
    }
}
