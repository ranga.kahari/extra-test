<?php

namespace App\Http\Requests;

use App\Models\Ward;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreWardRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ward_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'district' => [
                'required',
            ],
            'name'     => [
                'string',
                'required',
            ],
        ];
    }
}
