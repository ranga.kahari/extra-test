<?php

namespace App\Http\Requests;

use App\Models\Group;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateGroupRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('group_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'           => [
                'string',
               // 'required',
                'unique:groups,name,' . request()->input('id'),
            ],
             'group_number'   => [
                'string',
                'required',
                'nullable',
                //'unique:groups',
            ],
            'name_of_health_centre'   => [
                'string',
                'nullable',
            ],
            'demo_type'   => [
                'string',
                'nullable',
            ],
            'creation_date'  => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'facilitators'   => [
                'array',
            ],
        ];
    }
}
