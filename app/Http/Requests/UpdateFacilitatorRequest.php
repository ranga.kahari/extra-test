<?php

namespace App\Http\Requests;

use App\Models\Facilitator;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateFacilitatorRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('facilitator_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'    => [
                'string',
                'required',
                'unique:facilitators,name,' . request()->route('facilitator')->id,
            ],
            'type'    => [
                'required',
            ],
            'contact' => [
                'string',
                'nullable',
            ],
        ];
    }
}
