<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Response;

class StoreInterventionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('intervention_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'village_id' => [
                'required',
                // 'integer',
            ],
            'custodian' => [
                'string',
                'required',
            ],
            'intervention' => [
                'string',
                'required',
            ],
            'latitude' => [
                'numeric',
                'required',
            ],
            'longitude' => [
                'numeric',
                'required',
            ],
        ];
    }
}
