<?php

namespace App\Http\Requests;

use App\Models\ThematicArea;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreThematicAreaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('thematic_area_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'              => [
                'string',
                'required',
                'unique:thematic_areas',
            ],
            'label'             => [
                'string',
                'required',
                'unique:thematic_areas',
            ],
            'slug'              => [
                'string',
                'required',
                'unique:thematic_areas',
            ],
            'show_in_side_menu' => [
                'required',
            ],
            'order'             => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
