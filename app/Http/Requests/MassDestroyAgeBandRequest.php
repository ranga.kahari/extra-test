<?php

namespace App\Http\Requests;

use App\Models\AgeBand;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyAgeBandRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('age_band_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:age_bands,id',
        ];
    }
}
