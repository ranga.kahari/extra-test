<?php

namespace App\Traits;

use App\Models\AgeBand;
use App\Models\Beneficiary;
use App\Models\Person;
use Carbon\Carbon;

trait AgeFunctions
{
    public function setAgeBand(Beneficiary $beneficiary)
    {
        $ageBands = AgeBand::all();

        $age = Carbon::createFromDate($beneficiary->year_of_birth)->age;
        foreach ($ageBands as $ageBand) {
            if ($age >= $ageBand->lower_limit && $age < $ageBand->upper_limit) {
                $beneficiary->age_band_id = $ageBand->id;
                $beneficiary->save();
                break;
            }
        }
    }
    
    public function setAgeBandPerson(Person $person)
    {
        $ageBands = AgeBand::all();

        $age = Carbon::createFromDate($person->year_of_birth)->age;
        foreach ($ageBands as $ageBand) {
            if ($age >= $ageBand->lower_limit && $age < $ageBand->upper_limit) {
                $person->age_band_id = $ageBand->id;
                $person->save();
                break;
            }
        }
    }
}