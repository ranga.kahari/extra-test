<?php

namespace App\Traits;

use Illuminate\Support\Str;
use App\Models\Ward;
use App\Models\Village;
use App\Models\Facilitator;
use App\Models\Group;
use App\Models\Topic;
use App\Models\TrainingCategory;
use App\Models\ThematicArea;
use App\Models\ValueChain;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


trait ProcessesExcelImports
{
    public function getFirstname(array $row = [])
    {
        return Str::of($row['firstname'])
            ->trim()
            ->__toString();
    }

    public function getSurname(array $row = [])
    {
        return Str::of($row['surname'])
            ->trim()
            ->__toString();
    }

    public function getNationalIdNumber(array $row =  [])
    {
        return Str::of($row['id_number'])
            ->trim()
            ->__toString();
    }

    public function getGender(array $row = [])
    {
        if (
            Str::of($row['sex'])
            ->trim()
            ->lower()
            ->startsWith('f')
        ) {
            return 'Female';
        }

        if (
            Str::of($row['sex'])
            ->trim()
            ->lower()
            ->startsWith('m')
        ) {
            return 'Male';
        }

        return 'Unspecified';
    }

    public function getBirthYear(array $row = [])
    {
        return Str::of($row['birth_year'])
            ->trim()
            ->__toString();
    }

    public function getMobileNumber(array $row = [])
    {
        return Str::of($row['mobile'])
            ->trim()
            ->__toString();
    }

    public function getDistrict(array $row = [])
    {
        //Log::info($row['district']);
        return Str::of($row['district'])
            ->trim()
            ->__toString();
    }
   

    public function getWard(array $row = [])
    {
        $wardNumber = Str::of($row['ward'])
            ->trim()
            ->__toString();

        // TODO: Optimize

        return Ward::firstOrCreate([
            'ward_number' => $wardNumber,
            'district' => $this->getDistrict($row)
        ]);
    }

    public function getVillage(array $row = [])
    {
        $villageName =  Str::of($row['village'])
            ->trim()
            ->__toString();;

        // TODO: Optimize
        return Village::firstOrCreate([
            'name' => $villageName,
            'ward_id' => $this->getWard($row)->id
        ]);
    }
    
    public function getHeadOfHouseHold(array $row = [])
    {
        return Str::of($row['is_hohh'])
            ->trim()
            ->__toString();
    }
    
    public function getHeadOfHouseHoldID(array $row = [])
    {
        return Str::of($row['hohh_id'])
            ->trim()
            ->__toString();
    }
    
    public function getHouseHoldSize(array $row = [])
    {
        return Str::of($row['hh_size'])
            ->trim()
            ->__toString();
    }
    
    public function getHouseHoldFemales(array $row = [])
    {
        return Str::of($row['hh_females'])
            ->trim()
            ->__toString();
    }
    
    public function getHouseHoldUnderFive(array $row = [])
    {
        return Str::of($row['under_five'])
            ->trim()
            ->__toString();
    }
    
    public function getProductionArea(array $row = [])
    {
        return Str::of($row['production_area'])
            ->trim()
            ->__toString();
    }
    

    public function getFacilitators(array $row = [])
    {
        return Str::of($row['facilitator'])
            ->trim()
            ->explode(',')
            ->map(function ($facilitator, $key) {
                // $facilitator = 'Ranga Kahari (CBM)'
                $name = Str::of($facilitator)
                    ->before('(')
                    ->trim()
                    ->__toString();

                $type = Str::of($facilitator)
                    ->after('(')
                    ->before(')')
                    ->trim()
                    ->__toString();

                // TODO: Optimize
                return Facilitator::firstOrCreate([
                    'name' => $name,
                ], [
                    'type' => $type
                ]);
            });
    }

    public function getTrainingCategory(array $row = [])
    {
        // TODO: Optimize
        return TrainingCategory::firstOrCreate([
            'name' => $this->getTrainingName($row)
        ]);
    }

    public function getTopics(array $row = [])
    {
        return Str::of($row['topic'])
            ->explode(',')
            ->map(function ($topic, $key) use ($row) {
                // TODO: Optimize
                return Topic::firstOrCreate([
                    'name' => Str::of($topic)->trim()->__toString(),
                ], [
                    'training_category_id' => $this->getTrainingCategory($row)->id
                ]);
            });
    }

    public function getThematicAreas(array $row = [])
    {
        return Str::of($row['thematic_area'])
            ->explode(',')
            ->map(function ($thematicArea, $key) {
                // TODO: Optimize
                return ThematicArea::firstOrCreate([
                    'label' => Str::of($thematicArea)->trim()->__toString(),

                ], [
                    'name' => Str::of($thematicArea)->trim()->__toString(),
                    'slug' => Str::of($thematicArea)->trim()->slug()->__toString()
                ]);
            });
    }

    

    public function getValueChains(array $row = [])
    {
        return Str::of($row['value_chain'])
            ->explode(',')
            ->map(function ($valueChain, $key) {
                // TODO: Optimize
                return ValueChain::firstOrCreate([
                    'name' => Str::of($valueChain)->trim()->__toString()
                ]);
            });
    }

    public function getTrainingCode(array $row = [])
    {
        return Str::of($row['training_code'])
            ->trim()
            ->__toString();
    }

    public function getVenue(array $row = [])
    {
        return Str::of($row['venue'])
            ->trim()
            ->__toString();
    }

    public function getTrainingName(array $row = [])
    {
        return Str::of($row['training_name'])
            ->trim()
            ->__toString();
    }
    
    public function getSizeOfHousehold(array $row = [])
    {
        return Str::of($row['hh_size'])
            ->trim()
            ->__toString();
    }

    public function getGroups(array $row = [])
    {
        return Str::of($row['group_name'])
            ->explode(',')
            ->map(function ($group, $key) {
                // TODO: Optimize
                return Group::firstOrCreate([
                    'name' => $group
                ]);
            });
    }
    
     
    public function getGroupName(array $row = [])
    {
        return Str::of($row['group_name'])
            ->trim()
            ->__toString();
    }
    
    public function getIsalsCategory(array $row = [])
    {
        return Str::of($row['isal_group_category'])
            ->trim()
            ->__toString();
    }
    

    public function getStartDate(array $row = [])
    {
        return Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['start_date']));
    }

    public function getEndDate(array $row = [])
    {
        return Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['end_date']));
    }
    

    public function getGroupCode(array $row = [])
    {
        
       //Log::info($row['group_code']);
        return Str::of($row['group_code'])
            ->trim()
            ->__toString();
    }
    
     public function getCareGroupCode(array $row = [])
    {
        return Str::of($row['code'])
            ->trim()
            ->__toString();
    }
    
    public function getLastname(array $row = [])
    {
        return Str::of($row['lastname'])
            ->trim()
            ->__toString();
    }
      public function getGroupCategory(array $row = [])
    {
        //Log::info($row['group_category']);
        return Str::of($row['group_category'])
            ->trim()
            ->__toString();
    }
    
    
    public function getValueChainsVCTS(array $row=[])
    {
        //Log::info($row['value_chain']);
      $chain=Str::of($row['value_chain'])
      ->trim()
      ->__toString() ;

      $valueChain= ValueChain::firstOrCreate([
         'name'=>$chain
      ]);
    return  $valueChain;
    }
    
    public function getVCTSDemoType(array $row = [])
    {
        return Str::of($row['demo_type'])
            ->trim()
            ->__toString();
    }
    
    public function getHealthCentreName(array $row = [])
    {
        return Str::of($row['name_of_health_centre'])
            ->trim()
            ->__toString();
    }
    
    
    public function getPersonRegistered(array $row = [])
    {
        //Log::info($row['facilitator_one']);
        return Str::of($row['facilitator_one'])
            ->trim()
            ->explode(',')
            ->map(function ($facilitator, $key) {
                // $facilitator = 'Ranga Kahari (CBM)'
                $name = Str::of($facilitator)
                    ->before('(')
                    ->trim()
                    ->__toString();

                $type = Str::of($facilitator)
                    ->after('(')
                    ->before(')')
                    ->trim()
                    ->__toString();
                    
                // TODO: Optimize
                
                return Facilitator::updateOrCreate([
                    'name' => $name,
                ], [
                    'type' => $type
                ]);
            })->first();
    }
    
    
    public function getVillageHealthWorker(array $row = [])
    {
        //Log::info($row['facilitator_two']);
        return Str::of($row['facilitator_two'])
            ->trim()
            ->explode(',')
            ->map(function ($facilitator , $key) {
                // $facilitator = 'Ranga Kahari (CBM)'
                $name = Str::of($facilitator)
                    ->before('(')
                    ->trim()
                    ->__toString();

                $type = Str::of($facilitator)
                    ->after('(')
                    ->before(')')
                    ->trim()
                    ->__toString();

                // TODO: Optimize
                return Facilitator::updateOrCreate([
                    'name' => $name,
                ], [
                    'type' => $type
                ]);
            })->first();
    }
    
    public function getVCTSFacilitator(array $row = [])
    {
        //Log::info($row['facilitator']);
        return Str::of($row['facilitator'])
            ->trim()
            ->explode(',')
            ->map(function ($facilitator , $key) {
                // $facilitator = 'Ranga Kahari (CBM)'
                $name = Str::of($facilitator)
                    ->before('(')
                    ->trim()
                    ->__toString();

                $type = Str::of($facilitator)
                    ->after('(')
                    ->before(')')
                    ->trim()
                    ->__toString();

                // TODO: Optimize
                return Facilitator::updateOrCreate([
                    'name' => $name,
                ], [
                    'type' => $type
                ]);
            })->first();
    }
    
    public function getFarmerCategory(array $row = [])
    {
        return Str::of($row['farmer_category'])
            ->trim()
            ->__toString();
    }
    
    public function getFarmerGroupMembership(array $row = [])
    {
        return Str::of($row['farmer_group_membership'])
            ->trim()
            ->__toString();
    }
    
    public function getChildrenUnderFive(array $row = [])
    {
        return Str::of($row['number_of_children_under_five'])
            ->trim()
            ->__toString();
    }
    
    
    //Facilitators
    
    public function getFacilitatorName(array $row = [])
    {
        return Str::of($row['name'])
            ->trim()
            ->__toString();
    }
    
    public function getFacilitatorType(array $row = [])
    {
        return Str::of($row['type'])
            ->trim()
            ->__toString();
    }
    
    public function getFacilitatorContact(array $row = [])
    {
        return Str::of($row['contact'])
            ->trim()
            ->__toString();
    }
    
    public function getFacilitatorDistrict(array $row = [])
    {
        return Str::of($row['district'])
            ->trim()
            ->__toString();
    }
    
     public function getFacilitatorWard(array $row = [])
    {
        return Str::of($row['ward'])
            ->trim()
            ->__toString();
    }
    
    
    //Interventions
    
    public function getInterventionWard(array $row = [])
    {
        $wardNumber = Str::of($row['ward'])
            ->trim()
            ->__toString();
            
        $ward = Ward::firstOrCreate([
            'district' => $this->getDistrict($row),
            'ward_number' => $wardNumber
        ]);
            
        return $ward;
    }
    
    public function getInterventionVillage(array $row = [])
    {
        $villageName = Str::of($row['village'])
            ->trim()
            ->__toString();
            
        $village = Village::updateOrCreate([
            'name' => $villageName, 
            'ward_id' => $this->getInterventionWard($row)->id
        ]);
        
        return $village;
    }
    
    
    
    public function getCustodian(array $row = [])
    {
        return Str::of($row['beneficiaryname_or_group_name'])
            ->trim()
            ->__toString();
    }
    
     public function getIntervention(array $row = [])
    {
        return Str::of($row['name_of_intervention_being_mapped'])
            ->trim()
            ->__toString();
    }
    
    public function getLatitude(array $row = [])
    {
        return Str::of($row['gps_coordinates_latitude'])
            ->trim()
            ->__toString();
    }
    
     public function getLongitude(array $row = [])
    {
        return Str::of($row['gps_coordinates_longitude'])
            ->trim()
            ->__toString();
    }
    
    //Distribution
    
    public function getProject(array $row = [])
    {
        return Str::of($row['project'])
            ->trim()
            ->__toString();
    }
    
    public function getItemsDistributedType(array $row = [])
    {
        return Str::of($row['items_distributed_type'])
            ->trim()
            ->__toString();
    }
    
    public function getDate(array $row = [])
    {
        return Str::of($row['date'])
            ->trim()
            ->__toString();
    }
    
    public function getItemsDistributed(array $row = [])
    {
        return Str::of($row['items_distributed'])
            ->trim()
            ->__toString();
    }
    
    public function getVariety(array $row = [])
    {
        return Str::of($row['variety'])
            ->trim()
            ->__toString();
    }
    
    public function getQuantity(array $row = [])
    {
        return Str::of($row['quantity'])
            ->trim()
            ->__toString();
    }
    
    public function getBeneficiary(array $row = [])
    {
        return Str::of($row['items_distributed'])
            ->trim()
            ->__toString();
    }
    
    public function getDistributionDate(array $row = [])
    {
        return Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['date']));
    }
    
    //Crops
    
    public function getSeasonYear(array $row = [])
    {
        return Str::of($row['season'])
            ->trim()
            ->__toString();
    }
    
    public function getYieldValueChain(array $row = [])
    {
        return Str::of($row['value_chain'])
            ->trim()
            ->__toString();
    }
    
    public function getAreaPlanted(array $row = [])
    {
        return Str::of($row['area_planted'])
            ->trim()
            ->__toString();
    }
    
    public function getYield(array $row = [])
    {
        return Str::of($row['yield_ha'])
            ->trim()
            ->__toString();
    }
    
    public function getProduction(array $row = [])
    {
        return Str::of($row['production'])
            ->trim()
            ->__toString();
    }
    
    //Pfumvudza
    
    public function getAgronomicPractice(array $row = [])
    {
        return Str::of($row['agronomic_practice'])
            ->trim()
            ->__toString();
    }
    
    
    //Facilitators - Teachers
    
    public function getTeacherDistrict(array $row = [])
    {
        //Log::info($row['district']);
        return Str::of($row['district'])
            ->trim()
            ->__toString();
    }
   

    public function getTeacherWard(array $row = [])
    {
        $wardNumber = Str::of($row['ward'])
            ->trim()
            ->__toString();

        // TODO: Optimize

        return Ward::firstOrCreate([
            'ward_number' => $wardNumber,
            'district' => $this->getDistrict($row)
        ]);
    }

    public function getTeacherVillage(array $row = [])
    {
        $villageName =  Str::of($row['ward'])
            ->trim()
            ->__toString();;

        // TODO: Optimize
        return Village::firstOrCreate([
            'name' => $villageName,
            'ward_id' => $this->getWard($row)->id
        ]);
    }
    
    //Smart Subsidies
    
    public function getSmartGroup(array $row = [])
    {
        return Str::of($row['group'])
            ->trim()
            ->__toString();
    }
    
    public function getSmartWard(array $row = [])
    {
        $wardNumber = Str::of($row['ward'])
            ->trim()
            ->__toString();
            
        $ward = Ward::firstOrCreate([
            'district' => $this->getDistrict($row),
            'ward_number' => $wardNumber
        ]);
            
        return $ward;
    }
    
    public function getMales(array $row = [])
    {
        return Str::of($row['males'])
            ->trim()
            ->__toString();
    }
    
    public function getFemales(array $row = [])
    {
        return Str::of($row['females'])
            ->trim()
            ->__toString();
    }
    
     public function getTotal(array $row = [])
    {
        return Str::of($row['total'])
            ->trim()
            ->__toString();
    }
    
     public function getSmartValueChain(array $row = [])
    {
        return Str::of($row['value_chain'])
            ->trim()
            ->__toString();
    }
    
     public function getFarmerContributionUSD(array $row = [])
    {
        return Str::of($row['farmer_contribution_value_usd'])
            ->trim()
            ->__toString();
    }
    
    public function getFarmerContributionRTGS(array $row = [])
    {
        return Str::of($row['farmer_contribution_value_rtgs'])
            ->trim()
            ->__toString();
    }
    
     public function getExtraContribution(array $row = [])
    {
        return Str::of($row['extra_contribution'])
            ->trim()
            ->__toString();
    }
    
     public function getCollected(array $row = [])
    {
        return Str::of($row['collected'])
            ->trim()
            ->__toString();
    }
    
     public function getOutStanding(array $row = [])
    {
        return Str::of($row['outstanding'])
            ->trim()
            ->__toString();
    }
    
     public function getSubsidyAllocated(array $row = [])
    {
        return Str::of($row['subsidy_allocated_per_hh'])
            ->trim()
            ->__toString();
    }
    
     public function getStatus(array $row = [])
    {
        return Str::of($row['status'])
            ->trim()
            ->__toString();
    }


}
