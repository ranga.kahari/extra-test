<?php

namespace App\Traits;

use Illuminate\Support\Str;
use App\Models\Ward;
use App\Models\Village;
use App\Models\Facilitator;
use App\Models\Group;
use App\Models\Topic;
use App\Models\TrainingCategory;
use App\Models\ThematicArea;
use App\Models\ValueChain;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


trait NewBeneficiaryImport
{
    
    public function getFirstname(array $row = [])
    {
        return Str::of($row['firstname'])
            ->trim()
            ->__toString();
    }

    public function getLastname(array $row = [])
    {
        return Str::of($row['lastname'])
            ->trim()
            ->__toString();
    }
    
    public function getGender(array $row = [])
    {
        if (
            Str::of($row['sex'])
            ->trim()
            ->lower()
            ->startsWith('f')
        ) {
            return 'Female';
        }

        if (
            Str::of($row['sex'])
            ->trim()
            ->lower()
            ->startsWith('m')
        ) {
            return 'Male';
        }

        return 'Unspecified';
    }

    public function getNationalIdNumber(array $row =  [])
    {
        return Str::of($row['id_number'])
            ->trim()
            ->__toString();
    }

    public function getBirthYear(array $row = [])
    {
        return Str::of($row['birth_year'])
            ->trim()
            ->__toString();
    }

    public function getMobileNumber(array $row = [])
    {
        return Str::of($row['mobile'])
            ->trim()
            ->__toString();
    }

    public function getDistrict(array $row = [])
    {
        //Log::info($row['district']);
        return Str::of($row['district'])
            ->trim()
            ->__toString();
    }
   

    public function getWard(array $row = [])
    {
        $wardNumber = Str::of($row['ward'])
            ->trim()
            ->__toString();

        // TODO: Optimize

        return Ward::firstOrCreate([
            'ward_number' => $wardNumber,
            'district' => $this->getDistrict($row)
        ]);
    }

    public function getVillage(array $row = [])
    {
        $villageName =  Str::of($row['village_name'])
            ->trim()
            ->__toString();;

        // TODO: Optimize
        return Village::firstOrCreate([
            'name' => $villageName,
            'ward_id' => $this->getWard($row)->id
        ]);
    }
    
    public function getHeadOfHouseHold(array $row = [])
    {
        return Str::of($row['is_hohh'])
            ->trim()
            ->__toString();
    }
    
    public function getHeadOfHouseHoldID(array $row = [])
    {
        return Str::of($row['hohh_id'])
            ->trim()
            ->__toString();
    }
    
    public function getHouseHoldSize(array $row = [])
    {
        return Str::of($row['hh_size'])
            ->trim()
            ->__toString();
    }
    
    public function getHouseHoldFemales(array $row = [])
    {
        return Str::of($row['hh_females'])
            ->trim()
            ->__toString();
    }
    
    public function getHouseHoldUnderFive(array $row = [])
    {
        return Str::of($row['under_five'])
            ->trim()
            ->__toString();
    }
    
    public function getChildrenUnderFive(array $row = [])
    {
        return Str::of($row['number_of_children_under_five'])
            ->trim()
            ->__toString();
    }
    
    public function getProductionArea(array $row = [])
    {
        return Str::of($row['production_area'])
            ->trim()
            ->__toString();
    }
    
    
}