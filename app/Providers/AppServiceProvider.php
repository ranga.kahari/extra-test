<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::macro('search', function ($attributes, string $searchTerms) {
            // the following line preserves the order of the search. For example if you search for 'A B' it will not find 'B C A' but it will find 'A C B'
            // $searchTerm = str_replace(' ', '%', $searchTerm);
            $this->where(function (Builder $query) use ($attributes, $searchTerms) {
                foreach (Arr::wrap($attributes) as $attribute) {
                    $query->when(
                        str_contains($attribute, '.'),
                        function (Builder $query) use ($attribute, $searchTerms) {
                            [$relationName, $relationAttribute] = explode('.', $attribute);

                            $query->orWhereHas($relationName, function (Builder $query) use ($relationAttribute, $searchTerms) {
                                foreach (explode(' ', $searchTerms) as $searchTerm) {
                                    $query->where($relationAttribute, 'LIKE', "%{$searchTerm}%");
                                }
                            });
                        },
                        function (Builder $query) use ($attribute, $searchTerms) {
                            $query->orWhere(function ($query) use ($attribute, $searchTerms) {
                                foreach (explode(' ', $searchTerms) as $searchTerm) {
                                    $query->where($attribute, 'LIKE', "%{$searchTerm}%");
                                }
                            });
                        }
                    );
                }
            });

            return $this;
        });
    }
}
