<?php

namespace App\Models;

use \DateTimeInterface;
use App\Models\YieldData;
use App\Models\Distribution;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Beneficiary extends Model
{
    use SoftDeletes;

    public $table = 'beneficiaries';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const GENDER_SELECT = [
        'Male'        => 'Male',
        'Female'      => 'Female',
        'Unspecified' => 'Unspecified',
    ];

    const FARMER_CATEGORY_SELECT = [
        'Food Security'              => 'Food Security',
        'Commercial'                 => 'Commercial',
        'Food Security & Commercial' => 'Food Security & Commercial',
    ];

    const FARMER_GROUP_MEMBERSHIP = [
        'ISAL'                     => 'ISAL',
        'SACCO'                    => 'SACCO',
        'Commodity Association'    => 'Commodity Association',
        'Farmer Group Enterprise'  => 'Farmer Group Enterprise',
    ];

    protected $fillable = [
        'first_name',
        'surname',
        'name',
        'gender',
        'national_id_number',
        'year_of_birth',
        'phone_number',
        'is_household_head',
        'name_of_household_head',
        'id_of_household_head',
        'size_of_household',
        'females_in_household',
        'children_under_five_in_household',
        'production_area',
        'village_id',
        'farmer_category',
        'farmer_group_membership',
        'age_band_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function beneficiaryAttendances()
    {
        return $this->hasMany(Attendance::class, 'beneficiary_id', 'id');
    }
    
     public function beneficiaryMembers()
    {
        return $this->hasMany(Member::class, 'beneficiary_id', 'id');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'village_id');
    }

    public function ageBand()
    {
        return $this->belongsTo(AgeBand::class, 'age_band_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)
            ->withPivot('position');
    }

    public function value_chains()
    {
        return $this->belongsToMany(ValueChain::class);
    }
    
    public function distributions()
    {
        return $this->belongsTo(Distribution::class);
    }
    
    public function crop()
    {
        return $this->belongsTo(Crop::class);
    }
    
    public function pfumvudza()
    {
        return $this->belongsTo(Pfumvudza::class);
    }
}
