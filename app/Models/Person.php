<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Person extends Model
{
    use SoftDeletes;
    
    public $table = 'people';
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const GENDER_SELECT = [
        'Male'        => 'Male',
        'Female'      => 'Female',
        'Unspecified' => 'Unspecified',
    ];

    const FARMER_CATEGORY_SELECT = [
        'Food Security'              => 'Food Security',
        'Commercial'                 => 'Commercial',
        'Food Security & Commercial' => 'Food Security & Commercial',
    ];

    const FARMER_GROUP_MEMBERSHIP = [
        'ISAL'                     => 'ISAL',
        'SACCO'                    => 'SACCO',
        'Commodity Association'    => 'Commodity Association',
        'Farmer Group Enterprise'  => 'Farmer Group Enterprise',
    ];

    protected $fillable = [
        'first_name',
        'surname',
        'name',
        'gender',
        'national_id_number',
        'year_of_birth',
        'phone_number',
        'is_household_head',
        'name_of_household_head',
        'id_of_household_head',
        'size_of_household',
        'females_in_household',
        'children_under_five_in_household',
        'production_area',
        'village_id',
        'farmer_category',
        'farmer_group_membership',
        'age_band_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'village_id');
    }

    public function ageBand()
    {
        return $this->belongsTo(AgeBand::class, 'age_band_id');
    }

}
