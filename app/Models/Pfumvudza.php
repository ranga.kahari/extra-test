<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pfumvudza extends Model
{
    use SoftDeletes;

    public $table = 'pfumvudzas';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    protected $fillable = [
        'district',
        'agronomic_practice',
        'value_chain',
        'variety',
        'area_planted',
        'beneficiary_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    const AGRONOMIC_PRACTICE = [
        'Live mulch' => 'Live mulch',
        'Organic fertilizer' => 'Organic fertilizer',
        'InOrganic fertilizer' => 'InOrganic fertilizer',
        'Water harvesting' => 'Water harvesting',
        'Tied ridges' => 'Tied ridges',
        'Dead level contours' => 'Dead leve; contours',
        'Infiltration pits' => 'Infiltration pits',
        'Dead mulch' => 'Dead mulch',
    ];
    
    const ValueChain = [
        'sunflower'  => 'Sunflower',
        'C.V.4 sorghum'  => 'C.V.4 sorghum',
        'Pearl millet'  => 'Pearl millet',
        'Sunflower'  => 'Sunflower',
        'Poultry'  => 'Poultry',
        'Goats'  => 'Goats',
        'Piggery'  => 'Piggery',
        'Mbambara'  => 'Mbambara',
        'Groundnuts'  => 'Groundnuts',
        'Small grain'  => 'Small grain',
        'Poultry'  => 'Poultry',
        'Beef Fattening'  => 'Beef Fattening',
        'Sorghum'  => 'Sorghum',
        'Orange Maize'  => 'Orange Maize',
        'White Maize'  => 'White Maize',
        'Maize'  => 'Maize',
    ];
    
    
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function beneficiaries()
    {
        return $this->hasMany(Beneficiary::class, 'id','beneficiary_id');
    }
    
    public function facilitators()
    {
        return $this->belongsToMany(Facilitator::class)
            ->withPivot('position');
    }
    
     public function totalFarmers($practise,$district)
    {
        return Pfumvudza::query()->where('agronomic_practice', $practise)->where('district',$district)->count('beneficiary_id');
    }
     public function totalFarmersValueChain($valuechain,$district)
    {
        return Pfumvudza::query()->where('value_chain', $valuechain)->where('district',$district)->count('beneficiary_id');
    }

    public function males($practise,$district)
    {
        $males = 0;
        $checks = Pfumvudza::query()->where('agronomic_practice', $practise)->where('district',$district)->get();
        foreach ($checks as $check) {
            $beneficiary = Beneficiary::query()->where('id', $check->beneficiary_id)->first();
            if ($beneficiary->gender == "Male") {
                $males++;
            }
        }
        return $males;
    }
      public function malesValueChain($valuechain,$district)
    {
        $males = 0;
        $checks = Pfumvudza::query()->where('value_chain', $valuechain)->where('district',$district)->get();
        foreach ($checks as $check) {
            $beneficiary = Beneficiary::query()->where('id', $check->beneficiary_id)->first();
            if ($beneficiary->gender == "Male") {
                $males++;
            }
        }
        return $males;
    }

    public function females($practise,$district)
    {
        $females = 0;
        $checks = Pfumvudza::query()->where('agronomic_practice', $practise)->where('district',$district)->get();
        foreach ($checks as $check) {
            $beneficiary = Beneficiary::query()->where('id', $check->beneficiary_id)->first();
            if ($beneficiary->gender == "Female") {
                $females++;
            }
        }
        return $females;
    }
    
        public function femalesValueChain($valuechain,$district)
    {
        $females = 0;
    $checks = Pfumvudza::query()
       ->where('value_chain',$valuechain)
       ->where('district',$district)
       ->get();
    
        foreach ($checks as $check) {
            $beneficiary = Beneficiary::query()->where('id', $check->beneficiary_id)->first();
            if ($beneficiary->gender == "Female") {
                $females++;
            }
        }
        return $females;
    }
}
