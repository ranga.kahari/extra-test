<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class ValueChain extends Model
{
    use SoftDeletes;

    public $table = 'value_chains';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
       // 'categories',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const CATEGORIES_SELECT = [
        'Food Security'              => 'Food Security',
        'Commercial'                 => 'Commercial',
        'Food Security & Commercial' => 'Food Security & Commercial',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function trainings()
    {
        return $this->belongsToMany(Training::class);
    }
    
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
