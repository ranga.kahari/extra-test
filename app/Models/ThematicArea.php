<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class ThematicArea extends Model
{
    use SoftDeletes;

    public $table = 'thematic_areas';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'label',
        'slug',
        'show_in_side_menu',
        'order',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function trainingCategories()
    {
        return $this->hasMany(TrainingCategory::class, 'thematic_area_id', 'id');
    }

    public function trainings()
    {
        return $this->belongsToMany(Training::class);
    }
    
    public function topics()
    {
        return $this->hasManyThrough(Topic::class, TrainingCategory::class);
    }
}
