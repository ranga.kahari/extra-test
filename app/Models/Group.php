<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use Dotenv\Loader\Value;
use App\Models\Member;

class Group extends Model
{
    use SoftDeletes;

    public $table = 'groups';

    protected $dates = [
        'creation_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    protected $fillable = [
        'group_categories',
        'name',
        'group_number',
        'name_of_health_centre',
        'health_centre_ward',
        'group_district',
        'isals_categories',
        'value_chain_id',
        'demo_type',
        'creation_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const GROUP_CATEGORIES_SELECT = [
        'VCTS'               => 'VCTS',
        'ISAL'               => 'ISAL',
        'Care Group'         => 'Care Group',
        'Isals Group'        => 'ISALS Group',  
    ];

    const DEMO_TYPE  = [
        'Anchor'                => 'Anchor',
        'Technical'             => 'Technical',
        'Commercial'            => 'Commercial',
        'Promotional'           => 'Promotional',
        'Moringa demo'          => 'Moringa demo',
        'BSF demo'              => 'BSF demo',
        'Pfumvudza demo'        => 'Pfumvudza demo',
        'Vermicompost'          => 'Vermicompost',
        'Biochar'               => 'Biochar',
        'Farmer Field School'   => 'Farmer Field School',
        
    ];
    
    const ISAL_GROUP_CATEGORY  = [
        'FGE'                   => 'FGE',
        'ISALS'                 => 'ISALS',
        'SACCO'                 => 'SACCO',
        'COMMODITY ASSOCIATION' => 'COMMODITY ASSOCIATION', 
    ];


 

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function beneficiaries()
    {
        return $this->belongsToMany(Beneficiary::class)
            ->withPivot('position');
    }

    public function members()
    {
        return $this->hasMany(Member::class, 'group_id', 'id');
    }


    public function getCreationDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setCreationDateAttribute($value)
    {
        $this->attributes['creation_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function facilitators()
    {
        return $this->belongsToMany(Facilitator::class)
            ->withPivot('position');
    }

    public function value_chain()
    {
        return $this->belongsTo(ValueChain::class);
    }

     public function valueChains()
    {
        return $this->belongsTo(ValueChain::class);
    }

}
