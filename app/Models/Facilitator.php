<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Facilitator extends Model
{
    use SoftDeletes;

    public $table = 'facilitators';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'Facilitator' => 'Facilitator',
        'CBM' => 'CBM',
        'BCF' => 'BCF',
        'MAMID' => 'MAMID',
        'CAHW' => 'CAHW',
        'Leadmother' => 'Leadmother',
        'Lead Farmer' => 'Lead Farmer',
        'Private Sector' => 'Private Sector',
        'Field Officer' => 'Field Officer',
        'Village Health Worker' => 'Village Health Worker',
    ];

    protected $fillable = [
        'name',
        'type',
        'contact',
        'district',
        'wards',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)
            ->withPivot('position');
    }
    
    public function pfumvudza()
    {
        return $this->belongsToMany(Pfumvudza::class)
            ->withPivot('position');
    }

    public function trainings()
    {
        return $this->belongsToMany(Training::class)
            ->withPivot('position');
    }
    public function countLeadMothers($id){
         $facilitate = Facilitator::query()->where('id', $id)->first();
            $facilitate->load('groups');
            $leadMothers = array();
            $counting = 0;
            foreach ($facilitate->groups as $group) {
                $group = Group::query()->where('id', $group->id)->where('group_categories',"Care Group")->first();
                $leadMothers[] = $group->load('facilitators');
                $data = $group->load('facilitators');
                $counting += (sizeof($data->facilitators) - 1);
            }
            return $counting; 
    }

}
