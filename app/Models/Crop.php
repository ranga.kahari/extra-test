<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Crop extends Model
{
    use SoftDeletes;

    public $table = 'crops';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'district',
        'season',
        'value_chain',
        'area_planted',
        'yield',
        'production',
        'beneficiary_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const ValueChain = [
        'sunflower'  => 'Sunflower',
        'C.V.4 sorghum'  => 'C.V.4 sorghum',
        'Pearl millet'  => 'Pearl millet',
        'Sunflower'  => 'Sunflower',
        'Poultry'  => 'Poultry',
        'Goats'  => 'Goats',
        'Piggery'  => 'Piggery',
        'Mbambara'  => 'Mbambara',
        'Groundnuts'  => 'Groundnuts',
        'Small grain'  => 'Small grain',
        'Poultry'  => 'Poultry',
        'Beef Fattening'  => 'Beef Fattening',
        'Sorghum'  => 'Sorghum',
        'Orange Maize'  => 'Orange Maize',
        'White Maize'  => 'White Maize',
        'Maize'  => 'Maize',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function beneficiaries()
    {
        return $this->hasMany(Beneficiary::class, 'id','beneficiary_id');
    }

    public function total($valueChain, $season,$district)
    {
        return Crop::query()->where('value_chain', $valueChain)->where('season', $season)->where('district',$district)->sum('yield');
    }

}
