<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Ward extends Model
{
    use SoftDeletes;

    public $table = 'wards';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'district',
        'ward_number',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const DISTRICT_SELECT = [
        'Gokwe North' => 'Gokwe North', // old id = 80
        'Gokwe South' => 'Gokwe South', // old id = 81
        'Kwekwe'      => 'Kwekwe',      // old id = 83
        'Shurugwi'    => 'Shurugwi',    // old id = 85
    ];
    
    public function getNameAttribute()
    {
        return $this->district . ' - Ward ' . $this->ward_number;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function villages()
    {
        return $this->hasMany(Village::class, 'ward_id', 'id');
    }
}
