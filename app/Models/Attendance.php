<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;

class Attendance extends Model
{
    public $table = 'attendances';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'beneficiary_id',
        'training_id',
        'distribution_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function beneficiary()
    {
        return $this->belongsTo(Beneficiary::class, 'beneficiary_id');
    }

    public function training()
    {
        return $this->belongsTo(Training::class, 'training_id');
    }
    
    public function distribution()
    {
        return $this->belongsTo(Distribution::class, 'distribution_id');
    }
    
    
}
