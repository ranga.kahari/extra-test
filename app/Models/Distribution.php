<?php

namespace App\Models;

use Carbon\Carbon;
use \DateTimeInterface;
use Dotenv\Loader\Value;
use App\Models\Beneficiary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distribution extends Model
{
     use SoftDeletes;

    public $table = 'distributions';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    protected $fillable = [
        'district',
        'date',
        'item_distributed_type',
        'item_distributed',
        'variety',
        'quantity',
        'project',
        'beneficiary_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    
    const ITEM_DISTRIBUTED_TYPE = [
        'Input' => 'Input',
        'Nutrition' => 'Nutrition',
    ];
    
    const PROJECT_OWNER = [
        'EXTRA' => 'EXTRA',
    ];
    
    const ITEM_DISTRIBUTED = [
        'Sorghum'           => 'Sorghum',
        'Pearl Millet'      => 'Pearl Millet',
        'Cowpeas'           => 'Cowpeas',
        'Orange Maize'      => 'Orange Maize',
        'Velvet Beans'      => 'Velvet Beans',
        'Lime'              => 'Lime',
        'Maize'             => 'Maize',
        'Soya Meal'         => 'Soya Meal',
        'Cotton Seed Cake'  => 'Cotton Seed Cake',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

   public function beneficiaries()
    {
        return $this->hasMany(Beneficiary::class, 'id','beneficiary_id');
    }
    
    public function villages()
    {
        return $this->hasMany(Village::class, 'id','village_id');
    }
    

    public function getCreationDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setCreationDateAttribute($value)
    {
        $this->attributes['creation_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

   
}
