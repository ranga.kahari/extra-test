<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Training extends Model
{
    use SoftDeletes;

    public $table = 'trainings';

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'village_id',
        'venue',
        'training_code',
        'start_date',
        'end_date',
        'prepared_by',
        'reviewed_by',
        'captured_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class, 'training_id', 'id');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'village_id');
    }

    public function getStartDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getEndDateAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function thematicAreas()
    {
        return $this->belongsToMany(ThematicArea::class);
    }

    public function valueChains()
    {
        return $this->belongsToMany(ValueChain::class);
    }

    public function topics()
    {
        return $this->belongsToMany(Topic::class);
    }

    public function facilitators()
    {
        return $this->belongsToMany(Facilitator::class)
            ->withPivot('position');
    }
}
