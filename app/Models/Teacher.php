<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
     use SoftDeletes;

    public $table = 'teachers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'Facilitator' => 'Facilitator',
        'CBM' => 'CBM',
        'MAMID' => 'MAMID',
        'CAHW' => 'CAHW',
        'Lead Mother' => 'Lead Mother',
        'Leadmother' => 'Leadmother',
        'Lead Farmer' => 'Lead Farmer',
        'Private Sector' => 'Private Sector',
        'Field Officer' => 'Field Officer',
        'Village Health Worker' => 'Village Health Worker',
    ];

    protected $fillable = [
        'name',
        'type',
        'contact',
        'wards',
        'district',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
