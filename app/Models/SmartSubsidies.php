<?php

namespace App\Models;

use App\Models\Ward;
use \DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmartSubsidies extends Model
{
    use SoftDeletes;

    public $table = 'smart_subsidies';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'ward_id',
        'custodian',
        'males',
        'females',
        'total',
        'value_chain',
        'farmer_contribution_value_usd',
        'farmer_contribution_value_rtgs',
        'extra_contribution',
        'subsidy_allocated_per_hh',
        'supplier',
        'officer',
        'collected',
        'outstanding',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const ValueChain = [

        'Barbed wire' => 'Barbed wire',
        'Boschveld' => 'Boschveld',
        'Broilers' => 'Broilers',
        'Building Material' => 'Building Material',
        'Capentry' => 'Capentry',
        'Castor Beans' => 'Castor Beans',
        'Cement' => 'Cement',
        'Crop Support' => 'Crop Support',
        'Diamond fence' => 'Diamond fence',
        'Drink making' => 'Drink making',
        'Farm equipment' => 'Farm equipment',
        'Farm implements' => 'Farm implements',
        'Fertilizer' => 'Fertilizer',
        'Groundnuts' => 'Groundnuts',
        'Improved Goats' => 'Improved Goats',
        'Irrigation Pipes' => 'Irrigation Pipes',
        'Livestock support' => 'Livestock support',
        'Maputi Gun' => 'Maputi Gun',
        'Nua 45' => 'Nua 45',
        'Orange Maize' => 'Orange Maize',
        'Piggery' => 'Piggery',
        'Polypipes' => 'Polypipes',
        'Poultry' => 'Poultry',
        'Rabbit' => 'Rabbit',
        'Sheller' => 'Sheller',
        'Solar pump' => 'Solar pump',
        'Vet drugs' => 'Vet drugs',
        'Wire' => 'Wire',
        'Road Runner' => 'Road Runner',


        // 'Poultry Broiler' => 'Poultry/ Broiler',
        // 'Broilers' => 'Broilers',
        // 'Poultry Boshveld' => 'Poultry / Boshveld',
        // 'Boschveld' => 'Boschveld',
        // 'Orange maize' => 'Orange maize',
        // 'Building material' => 'Building material',
        // 'Sheller' => 'Sheller',
        // 'Wire' => 'Wire',
        // 'Castor bean' => 'Castor bean',
        // 'Vet drugs' => 'Vet drugs',
        // 'Piggery' => 'Piggery',
        // 'Nua 45' => 'Nua 45',
        // 'Livestock support_salt' => 'Livestock support/salt',
        // 'Irrigation Pipes' => 'Irrigation Pipes',
        // 'Crop Support_Flat share' => 'Crop Support/ Flat share',
        // 'Vet drugs' => 'Vet drugs',
        // 'Groundnuts' => 'Groundnuts',
        // 'Engine' => 'Engine',
    ];

    const Status = [
        'Fully Processed' => 'Fully Processed',
        'Unprocessed' => 'Unprocessed',
        'Processed' => 'Processed',
    ];

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }


    public function total($district, $valuechain)
    {
        $district = Ward::query()->where('district', $district)->get();
        $ids = array();
        foreach ($district as $dist) {
            $ids[] = $dist->id;
        }
        return SmartSubsidies::query()->where('value_chain', $valuechain)->whereIn('ward_id', $ids)->sum('total');
    }
}
