<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Intervention extends Model
{
     use SoftDeletes;

    public $table = 'interventions';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'district',
        'intervention',
        'custodian',
        'latitude',
        'longitude',
        'village_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
     const INTERVENTION_SELECT = [
        'ISALS' => 'ISALS',
        'ISALS Group' => 'ISALS Group',
        'SACCO' => 'SACCO',
        'Commodity Association' => 'Commodity Association',
        'Caregroup' => 'Caregroup',
        'Solar drier' =>'Solar drier', 
        'Orchards' => 'Orchards',
        'Solarised Nutrition Garden' => 'Solarised Nutrition Garden',    
        'Community Garden' => 'Community Garden',
        'Community Seed Banks' => 'Community Seed Banks',
        'BSF Demo' => 'BSF Demo',
        'Moringa Demo' => 'Moringa Demo',
        'Hammer Mills/Feed Formulation' => 'Hammer Mills/Feed Formulation', 
        'Spray Races' => 'Spray Races',      
        'Renovated Dip Tanks' => 'Renovated Dip Tanks',       
        'Improved Goats' => 'Improved Goats',    
        'Improved Chickens' => 'Improved Chickens',       
        'Vermicompost Site' => 'Vermicompost Site',
        'VCTS/Centre of Excellence' => 'VCTS/Centre of Excellence',
        'Irrigation Pipes'=> 'Irrigation Pipes',              
        'Seed Banks'=> 'Seed Banks',
        'Bulls' => 'Bulls',
        'Fishponds/Aquaculture'=> 'Fishponds/Aquaculture',
        'Borehole' => 'Borehole',
        'Youth Project'=> 'Youth Project',
        'Gender Champion'=> 'Gender Champion',
        
        'Baby Demo (orange maize)' => 'Baby Demo (orange maize)',
        'Bakery' => 'Bakery',
        'BCC' => 'BCC',
        'Beef survival' => 'Beef survival',
        'Behaviour change facilitator' => 'Behaviour change facilitator',
        'Biochar' => 'Biochar',
        'Bicycle(promoter)' =>'Bicycle(promoter)',
        'Borehole (drilled)' =>'Borehole (drilled)',
        'CAHW' =>'CAHW',
        'Cattle Penfattening' =>'Cattle Penfattening ', 
        'CBM' =>'CBM',
        'Chilli Producer group' =>'Chilli Producer group',
        'Community Animal Health Worker' =>'Community Animal Health Worker',
        'Community Garden ' =>'Community Garden ',
        'Community Garden(Established by LFSP)' =>'Community Garden(Established by LFSP)',
        'Community Seed Multiplication Site' =>'Community Seed Multiplication Site',
        'Crisis modifer' =>'Crisis modifer',
        'Dam constraction' =>'Dam constraction',
        'Demo' =>'Demo',
        'Diptank' =>'Diptank',
        'Facilitator' =>'Facilitator',
        'Farmer Field School' =>'Farmer Field School',
        'Feed formulation' =>'Feed formulation',
        'FGE' =>'FGE',
        'Fodder Producing Farmers ' =>'Fodder Producing Farmers ',
        'GALS' =>'GALS',
        'General garden' =>'General garden',
        'Goats Production' =>'Goats Production ', 
        'Groundnuts sheller' =>'Groundnuts sheller',
        'Hammer Mills/Feed Formulation' =>'Hammer Mills/Feed Formulation',
        'Health Centre' =>'Health Centre',
        'Health club' =>'Health club',
        'Healthy facilitator' =>'Healthy facilitator',
        'Improved granary' =>'Improved granary',
        'ISALS Group' =>'ISALS Group',
        'Kenya Trip beneficiary' =>'Kenya Trip beneficiary',
        'Lead farmer' =>'Lead farmer',
        'Livestock demo' =>'Livestock demo',
        'Loan Access beneficiary' =>'Loan Access beneficiary',
        'Metal Silo' =>'Metal Silo',
        'Neighbour Women Group' =>'Neighbour Women Group',
        'Nutrion Garden' =>'Nutrion Garden',
        'Oil press' =>'Oil press',
        'Orange Maize Demo' =>'Orange Maize Demo',
        'Pearl Millet Demo' =>'Pearl Millet Demo',
        'Pfumvudza demo' =>'Pfumvudza demo',
        'Pheromone Traps' =>'Pheromone Traps',
        'Polarised borehole' =>'Polarised borehole',
        'Poultry project' =>'Poultry project',
        'Proposed diptank' =>'Proposed diptank',
        'Raised Goat pen' =>'Raised Goat pen',
        'Rehabilitated Borehole' =>'Rehabilitated Borehole',
        'Ridger' =>'Ridger',
        'Rippers' =>'Rippers',
        'Sewing bussiness' =>'Sewing bussiness',
        'Sheller' =>'Sheller',
        'Shelling service ' =>'Shelling service ',
        'Simit demo' =>'Simit demo',
        'Small grains thresher' =>'Small grains thresher',
        'Smart Subsidy' =>'Smart Subsidy',
        'Sprayrace' =>'Sprayrace',
        'Study Circle group' =>'Study Circle group',
        'Supported diptank(acaricides)' =>'Supported diptank(acaricides)',
        'Technical Demo Site' =>'Technical Demo Site',
        'VCTs / Centre of Excellence livestock' =>'VCTs / Centre of Excellence livestock',
        'VCTS/Centre of Excellence' =>'VCTS/Centre of Excellence',
        'VCTS/Centre of Excellence crops' =>'VCTS/Centre of Excellence crops',
        'Ward Chairperson' =>'Ward Chairperson',

    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
    
    public function village()
    {
        return $this->belongsTo(Village::class, 'village_id');
    }
    
    public function total($intervention,$district)
    {
        return Intervention::query()->where('intervention', $intervention)->where('district',$district)->count();
    }
}
