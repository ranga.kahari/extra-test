<?php

namespace App\Exports;

use App\Models\Pfumvudza;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\FromCollection;

class PfumvudzaExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
     use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //dd(Pfumvudza::all());
        return Pfumvudza::all();
    }
    
    public function map($pfumvudza): array
    {
        return [
                $pfumvudza->id,
                $pfumvudza->Facilitators[0]['name'],
                $pfumvudza->Facilitators[0]['type'],
                $pfumvudza->beneficiaries[0]['first_name'],
                $pfumvudza->beneficiaries[0]['surname'],
                $pfumvudza->beneficiaries[0]['national_id_number'],
                $pfumvudza->beneficiaries[0]['gender'],
                $pfumvudza->beneficiaries[0]['phone_number'],
                $pfumvudza->agronomic_practice,
                $pfumvudza->value_chain, 
                $pfumvudza->variety,            
                $pfumvudza->area_planted,           
                
                        
            
        ];
    }
    
    public function headings(): array
    {
        return [
                 '#',
                 'Facilitator',
                 'Facilitator-type',
                 'Firstname', 
                 'Surname',
                 'National ID Number',
                 'Sex',
                 'Mobile',
                 'Agronomic Practice',
                 'Value Chain', 
                 'Variety', 
                 'Area Planted', 
        ];
    }
}
