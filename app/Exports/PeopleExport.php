<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\FromCollection;

class PeopleExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //dd(People::all());
        return People::all();
    }
    
    public function map($people): array
    {
        return [
                $people->id,
                            
                $people->first_name,
                        
                $people->surname, 
                        
                $people->gender,
                        
                $people->national_id_number,
                        
                $people->year_of_birth,
                        
                $people->phone_number,
                            
                $people->village->ward->district,
                            
                $people->village->ward->ward_number,
                            
                $people->village->name,

                $people->is_household_head,

                $people->name_of_household_head,

                $people->id_of_household_head,
                           
                $people->size_of_household,

                $people->females_in_household,
                          
                $people->children_under_five_in_household,
                            
                $people->production_area,
                             
        ];
    }
    
    public function headings(): array
    {
        return [
                            '#',
                            'first_name',
                            'surname', 
                            'gender',
                            'national_id_number',
                            'year_of_birth',
                            'phone_number',
                            'District',
                            'Ward Number',
                            'Village Name',
                            'Is HoHH',
                            'HoHH Name',
                            'HoHH ID',
                            'HH Size',
                            'HH Females',
                            'Under Five',
                            'Production Area',
        ];
    }
    
}
