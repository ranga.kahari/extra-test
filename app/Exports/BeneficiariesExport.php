<?php

namespace App\Exports;

use App\Models\Beneficiary;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class BeneficiariesExport implements withMapping, ShouldAutoSize, WithHeadings, FromQuery
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return  Beneficiary::query()->with('village', 'value_chains'); 
    }
    
    public function map($beneficiary): array
    {
        return [
                $beneficiary->id,
                            
                $beneficiary->first_name,
                        
                $beneficiary->surname, 
                        
                $beneficiary->gender,
                        
                $beneficiary->national_id_number,
                        
                $beneficiary->year_of_birth,
                        
                $beneficiary->phone_number,
                            
                $beneficiary->village->ward->district,
                            
                $beneficiary->village->ward->ward_number,
                            
                $beneficiary->village->name,

                $beneficiary->is_household_head,

                $beneficiary->name_of_household_head,

                $beneficiary->id_of_household_head,
                           
                $beneficiary->size_of_household,

                $beneficiary->females_in_household,
                          
                $beneficiary->children_under_five_in_household,
                            
                $beneficiary->production_area,
                             
                $beneficiary->value_chains[0]['name'] ?? null,
        ];
    }
    
    public function headings(): array
    {
        return [
                            '#',
                            'first_name',
                            'surname', 
                            'gender',
                            'national_id_number',
                            'year_of_birth',
                            'phone_number',
                            'District',
                            'Ward Number',
                            'Village Name',
                            'Is HoHH',
                            'HoHH Name',
                            'HoHH ID',
                            'HH Size',
                            'HH Females',
                            'Under Five',
                            'Production Area',
                            'Value Chain' 
        ];
    }
    
    // public function chunkSize(): int
    // {
    //     return 500; 
    // }
}
