<?php

namespace App\Exports;

use App\Models\Teacher;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\FromCollection;

class TeacherExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function collection()
    {
        //dd(Teacher::all());
        return Teacher::all();
    }
    
    // public function query()
    // {
    //     //dd(Teacher::all());
    //     return  Teacher::all(); 
    // }
    
    public function map($teacher): array
    {
        return [
                $teacher->id,
                            
                $teacher->name,
                        
                $teacher->type, 
                        
                $teacher->contact,
                
                $teacher->wards,
                 
                $teacher->district,
                        
            
        ];
    }
    
    public function headings(): array
    {
        return [
                 '#',
                 'Name',
                 'Type', 
                 'Contact',
                 'Ward',
                 'District', 
        ];
    }
}
