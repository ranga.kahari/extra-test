<?php

namespace App\Exports;

use App\Models\Crop;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\FromCollection;

class YieldDataExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
     public function collection()
    {
        //dd(Crop::all());
        return Crop::all();
    }
    
    
    public function map($crop): array
    {
        return [
                $crop->id,  
                $crop->season,        
                $crop->value_chain,     
                $crop->area_planted,         
                $crop->yield,
                $crop->production,
                $crop->beneficiaries[0]['first_name'],
                $crop->beneficiaries[0]['surname'],
                $crop->beneficiaries[0]['national_id_number'],
                $crop->beneficiaries[0]['gender'],
                $crop->beneficiaries[0]['phone_number'],
                        
            
        ];
    }
    
    public function headings(): array
    {
        return [
                 '#',
                 'season', 
                 'value_chain',
                 'area_planted', 
                 'yield',
                 'production',
                 'firstname', 
                 'surname',
                 'id_number',
                 'Sex',
                 'Mobile', 
        ];
    }
}
