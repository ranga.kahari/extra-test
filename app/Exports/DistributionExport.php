<?php

namespace App\Exports;

use App\Models\Distribution;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\FromCollection;


class DistributionExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //dd(Distribution::all());
        return Distribution::all();
    }
    
    public function map($distribution): array
    {
        return [
                $distribution->id,
                $distribution->date,
                $distribution->item_distributed_type, 
                $distribution->item_distributed,            
                $distribution->variety,       
                $distribution->quantity,
                $distribution->project,     
                $distribution->beneficiaries[0]['first_name'],
                $distribution->beneficiaries[0]['surname'],
                $distribution->beneficiaries[0]['national_id_number'],
                $distribution->beneficiaries[0]['gender'],
                $distribution->beneficiaries[0]['phone_number'],
                $distribution->beneficiaries[0]['village_id'],
                        
            
        ];
    }
    
    public function headings(): array
    {
        return [
                 '#',
                 'date',
                 'item_distributed_type', 
                 'item_distributed',
                 'variety', 
                 'project',
                 'quantity', 
                 'firstname', 
                 'surname',
                 'id_number',
                 'Sex',
                 'Mobile',
        ];
    }
}
