<?php

namespace App\Imports;

use App\Models\Group;
use App\Models\Member;
use App\Models\Attendance;
use App\Models\Beneficiary;
use App\Traits\AgeFunctions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class CareGroupImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    
    use ProcessesExcelImports, AgeFunctions;
    
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {

            $rowArray = $row->toArray();
            
             $group = Group::firstOrCreate([
                    'group_number'          => $this->getCareGroupCode($rowArray),
                ], [
                    'group_district'        => $this->getDistrict($rowArray),
                    'group_categories'      => $this->getGroupCategory($rowArray),
                    'health_centre_ward'    => $this->getWard($rowArray)->id,
                    'name_of_health_centre' => $this->getHealthCentreName($rowArray),
                ]);
                    
                // Log::info('Facilitator One'.$this->getPersonRegistered($rowArray));
                // Log::info('Facilitator Two'.$this->getVillageHealthWorker($rowArray));
                
                $villageHealthWorker = $this->getVillageHealthWorker($rowArray);
                $group->facilitators()->sync([$villageHealthWorker->id => ['position' => $villageHealthWorker->type]]);
                        
                $personRegistered = $this->getPersonRegistered($rowArray);
                $group->facilitators()->syncWithoutDetaching([$personRegistered->id => ['position' => $personRegistered->type]]);
                
                

            //TODO: Optimize
            $beneficiary = Beneficiary::updateOrCreate([
                'first_name' => $this->getFirstname($rowArray),
                'surname' => $this->getLastname($rowArray),
                'gender' => $this->getGender($rowArray),
                'national_id_number' => $this->getNationalIdNumber($rowArray),
                'year_of_birth' => $this->getBirthYear($rowArray) ?: null,
            ], [
                'name' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray) . ' (' . $this->getNationalIdNumber($rowArray) . ')',
                'phone_number' => $this->getMobileNumber($rowArray),
                'is_household_head' => 1,
                'name_of_household_head' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray),
                'id_of_household_head' => $this->getNationalIdNumber($rowArray),
                'village_id' => $this->getVillage($rowArray)->id,
                'farmer_category' => $this->getFarmerCategory($rowArray),
                'farmer_group_membership' => $this->getFarmerGroupMembership($rowArray),
                'children_under_five_in_household' => $this->getChildrenUnderFive($rowArray),
            ]);

           //$beneficiary->groups()->sync($this->getGroupCode($rowArray)->pluck('id'));

            // TODO: Optimize
            $this->setAgeBand($beneficiary);

            // TODO: Optimize
            $members = Member::create([
                'beneficiary_id' => $beneficiary->id,
                'group_id' => $group->id
            ]);
            

        }
    }
    
     public function chunkSize(): int
    {
        return 500;
    }
}
