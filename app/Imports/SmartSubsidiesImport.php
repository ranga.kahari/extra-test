<?php

namespace App\Imports;

use App\Models\Group;
use App\Models\Member;
use App\Models\Beneficiary;
use App\Models\SmartSubsidies;
use App\Traits\AgeFunctions;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class SmartSubsidiesImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $row){
            
            $rowArray = $row->toArray();
            
            Log::info($rowArray);
            
                SmartSubsidies::firstOrCreate([
                    'district'          => $this->getDistrict($rowArray),
                    'custodian'   => $this->getSmartGroup($rowArray),
                    'ward_id'   => $this->getSmartWard($rowArray)->id,
                    'males'   => $this->getMales($rowArray),
                    'females'   => $this->getFemales($rowArray),
                    'total'   => $this->getTotal($rowArray),
                    'value_chain'   => $this->getSmartValueChain($rowArray),
                    'farmer_contribution_value_usd'    => $this->getFarmerContributionUSD($rowArray),
                    'extra_contribution'    => $this->getExtraContribution($rowArray),
                    'subsidy_allocated_per_hh'    => $this->getSubsidyAllocated($rowArray),
                    'collected'    => $this->getCollected($rowArray),
                    'outstanding'    => $this->getOutStanding($rowArray),
                    'status'     => $this->getStatus($rowArray),
                ]);
        }
    }
    
    public function chunkSize(): int
    {
        return 500; 
    }
}
