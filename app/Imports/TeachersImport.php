<?php

namespace App\Imports;

use App\Models\Teacher;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class TeachersImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
     use ProcessesExcelImports;
     
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $row){
            
            $rowArray = $row->toArray();
            
            //Log::info($rowArray);
            
            Teacher::firstOrCreate([
                'name' => $this->getFacilitatorName($rowArray),
             ],[
                'type' => $this->getFacilitatorType($rowArray),
                'contact' => $this->getFacilitatorContact($rowArray),
                'wards' => $this->getFacilitatorWard($rowArray),
                'district' => $this->getFacilitatorDistrict($rowArray),
             ]);
             
             
        }
    }
    
    public function chunkSize(): int
    {
        return 500; 
    }
}
