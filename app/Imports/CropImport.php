<?php

namespace App\Imports;

use App\Models\Crop;
use App\Models\Beneficiary;
use App\Traits\AgeFunctions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class CropImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports, AgeFunctions;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            
            $rowArray = $row->toArray();
         
            //Log::info($rowArray);
            
            $beneficiary = Beneficiary::updateOrCreate([
                'first_name' => $this->getFirstname($rowArray),
                'surname' => $this->getLastname($rowArray),
                'gender' => $this->getGender($rowArray),
                'national_id_number' => $this->getNationalIdNumber($rowArray),
            ], [
                'name' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray) . ' (' . $this->getNationalIdNumber($rowArray) . ')',
                'phone_number' => $this->getMobileNumber($rowArray),
                'is_household_head' => 1,
                'name_of_household_head' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray),
                'id_of_household_head' => $this->getNationalIdNumber($rowArray),
                'village_id' => $this->getVillage($rowArray)->id,
            ]);

            $this->setAgeBand($beneficiary);
            
            $crops = Crop::firstOrCreate([
                'district'          => $this->getDistrict($rowArray),
                'season'          => $this->getSeasonYear($rowArray),
                'value_chain'     => $this->getYieldValueChain($rowArray),
                'area_planted'    => $this->getAreaPlanted($rowArray),
                'yield'           => $this->getYield($rowArray),
                'production'      => $this->getProduction($rowArray),
                'beneficiary_id'  => $beneficiary->id,
            ]);
            
        }
    }
    
    public function chunkSize(): int
    {
        return 500;
    }
}
