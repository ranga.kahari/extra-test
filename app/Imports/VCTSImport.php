<?php

namespace App\Imports;

use App\Models\Group;
use App\Models\Member;
use App\Models\Training;
use App\Models\Attendance;
use App\Models\Beneficiary;
use App\Traits\AgeFunctions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class VCTSImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports, AgeFunctions;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

        foreach ($collection as $row) {

            $rowArray = $row->toArray();
            
            Log::info($rowArray);
            
             $group = Group::firstOrCreate([
                    // 'group_number'        => $this->getGroupNumber($rowArray),
                    'group_number'        => $rowArray['group_code'],
                ], [
                    'group_district'      => $this->getDistrict($rowArray),
                    'group_categories'    => $this->getGroupCategory($rowArray),
                    'demo_type'    => $this->getVCTSDemoType($rowArray),
                    'value_chain_id'      =>  $this->getValueChainsVCTS($rowArray)->id,
                ]);

                //$group->facilitators()->sync($this->getFacilitators($rowArray)->pluck('id'));
                
                $facilitator = $this->getVCTSFacilitator($rowArray);
                $group->facilitators()->sync([$facilitator->id => ['position' => $facilitator->type]]);

            // TODO: Optimize
            $beneficiary = Beneficiary::updateOrCreate([
                'first_name' => $this->getFirstname($rowArray),
                'surname' => $this->getSurname($rowArray),
                'gender' => $this->getGender($rowArray),
                'national_id_number' => $this->getNationalIdNumber($rowArray),
                'year_of_birth' => $this->getBirthYear($rowArray) ?: null,
            ], [
                'name' => $this->getFirstname($rowArray) . ' ' . $this->getSurname($rowArray) . ' (' . $this->getNationalIdNumber($rowArray) . ')',
                'phone_number' => $this->getMobileNumber($rowArray),
                'is_household_head' => 1,
                'name_of_household_head' => $this->getFirstname($rowArray) . ' ' . $this->getSurname($rowArray),
                'id_of_household_head' => $this->getNationalIdNumber($rowArray),
                'village_id' => $this->getVillage($rowArray)->id,
            ]);

           $beneficiary->groups()->sync($this->getGroups($rowArray)->pluck('id'));

            // TODO: Optimize
            $this->setAgeBand($beneficiary);

            // TODO: Optimize
            $members = Member::create([
                'beneficiary_id' => $beneficiary->id,
                'group_id' => $group->id
            ]);
            // $members = Member::query()->firstOrCreate([
            //     'beneficiary_id' => $beneficiary->id,
            //     'group_id' => $group->id
            // ]);

        }
    }

    public function chunkSize(): int
    {
        return 500; 
    }

}
