<?php

namespace App\Imports;

use App\Models\Intervention;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class InterventionsImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $row){
            
            $rowArray = $row->toArray();
            
            Log::info($rowArray);
            
              Intervention::firstOrCreate([
                'village_id'   => $this->getInterventionVillage($rowArray)->id,
                'custodian'     => $this->getCustodian($rowArray),
                'intervention'  => $this->getIntervention($rowArray),
                'latitude'      => $this->getLatitude($rowArray),
                'longitude'     => $this->getLongitude($rowArray),
                'district'=>$this->getDistrict($rowArray),
                
             ],[

             ]);
             
        }
         
    }
    
    public function chunkSize(): int
    {
        return 500; 
    }
}
