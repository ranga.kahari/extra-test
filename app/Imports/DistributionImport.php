<?php

namespace App\Imports;

use App\Models\Beneficiary;
use App\Models\Distribution;

use App\Traits\AgeFunctions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class DistributionImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports, AgeFunctions;
    
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            
            $rowArray = $row->toArray();
            
            Log::info($rowArray);
            
            $beneficiary = Beneficiary::updateOrCreate([
                'first_name' => $this->getFirstname($rowArray),
                'surname' => $this->getLastname($rowArray),
                'gender' => $this->getGender($rowArray),
                'national_id_number' => $this->getNationalIdNumber($rowArray),
            ], [
                'name' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray) . ' (' . $this->getNationalIdNumber($rowArray) . ')',
                'phone_number' => $this->getMobileNumber($rowArray),
                'is_household_head' => 1,
                'name_of_household_head' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray),
                'id_of_household_head' => $this->getNationalIdNumber($rowArray),
                'village_id' => $this->getVillage($rowArray)->id,
            ]);

            $this->setAgeBand($beneficiary);
            
            
            
            
            $distribution = Distribution::firstOrCreate([
                'district'                   => $this->getDistrict($rowArray),
                'project'                   => $this->getProject($rowArray),
                'item_distributed_type'     => $this->getItemsDistributedType($rowArray) ,
                'date'                      => $this->getDistributionDate($rowArray),
                'item_distributed'          => $this->getItemsDistributed($rowArray),
                'variety'                   => $this->getVariety($rowArray),
                'quantity'                  => $this->getQuantity($rowArray),
                'beneficiary_id'            => $beneficiary->id,
            ]);
        }
    }
    
    public function chunkSize(): int
    {
        return 500;
    }
}
