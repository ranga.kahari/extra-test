<?php

namespace App\Imports;

use App\Models\Attendance;
use App\Models\Beneficiary;
use App\Models\Training;
use App\Traits\AgeFunctions;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;

class TrainingsImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports, AgeFunctions;

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {

        foreach ($collection as $row) {

            $rowArray = $row->toArray();

            // Start transaction!
            DB::beginTransaction();
            try {

                // TODO: Optimize

                $training = Training::firstOrCreate([
                    'training_code' => $this->getTrainingCode($rowArray),
                ], [
                    'name'          => $this->getTrainingName($rowArray),
                    'village_id'    => $this->getVillage($rowArray)->id,
                    'venue'         => $this->getVenue($rowArray),
                    'start_date'    => $this->getStartDate($rowArray)->format(config('panel.date_format')),
                    'end_date'      => $this->getEndDate($rowArray)->format(config('panel.date_format')),
                    'prepared_by'   => 'Admin',
                    'reviewed_by'   => 'Admin'
                ]);

                $training->facilitators()->sync($this->getFacilitators($rowArray)->pluck('id'));
                $training->thematicAreas()->sync($this->getThematicAreas($rowArray)->pluck('id'));
                $training->valueChains()->sync($this->getValueChains($rowArray)->pluck('id'));
                $training->topics()->sync($this->getTopics($rowArray)->pluck('id'));

                // TODO: Optimize
                $beneficiary = Beneficiary::updateOrCreate([
                    'first_name' => $this->getFirstname($rowArray),
                    'surname' => $this->getSurname($rowArray),
                    'gender' => $this->getGender($rowArray),
                    'national_id_number' => $this->getNationalIdNumber($rowArray),
                    'year_of_birth' => $this->getBirthYear($rowArray) ?: null,
                ], [
                    'name' => $this->getFirstname($rowArray) . ' ' . $this->getSurname($rowArray) . ' (' . $this->getNationalIdNumber($rowArray) . ')',
                    'phone_number' => $this->getMobileNumber($rowArray),
                    'is_household_head' => 1,
                    'name_of_household_head' => $this->getFirstname($rowArray) . ' ' . $this->getSurname($rowArray),
                    'id_of_household_head' => $this->getNationalIdNumber($rowArray),
                    'village_id' => $this->getVillage($rowArray)->id,
                ]);

                $beneficiary->groups()->sync($this->getGroups($rowArray)->pluck('id'));

                // TODO: Optimize
                $this->setAgeBand($beneficiary);

                // TODO: Optimize
                $attendance = Attendance::create([
                    'beneficiary_id' => $beneficiary->id,
                    'training_id' => $training->id
                ]);

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
            }
        }
    }


    public function chunkSize(): int
    {
        return 500;
    }
}
