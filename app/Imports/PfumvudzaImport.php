<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use App\Models\Beneficiary;
use App\Models\Pfumvudza;
use App\Traits\AgeFunctions;
use Illuminate\Support\Facades\Log;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\ToCollection;

class PfumvudzaImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use ProcessesExcelImports, AgeFunctions;
    
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            
            $rowArray = $row->toArray();
            
            $beneficiary = Beneficiary::updateOrCreate([
                'first_name' => $this->getFirstname($rowArray),
                'surname' => $this->getLastname($rowArray),
                'gender' => $this->getGender($rowArray),
                'national_id_number' => $this->getNationalIdNumber($rowArray),
            ], [
                'name' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray) . ' (' . $this->getNationalIdNumber($rowArray) . ')',
                'phone_number' => $this->getMobileNumber($rowArray),
                'is_household_head' => 1,
                'name_of_household_head' => $this->getFirstname($rowArray) . ' ' . $this->getLastname($rowArray),
                'id_of_household_head' => $this->getNationalIdNumber($rowArray),
                'size_of_household' => $this->getSizeOfHousehold($rowArray),
                'village_id' => $this->getVillage($rowArray)->id,
            ]);

            $this->setAgeBand($beneficiary);
            
            $pfumvudza = Pfumvudza::firstOrCreate([
                'district'          => $this->getDistrict($rowArray),
                'agronomic_practice'          => $this->getAgronomicPractice($rowArray),
                'value_chain'          => $this->getYieldValueChain($rowArray),
                'variety'          => $this->getVariety($rowArray),
                'area_planted'          => $this->getAreaPlanted($rowArray),
                'beneficiary_id'  => $beneficiary->id,
            ]);
            
            $facilitator = $this->getVCTSFacilitator($rowArray);
            $pfumvudza->facilitators()->sync([$facilitator->id => ['position' => $facilitator->type]]);
        }
    }
    
    public function chunkSize(): int
    {
        return 500;
    }
}
