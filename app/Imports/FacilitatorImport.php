<?php

namespace App\Imports;

use App\Models\Facilitator;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Traits\ProcessesExcelImports;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class FacilitatorImport implements ToCollection, WithHeadingRow, WithChunkReading, ShouldQueue
{
    
    use ProcessesExcelImports;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        
        foreach($collection as $row){
            
            $rowArray = $row->toArray();
            
             Facilitator::updateOrCreate([
                 'name' => $this->getFacilitatorName($rowArray),
                // 'name' => $rowArray['Name'],
             ],[
                'type' => $this->getFacilitatorType($rowArray),
                'contact' => $this->getFacilitatorContact($rowArray),
                'district' => $this->getFacilitatorDistrict($rowArray),
                'wards' => $this->getFacilitatorWard($rowArray),
             ]);
        }
    }
    
     public function chunkSize(): int
    {
        return 500; 
    }
}
